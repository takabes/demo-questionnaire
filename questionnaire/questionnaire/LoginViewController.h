//
//  LoginViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>  {
	UITextField*	_userIdTextField;
	UITextField*	_passwordTextField;
	UILabel*		_dataCountLabel;

	NSUInteger		_currentTextFieldTag;

	UIButton*		_oneButton;
	UIButton*		_twoButton;
	UIButton*		_threeButton;
	UIButton*		_fourButton;
	UIButton*		_fiveButton;
	UIButton*		_sixButton;
	UIButton*		_sevenButton;
	UIButton*		_eightButton;
	UIButton*		_nineButton;
	UIButton*		_zeroButton;

	UIButton*		_qButton;
	UIButton*		_wButton;
	UIButton*		_eButton;
	UIButton*		_rButton;
	UIButton*		_tButton;
	UIButton*		_yButton;
	UIButton*		_uButton;
	UIButton*		_iButton;
	UIButton*		_oButton;
	UIButton*		_pButton;
	UIButton*		_hyphenButton;

	UIButton*		_aButton;
	UIButton*		_sButton;
	UIButton*		_dButton;
	UIButton*		_fButton;
	UIButton*		_gButton;
	UIButton*		_hButton;
	UIButton*		_jButton;
	UIButton*		_kButton;
	UIButton*		_lButton;

	UIButton*		_zButton;
	UIButton*		_xButton;
	UIButton*		_cButton;
	UIButton*		_vButton;
	UIButton*		_bButton;
	UIButton*		_nButton;
	UIButton*		_mButton;
	UIButton*		_dotButton;
	UIButton*		_underscoreButton;

	UIButton*		_deleteButton;

	UIButton*		_nextButton;
	UIButton*		_adminButton;
	UIButton*		_showKeyboardButton;
}

@property (nonatomic) IBOutlet UITextField* userIdTextField;
@property (nonatomic) IBOutlet UITextField* passwordTextField;

@property (nonatomic) IBOutlet UILabel* dataCountLabel;

@property (nonatomic) IBOutlet UIButton* oneButton;
@property (nonatomic) IBOutlet UIButton* twoButton;
@property (nonatomic) IBOutlet UIButton* threeButton;
@property (nonatomic) IBOutlet UIButton* fourButton;
@property (nonatomic) IBOutlet UIButton* fiveButton;
@property (nonatomic) IBOutlet UIButton* sixButton;
@property (nonatomic) IBOutlet UIButton* sevenButton;
@property (nonatomic) IBOutlet UIButton* eightButton;
@property (nonatomic) IBOutlet UIButton* nineButton;
@property (nonatomic) IBOutlet UIButton* zeroButton;

@property (nonatomic) IBOutlet UIButton* qButton;
@property (nonatomic) IBOutlet UIButton* wButton;
@property (nonatomic) IBOutlet UIButton* eButton;
@property (nonatomic) IBOutlet UIButton* rButton;
@property (nonatomic) IBOutlet UIButton* tButton;
@property (nonatomic) IBOutlet UIButton* yButton;
@property (nonatomic) IBOutlet UIButton* uButton;
@property (nonatomic) IBOutlet UIButton* iButton;
@property (nonatomic) IBOutlet UIButton* oButton;
@property (nonatomic) IBOutlet UIButton* pButton;
@property (nonatomic) IBOutlet UIButton* hyphenButton;

@property (nonatomic) IBOutlet UIButton* aButton;
@property (nonatomic) IBOutlet UIButton* sButton;
@property (nonatomic) IBOutlet UIButton* dButton;
@property (nonatomic) IBOutlet UIButton* fButton;
@property (nonatomic) IBOutlet UIButton* gButton;
@property (nonatomic) IBOutlet UIButton* hButton;
@property (nonatomic) IBOutlet UIButton* jButton;
@property (nonatomic) IBOutlet UIButton* kButton;
@property (nonatomic) IBOutlet UIButton* lButton;

@property (nonatomic) IBOutlet UIButton* zButton;
@property (nonatomic) IBOutlet UIButton* xButton;
@property (nonatomic) IBOutlet UIButton* cButton;
@property (nonatomic) IBOutlet UIButton* vButton;
@property (nonatomic) IBOutlet UIButton* bButton;
@property (nonatomic) IBOutlet UIButton* nButton;
@property (nonatomic) IBOutlet UIButton* mButton;
@property (nonatomic) IBOutlet UIButton* dotButton;
@property (nonatomic) IBOutlet UIButton* underscoreButton;

@property (nonatomic) IBOutlet UIButton* deleteButton;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* adminButton;
@property (nonatomic) IBOutlet UIButton* showKeyboardButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)adminButtonTapped:(UIButton*)button;
- (IBAction)showKeyboardButtonTapped:(UIButton*)button;

@end
