//
//  SampleViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "SampleViewController.h"
#import "InputNameViewController.h"
#import "UtilityFunction.h"
#import "AppConsts.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"

@interface SampleViewController ()
@end

@implementation SampleViewController

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"Sample" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"入力内容";

    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];

	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	InputNameViewController *viewController = [[InputNameViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中の内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		[UtilityFunction resetPersonalInfo];
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}
		
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
