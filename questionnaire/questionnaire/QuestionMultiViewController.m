//
//  QuestionMultiViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "QuestionMultiViewController.h"
#import "Question06ViewController.h"
#import "QuestionSingleViewController.h"
#import "InputNameViewController.h"
#import "AppDelegate.h"
#import "UtilityFunction.h"
#import "QuestionListViewController.h"

@interface QuestionMultiViewController ()
@end

@implementation QuestionMultiViewController

@synthesize questionNumber = _questionNumber;

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"QuestionMulti" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"アンケート入力";

    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	[self initHeaderLabel];
	
	[self initTitle];
	
	[self initButton];
	
	[self initChoice];

	[self initAnswer];

	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initHeaderLabel {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	
	if (_questionNumber == 19) {
		if (questions[@"q20"]) {
			_headerLabel.text = @"8 / 9";
		} else {
			_headerLabel.text = @"8 / 8";
		}
	} else if (_questionNumber == 20) {
		_headerLabel.text = @"9 / 9";
	}
}

- (void)initTitle {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];

	if (_questionNumber == 19) {
		_questionTitleLabel.text = [NSString stringWithFormat:@"Q%d %@（複数回答可）", _questionNumber, questions[@"q19"][@"title"]];
	} else if (_questionNumber == 20) {
		_questionTitleLabel.text = [NSString stringWithFormat:@"Q%d %@（複数回答可）", _questionNumber, questions[@"q20"][@"title"]];
	}
}

- (void)initButton {
	
	[_qAnswer01Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer02Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer03Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer04Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer05Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer06Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer07Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer08Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer09Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer10Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer11Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer12Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer13Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer14Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer15Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer16Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer17Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer18Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer19Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_qAnswer20Button addTarget:self action:@selector(answerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
}

- (void)initChoice {

	_qAnswer01Button.hidden = YES;
	_qAnswer02Button.hidden = YES;
	_qAnswer03Button.hidden = YES;
	_qAnswer04Button.hidden = YES;
	_qAnswer05Button.hidden = YES;
	_qAnswer06Button.hidden = YES;
	_qAnswer07Button.hidden = YES;
	_qAnswer08Button.hidden = YES;
	_qAnswer09Button.hidden = YES;
	_qAnswer10Button.hidden = YES;
	_qAnswer11Button.hidden = YES;
	_qAnswer12Button.hidden = YES;
	_qAnswer13Button.hidden = YES;
	_qAnswer14Button.hidden = YES;
	_qAnswer15Button.hidden = YES;
	_qAnswer16Button.hidden = YES;
	_qAnswer17Button.hidden = YES;
	_qAnswer18Button.hidden = YES;
	_qAnswer19Button.hidden = YES;
	_qAnswer20Button.hidden = YES;
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	NSArray* choices;
	
	if (_questionNumber == 19) {
		choices = questions[@"q19"][@"choices"];
	} else if (_questionNumber == 20) {
		choices = questions[@"q20"][@"choices"];
	} else {
		return;
	}
	
	for (int i=0; i < [choices count]; i++) {
		
		switch (i) {
			case 0:
				_qAnswer01Button.hidden = NO;
				[_qAnswer01Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 1:
				_qAnswer02Button.hidden = NO;
				[_qAnswer02Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 2:
				_qAnswer03Button.hidden = NO;
				[_qAnswer03Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 3:
				_qAnswer04Button.hidden = NO;
				[_qAnswer04Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 4:
				_qAnswer05Button.hidden = NO;
				[_qAnswer05Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 5:
				_qAnswer06Button.hidden = NO;
				[_qAnswer06Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 6:
				_qAnswer07Button.hidden = NO;
				[_qAnswer07Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 7:
				_qAnswer08Button.hidden = NO;
				[_qAnswer08Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 8:
				_qAnswer09Button.hidden = NO;
				[_qAnswer09Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 9:
				_qAnswer10Button.hidden = NO;
				[_qAnswer10Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 10:
				_qAnswer11Button.hidden = NO;
				[_qAnswer11Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 11:
				_qAnswer12Button.hidden = NO;
				[_qAnswer12Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 12:
				_qAnswer13Button.hidden = NO;
				[_qAnswer13Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 13:
				_qAnswer14Button.hidden = NO;
				[_qAnswer14Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 14:
				_qAnswer15Button.hidden = NO;
				[_qAnswer15Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 15:
				_qAnswer16Button.hidden = NO;
				[_qAnswer16Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 16:
				_qAnswer17Button.hidden = NO;
				[_qAnswer17Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 17:
				_qAnswer18Button.hidden = NO;
				[_qAnswer18Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 18:
				_qAnswer19Button.hidden = NO;
				[_qAnswer19Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 19:
				_qAnswer20Button.hidden = NO;
				[_qAnswer19Button setTitle:[choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
		}
	}
	
	NSString* otherChoice;

	if (_questionNumber == 19) {
		otherChoice = questions[@"q19"][@"otherChoice"];
	} else if (_questionNumber == 20) {
		otherChoice = questions[@"q20"][@"otherChoice"];
	} else {
		return;
	}

	if (otherChoice && [otherChoice length]) {
		switch ([choices count]) {
			case 1:
				_qAnswer02Button.hidden = NO;
				_qAnswer02Button.tag = 20;
				[_qAnswer02Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 2:
				_qAnswer03Button.hidden = NO;
				_qAnswer03Button.tag = 20;
				[_qAnswer03Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 3:
				_qAnswer04Button.hidden = NO;
				_qAnswer04Button.tag = 20;
				[_qAnswer04Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 4:
				_qAnswer05Button.hidden = NO;
				_qAnswer05Button.tag = 20;
				[_qAnswer05Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 5:
				_qAnswer06Button.hidden = NO;
				_qAnswer06Button.tag = 20;
				[_qAnswer06Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 6:
				_qAnswer07Button.hidden = NO;
				_qAnswer07Button.tag = 20;
				[_qAnswer07Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 7:
				_qAnswer07Button.hidden = NO;
				_qAnswer07Button.tag = 20;
				[_qAnswer07Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 8:
				_qAnswer09Button.hidden = NO;
				_qAnswer09Button.tag = 20;
				[_qAnswer09Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 9:
				_qAnswer10Button.hidden = NO;
				_qAnswer10Button.tag = 20;
				[_qAnswer10Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 10:
				_qAnswer11Button.hidden = NO;
				_qAnswer11Button.tag = 20;
				[_qAnswer11Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 11:
				_qAnswer12Button.hidden = NO;
				_qAnswer12Button.tag = 20;
				[_qAnswer12Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 12:
				_qAnswer13Button.hidden = NO;
				_qAnswer13Button.tag = 20;
				[_qAnswer13Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 13:
				_qAnswer14Button.hidden = NO;
				_qAnswer14Button.tag = 20;
				[_qAnswer14Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 14:
				_qAnswer15Button.hidden = NO;
				_qAnswer15Button.tag = 20;
				[_qAnswer15Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 15:
				_qAnswer16Button.hidden = NO;
				_qAnswer16Button.tag = 20;
				[_qAnswer16Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 16:
				_qAnswer17Button.hidden = NO;
				_qAnswer17Button.tag = 20;
				[_qAnswer17Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 17:
				_qAnswer18Button.hidden = NO;
				_qAnswer18Button.tag = 20;
				[_qAnswer18Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 18:
				_qAnswer19Button.hidden = NO;
				_qAnswer19Button.tag = 20;
				[_qAnswer19Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
			case 19:
				_qAnswer20Button.hidden = NO;
				_qAnswer20Button.tag = 20;
				[_qAnswer20Button setTitle:otherChoice forState:UIControlStateNormal];
				break;
		}
	}
}

- (void)initAnswer {
	if (_questionNumber == 19) {
		[self initQ19Answer];
	} else if (_questionNumber == 20) {
		[self initQ20Answer];
	}
}

- (void)initQ19Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q19Answer01 isEqualToString:@"1"]) {
		[_qAnswer01Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer02 isEqualToString:@"1"]) {
		[_qAnswer02Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer03 isEqualToString:@"1"]) {
		[_qAnswer03Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer04 isEqualToString:@"1"]) {
		[_qAnswer04Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer05 isEqualToString:@"1"]) {
		[_qAnswer05Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer06 isEqualToString:@"1"]) {
		[_qAnswer06Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer07 isEqualToString:@"1"]) {
		[_qAnswer07Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer08 isEqualToString:@"1"]) {
		[_qAnswer08Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer09 isEqualToString:@"1"]) {
		[_qAnswer09Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer10 isEqualToString:@"1"]) {
		[_qAnswer10Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer11 isEqualToString:@"1"]) {
		[_qAnswer11Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer12 isEqualToString:@"1"]) {
		[_qAnswer12Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer13 isEqualToString:@"1"]) {
		[_qAnswer13Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer14 isEqualToString:@"1"]) {
		[_qAnswer14Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer15 isEqualToString:@"1"]) {
		[_qAnswer15Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer16 isEqualToString:@"1"]) {
		[_qAnswer16Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer17 isEqualToString:@"1"]) {
		[_qAnswer17Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer18 isEqualToString:@"1"]) {
		[_qAnswer18Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer19 isEqualToString:@"1"]) {
		[_qAnswer19Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q19Answer20 isEqualToString:@"1"]) {
		
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		id questions = [defaults objectForKey:@"questions"];
		NSArray* choices = questions[@"q19"][@"choices"];
		
		switch ([choices count]) {
			case 1:
				[_qAnswer02Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 2:
				[_qAnswer03Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 3:
				[_qAnswer04Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 4:
				[_qAnswer05Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 5:
				[_qAnswer06Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 6:
				[_qAnswer07Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 7:
				[_qAnswer08Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 8:
				[_qAnswer09Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 9:
				[_qAnswer10Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 10:
				[_qAnswer11Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 11:
				[_qAnswer12Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 12:
				[_qAnswer13Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 13:
				[_qAnswer14Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 14:
				[_qAnswer15Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 15:
				[_qAnswer16Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 16:
				[_qAnswer17Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 17:
				[_qAnswer18Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 18:
				[_qAnswer19Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 19:
				[_qAnswer20Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
		}
	}
}

- (void)initQ20Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q20Answer01 isEqualToString:@"1"]) {
		[_qAnswer01Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer02 isEqualToString:@"1"]) {
		[_qAnswer02Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer03 isEqualToString:@"1"]) {
		[_qAnswer03Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer04 isEqualToString:@"1"]) {
		[_qAnswer04Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer05 isEqualToString:@"1"]) {
		[_qAnswer05Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer06 isEqualToString:@"1"]) {
		[_qAnswer06Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer07 isEqualToString:@"1"]) {
		[_qAnswer07Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer08 isEqualToString:@"1"]) {
		[_qAnswer08Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer09 isEqualToString:@"1"]) {
		[_qAnswer09Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer10 isEqualToString:@"1"]) {
		[_qAnswer10Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer11 isEqualToString:@"1"]) {
		[_qAnswer11Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer12 isEqualToString:@"1"]) {
		[_qAnswer12Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer13 isEqualToString:@"1"]) {
		[_qAnswer13Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer14 isEqualToString:@"1"]) {
		[_qAnswer14Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer15 isEqualToString:@"1"]) {
		[_qAnswer15Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer16 isEqualToString:@"1"]) {
		[_qAnswer16Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer17 isEqualToString:@"1"]) {
		[_qAnswer17Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer18 isEqualToString:@"1"]) {
		[_qAnswer18Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer19 isEqualToString:@"1"]) {
		[_qAnswer19Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q20Answer20 isEqualToString:@"1"]) {
		
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		id questions = [defaults objectForKey:@"questions"];
		NSArray* choices = questions[@"q20"][@"choices"];
		
		switch ([choices count]) {
			case 1:
				[_qAnswer02Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 2:
				[_qAnswer03Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 3:
				[_qAnswer04Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 4:
				[_qAnswer05Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 5:
				[_qAnswer06Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 6:
				[_qAnswer07Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 7:
				[_qAnswer08Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 8:
				[_qAnswer09Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 9:
				[_qAnswer10Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 10:
				[_qAnswer11Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 11:
				[_qAnswer12Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 12:
				[_qAnswer13Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 13:
				[_qAnswer14Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 14:
				[_qAnswer15Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 15:
				[_qAnswer16Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 16:
				[_qAnswer17Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 17:
				[_qAnswer18Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 18:
				[_qAnswer19Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 19:
				[_qAnswer20Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
		}
	}
}

- (void)answerButtonTapped:(UIButton*)button {
	if (_questionNumber == 19) {
		[self q19AnswerButtonTapped:button];
	} else if (_questionNumber == 20) {
		[self q20AnswerButtonTapped:button];
	}
}

- (void)q19AnswerButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	BOOL check = YES;
	
	switch (button.tag) {
		case 1:
			if ([appDelegate.q19Answer01 isEqualToString:@"0"]) {
				appDelegate.q19Answer01 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer01 = @"0";
				check = NO;
			}
			break;
		case 2:
			if ([appDelegate.q19Answer02 isEqualToString:@"0"]) {
				appDelegate.q19Answer02 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer02 = @"0";
				check = NO;
			}
			break;
		case 3:
			if ([appDelegate.q19Answer03 isEqualToString:@"0"]) {
				appDelegate.q19Answer03 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer03 = @"0";
				check = NO;
			}
			break;
		case 4:
			if ([appDelegate.q19Answer04 isEqualToString:@"0"]) {
				appDelegate.q19Answer04 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer04 = @"0";
				check = NO;
			}
			break;
		case 5:
			if ([appDelegate.q19Answer05 isEqualToString:@"0"]) {
				appDelegate.q19Answer05 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer05 = @"0";
				check = NO;
			}
			break;
		case 6:
			if ([appDelegate.q19Answer06 isEqualToString:@"0"]) {
				appDelegate.q19Answer06 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer06 = @"0";
				check = NO;
			}
			break;
		case 7:
			if ([appDelegate.q19Answer07 isEqualToString:@"0"]) {
				appDelegate.q19Answer07 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer07 = @"0";
				check = NO;
			}
			break;
		case 8:
			if ([appDelegate.q19Answer08 isEqualToString:@"0"]) {
				appDelegate.q19Answer08 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer08 = @"0";
				check = NO;
			}
			break;
		case 9:
			if ([appDelegate.q19Answer09 isEqualToString:@"0"]) {
				appDelegate.q19Answer09 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer09 = @"0";
				check = NO;
			}
			break;
		case 10:
			if ([appDelegate.q19Answer10 isEqualToString:@"0"]) {
				appDelegate.q19Answer10 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer10 = @"0";
				check = NO;
			}
			break;
		case 11:
			if ([appDelegate.q19Answer11 isEqualToString:@"0"]) {
				appDelegate.q19Answer11 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer11 = @"0";
				check = NO;
			}
			break;
		case 12:
			if ([appDelegate.q19Answer12 isEqualToString:@"0"]) {
				appDelegate.q19Answer12 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer12 = @"0";
				check = NO;
			}
			break;
		case 13:
			if ([appDelegate.q19Answer13 isEqualToString:@"0"]) {
				appDelegate.q19Answer13 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer13 = @"0";
				check = NO;
			}
			break;
		case 14:
			if ([appDelegate.q19Answer14 isEqualToString:@"0"]) {
				appDelegate.q19Answer14 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer14 = @"0";
				check = NO;
			}
			break;
		case 15:
			if ([appDelegate.q19Answer15 isEqualToString:@"0"]) {
				appDelegate.q19Answer15 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer15 = @"0";
				check = NO;
			}
			break;
		case 16:
			if ([appDelegate.q19Answer16 isEqualToString:@"0"]) {
				appDelegate.q19Answer16 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer16 = @"0";
				check = NO;
			}
			break;
		case 17:
			if ([appDelegate.q19Answer17 isEqualToString:@"0"]) {
				appDelegate.q19Answer17 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer17 = @"0";
				check = NO;
			}
			break;
		case 18:
			if ([appDelegate.q19Answer18 isEqualToString:@"0"]) {
				appDelegate.q19Answer18 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer18 = @"0";
				check = NO;
			}
			break;
		case 19:
			if ([appDelegate.q19Answer19 isEqualToString:@"0"]) {
				appDelegate.q19Answer19 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer19 = @"0";
				check = NO;
			}
			break;
		case 20:
			if ([appDelegate.q19Answer20 isEqualToString:@"0"]) {
				appDelegate.q19Answer20 = @"1";
				check = YES;
			} else {
				appDelegate.q19Answer20 = @"0";
				check = NO;
			}
			break;
	}
	
	if (check) {
		[button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	} else {
		[button setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
}

- (void)q20AnswerButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	BOOL check = YES;
	
	switch (button.tag) {
		case 1:
			if ([appDelegate.q20Answer01 isEqualToString:@"0"]) {
				appDelegate.q20Answer01 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer01 = @"0";
				check = NO;
			}
			break;
		case 2:
			if ([appDelegate.q20Answer02 isEqualToString:@"0"]) {
				appDelegate.q20Answer02 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer02 = @"0";
				check = NO;
			}
			break;
		case 3:
			if ([appDelegate.q20Answer03 isEqualToString:@"0"]) {
				appDelegate.q20Answer03 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer03 = @"0";
				check = NO;
			}
			break;
		case 4:
			if ([appDelegate.q20Answer04 isEqualToString:@"0"]) {
				appDelegate.q20Answer04 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer04 = @"0";
				check = NO;
			}
			break;
		case 5:
			if ([appDelegate.q20Answer05 isEqualToString:@"0"]) {
				appDelegate.q20Answer05 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer05 = @"0";
				check = NO;
			}
			break;
		case 6:
			if ([appDelegate.q20Answer06 isEqualToString:@"0"]) {
				appDelegate.q20Answer06 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer06 = @"0";
				check = NO;
			}
			break;
		case 7:
			if ([appDelegate.q20Answer07 isEqualToString:@"0"]) {
				appDelegate.q20Answer07 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer07 = @"0";
				check = NO;
			}
			break;
		case 8:
			if ([appDelegate.q20Answer08 isEqualToString:@"0"]) {
				appDelegate.q20Answer08 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer08 = @"0";
				check = NO;
			}
			break;
		case 9:
			if ([appDelegate.q20Answer09 isEqualToString:@"0"]) {
				appDelegate.q20Answer09 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer09 = @"0";
				check = NO;
			}
			break;
		case 10:
			if ([appDelegate.q20Answer10 isEqualToString:@"0"]) {
				appDelegate.q20Answer10 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer10 = @"0";
				check = NO;
			}
			break;
		case 11:
			if ([appDelegate.q20Answer11 isEqualToString:@"0"]) {
				appDelegate.q20Answer11 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer11 = @"0";
				check = NO;
			}
			break;
		case 12:
			if ([appDelegate.q20Answer12 isEqualToString:@"0"]) {
				appDelegate.q20Answer12 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer12 = @"0";
				check = NO;
			}
			break;
		case 13:
			if ([appDelegate.q20Answer13 isEqualToString:@"0"]) {
				appDelegate.q20Answer13 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer13 = @"0";
				check = NO;
			}
			break;
		case 14:
			if ([appDelegate.q20Answer14 isEqualToString:@"0"]) {
				appDelegate.q20Answer14 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer14 = @"0";
				check = NO;
			}
			break;
		case 15:
			if ([appDelegate.q20Answer15 isEqualToString:@"0"]) {
				appDelegate.q20Answer15 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer15 = @"0";
				check = NO;
			}
			break;
		case 16:
			if ([appDelegate.q20Answer16 isEqualToString:@"0"]) {
				appDelegate.q20Answer16 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer16 = @"0";
				check = NO;
			}
			break;
		case 17:
			if ([appDelegate.q20Answer17 isEqualToString:@"0"]) {
				appDelegate.q20Answer17 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer17 = @"0";
				check = NO;
			}
			break;
		case 18:
			if ([appDelegate.q20Answer18 isEqualToString:@"0"]) {
				appDelegate.q20Answer18 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer18 = @"0";
				check = NO;
			}
			break;
		case 19:
			if ([appDelegate.q20Answer19 isEqualToString:@"0"]) {
				appDelegate.q20Answer19 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer19 = @"0";
				check = NO;
			}
			break;
		case 20:
			if ([appDelegate.q20Answer20 isEqualToString:@"0"]) {
				appDelegate.q20Answer20 = @"1";
				check = YES;
			} else {
				appDelegate.q20Answer20 = @"0";
				check = NO;
			}
			break;
	}
	
	if (check) {
		[button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	} else {
		[button setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	if (_questionNumber == 19) {
		if ([appDelegate.q19Answer01 isEqualToString:@"0"] &&
			[appDelegate.q19Answer02 isEqualToString:@"0"] &&
			[appDelegate.q19Answer03 isEqualToString:@"0"] &&
			[appDelegate.q19Answer04 isEqualToString:@"0"] &&
			[appDelegate.q19Answer05 isEqualToString:@"0"] &&
			[appDelegate.q19Answer06 isEqualToString:@"0"] &&
			[appDelegate.q19Answer07 isEqualToString:@"0"] &&
			[appDelegate.q19Answer08 isEqualToString:@"0"] &&
			[appDelegate.q19Answer09 isEqualToString:@"0"] &&
			[appDelegate.q19Answer10 isEqualToString:@"0"] &&
			[appDelegate.q19Answer11 isEqualToString:@"0"] &&
			[appDelegate.q19Answer12 isEqualToString:@"0"] &&
			[appDelegate.q19Answer13 isEqualToString:@"0"] &&
			[appDelegate.q19Answer14 isEqualToString:@"0"] &&
			[appDelegate.q19Answer15 isEqualToString:@"0"] &&
			[appDelegate.q19Answer16 isEqualToString:@"0"] &&
			[appDelegate.q19Answer17 isEqualToString:@"0"] &&
			[appDelegate.q19Answer18 isEqualToString:@"0"] &&
			[appDelegate.q19Answer19 isEqualToString:@"0"] &&
			[appDelegate.q19Answer20 isEqualToString:@"0"]) {
			[self showAlert:@"必須項目" message:@"Q19の回答を選択してください"];
			return;
		}
	} else if (_questionNumber == 20) {
		if ([appDelegate.q20Answer01 isEqualToString:@"0"] &&
			[appDelegate.q20Answer02 isEqualToString:@"0"] &&
			[appDelegate.q20Answer03 isEqualToString:@"0"] &&
			[appDelegate.q20Answer04 isEqualToString:@"0"] &&
			[appDelegate.q20Answer05 isEqualToString:@"0"] &&
			[appDelegate.q20Answer06 isEqualToString:@"0"] &&
			[appDelegate.q20Answer07 isEqualToString:@"0"] &&
			[appDelegate.q20Answer08 isEqualToString:@"0"] &&
			[appDelegate.q20Answer09 isEqualToString:@"0"] &&
			[appDelegate.q20Answer10 isEqualToString:@"0"] &&
			[appDelegate.q20Answer11 isEqualToString:@"0"] &&
			[appDelegate.q20Answer12 isEqualToString:@"0"] &&
			[appDelegate.q20Answer13 isEqualToString:@"0"] &&
			[appDelegate.q20Answer14 isEqualToString:@"0"] &&
			[appDelegate.q20Answer15 isEqualToString:@"0"] &&
			[appDelegate.q20Answer16 isEqualToString:@"0"] &&
			[appDelegate.q20Answer17 isEqualToString:@"0"] &&
			[appDelegate.q20Answer18 isEqualToString:@"0"] &&
			[appDelegate.q20Answer19 isEqualToString:@"0"] &&
			[appDelegate.q20Answer20 isEqualToString:@"0"]) {
			[self showAlert:@"必須項目" message:@"Q20の回答を選択してください"];
			return;
		}
	}
	
	if (_questionNumber == 19) {
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		id questions = [defaults objectForKey:@"questions"];
		if (questions[@"q20"]) {
			if ([questions[@"q20"][@"type"] isEqualToString:@"1"]) {
				[self nextPageSingle];
			} else {
				[self nextPageMulti];
			}
		} else {
			[self nextPageInputName];
		}
	} else if (_questionNumber == 20) {
		[self nextPageInputName];
	}
}

- (void)nextPageSingle {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	QuestionSingleViewController *viewController = [[QuestionSingleViewController alloc] init];
	viewController.questionNumber = 20;
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (void)nextPageMulti {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	QuestionMultiViewController *viewController = [[QuestionMultiViewController alloc] init];
	viewController.questionNumber = 20;
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (void)nextPageInputName {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	InputNameViewController *viewController = [[InputNameViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中のアンケート内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[UtilityFunction resetPersonalInfo];
		QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
