//
//  RegisterViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController <UIScrollViewDelegate> {
	NSString*		_showPublicInfo;
	
	UILabel*		_headerLabel;

	UILabel*		_nameLabel;
	UILabel*		_nameKanaLabel;
	UILabel*		_zipCodeLabel;;
	UILabel*		_address1Label;
	UILabel*		_address2Label;
	UILabel*		_address3Label;
	UILabel*		_phoneLabel;
	UILabel*		_emailLabel;

	UIButton*		_nextButton;
	UIButton*		_backButton;
	UIButton*		_checkBox1Button;
	UIButton*		_checkBox2Button;
	
	UILabel*		_label1;
	UILabel*		_label2;
	UILabel*		_label3;
	UILabel*		_label4;
	UIImageView*	_imageView;
	
	BOOL			_checkStatus1;
	BOOL			_checkStatus2;
	BOOL			_checkStatus3;
}

@property (nonatomic) NSString* showPublicInfo;

@property (nonatomic) IBOutlet UILabel* headerLabel;

@property (nonatomic) IBOutlet UILabel* nameLabel;
@property (nonatomic) IBOutlet UILabel* nameKanaLabel;
@property (nonatomic) IBOutlet UILabel* zipCodeLabel;
@property (nonatomic) IBOutlet UILabel* address1Label;
@property (nonatomic) IBOutlet UILabel* address2Label;
@property (nonatomic) IBOutlet UILabel* address3Label;
@property (nonatomic) IBOutlet UILabel* phoneLabel;
@property (nonatomic) IBOutlet UILabel* emailLabel;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;
@property (nonatomic) IBOutlet UIButton* checkBox1Button;
@property (nonatomic) IBOutlet UIButton* checkBox2Button;

@property (nonatomic) IBOutlet UILabel* label1;
@property (nonatomic) IBOutlet UILabel* label2;
@property (nonatomic) IBOutlet UILabel* label3;
@property (nonatomic) IBOutlet UILabel* label4;
@property (nonatomic) IBOutlet UIImageView* imageView;

- (IBAction)checkButton1Tapped:(UIButton*)button;
- (IBAction)checkButton2Tapped:(UIButton*)button;
- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
