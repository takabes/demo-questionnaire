//
//  QuestionSingleViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionSingleViewController : UIViewController {
	NSUInteger		_questionNumber;
	
	UILabel*		_headerLabel;

	UILabel*		_questionTitleLabel;

	UIButton*		_qAnswer01Button;
	UIButton*		_qAnswer02Button;
	UIButton*		_qAnswer03Button;
	UIButton*		_qAnswer04Button;
	UIButton*		_qAnswer05Button;
	UIButton*		_qAnswer06Button;
	UIButton*		_qAnswer07Button;
	UIButton*		_qAnswer08Button;
	UIButton*		_qAnswer09Button;
	UIButton*		_qAnswer103utton;
	UIButton*		_qAnswer11Button;
	UIButton*		_qAnswer12Button;
	UIButton*		_qAnswer13Button;
	UIButton*		_qAnswer14Button;
	UIButton*		_qAnswer15Button;
	UIButton*		_qAnswer16Button;
	UIButton*		_qAnswer17Button;
	UIButton*		_qAnswer18Button;
	UIButton*		_qAnswer19Button;
	UIButton*		_qAnswer203utton;
	
	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) NSUInteger questionNumber;

@property (nonatomic) IBOutlet UILabel* headerLabel;

@property (nonatomic) IBOutlet UILabel* questionTitleLabel;

@property (nonatomic) IBOutlet UIButton* qAnswer01Button;
@property (nonatomic) IBOutlet UIButton* qAnswer02Button;
@property (nonatomic) IBOutlet UIButton* qAnswer03Button;
@property (nonatomic) IBOutlet UIButton* qAnswer04Button;
@property (nonatomic) IBOutlet UIButton* qAnswer05Button;
@property (nonatomic) IBOutlet UIButton* qAnswer06Button;
@property (nonatomic) IBOutlet UIButton* qAnswer07Button;
@property (nonatomic) IBOutlet UIButton* qAnswer08Button;
@property (nonatomic) IBOutlet UIButton* qAnswer09Button;
@property (nonatomic) IBOutlet UIButton* qAnswer10Button;
@property (nonatomic) IBOutlet UIButton* qAnswer11Button;
@property (nonatomic) IBOutlet UIButton* qAnswer12Button;
@property (nonatomic) IBOutlet UIButton* qAnswer13Button;
@property (nonatomic) IBOutlet UIButton* qAnswer14Button;
@property (nonatomic) IBOutlet UIButton* qAnswer15Button;
@property (nonatomic) IBOutlet UIButton* qAnswer16Button;
@property (nonatomic) IBOutlet UIButton* qAnswer17Button;
@property (nonatomic) IBOutlet UIButton* qAnswer18Button;
@property (nonatomic) IBOutlet UIButton* qAnswer19Button;
@property (nonatomic) IBOutlet UIButton* qAnswer20Button;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
