//
//  ConfirmViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "ConfirmViewController.h"
#import "RegisterViewController.h"
#import "InputNameViewController.h"
#import "InputNameKanaViewController.h"
#import "InputZipCodeViewController.h"
#import "InputAddressViewController.h"
#import "InputPhoneViewController.h"
#import "InputEmailViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "AppConsts.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"

@interface ConfirmViewController ()
@end

@implementation ConfirmViewController

@synthesize nameLabel = _nameLabel;
@synthesize nameKanaLabel = _nameKanaLabel;
@synthesize zipCodeLabel = _zipCodeLabel;
@synthesize address1Label = _address1Label;
@synthesize address2Label = _address2Label;
@synthesize address3Label = _address3Label;
@synthesize phoneLabel = _phoneLabel;
@synthesize emailLabel = _emailLabel;

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;
@synthesize editNameButton = _editNameButton;
@synthesize editNameKanaButton = _editNameKanaButton;
@synthesize editZipCodeButton = _editZipCodeButton;
@synthesize editAddress1Button = _editAddress1Button;
@synthesize editAddress2Button = _editAddress2Button;
@synthesize editAddress3Button = _editAddress3Button;
@synthesize editPhoneButton = _editPhoneButton;
@synthesize editEmailButton = _editEmailButton;

- (id)init {
    self = [super initWithNibName:@"Confirm" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"確認画面";

    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
	
	[UtilityFunction customizeButton:_editNameButton];
	[UtilityFunction customizeButton:_editNameKanaButton];
	[UtilityFunction customizeButton:_editZipCodeButton];
	[UtilityFunction customizeButton:_editAddress1Button];
	[UtilityFunction customizeButton:_editAddress2Button];
	[UtilityFunction customizeButton:_editAddress3Button];
	[UtilityFunction customizeButton:_editPhoneButton];
	[UtilityFunction customizeButton:_editEmailButton];

	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	_nameLabel.text = [NSString stringWithFormat:@"%@　%@", appDelegate.lastName, appDelegate.firstName];
	_nameKanaLabel.text = [NSString stringWithFormat:@"%@　%@", appDelegate.lastNameKana, appDelegate.firstNameKana];
	_zipCodeLabel.text = appDelegate.zipCode;
	_address1Label.text = [NSString stringWithFormat:@"%@%@%@", appDelegate.prefecture, appDelegate.address1, appDelegate.address2];
	_address2Label.text = appDelegate.address3;
	_address3Label.text = appDelegate.address4;
	_phoneLabel.text = appDelegate.phone;
	if ([appDelegate.emailAccount length] && [appDelegate.emailDomain length]) {
		_emailLabel.text = [NSString stringWithFormat:@"%@@%@", appDelegate.emailAccount, appDelegate.emailDomain];
	} else {
		_emailLabel.text = @"";
	}
	
	[super viewWillAppear:animated];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (IBAction)nextButtonTapped:(UIButton*)button {

	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	RegisterViewController *viewController = [[RegisterViewController alloc] init];

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	if ([appDelegate.showPublicInfo isEqualToString:@"1"]) {
		viewController.showPublicInfo = @"1";
	} else {
		viewController.showPublicInfo = @"0";
		appDelegate.publicInfo = @"1";
	}

	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];

//	[self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)editButtonTapped:(UIButton*)button {
	_Log(@"editButtonTapped");
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;

	switch (button.tag) {
		case 1:
		{
			InputNameViewController *viewController = [[InputNameViewController alloc] init];
			viewController.edit = YES;
			[self.navigationController.view.layer addAnimation:transition forKey:nil];
			[self.navigationController pushViewController:viewController animated:NO];
		}
			break;

		case 2:
		{
			InputNameKanaViewController *viewController = [[InputNameKanaViewController alloc] init];
			viewController.edit = YES;
			[self.navigationController.view.layer addAnimation:transition forKey:nil];
			[self.navigationController pushViewController:viewController animated:NO];
		}
			break;

		case 3:
		{
			InputZipCodeViewController *viewController = [[InputZipCodeViewController alloc] init];
			viewController.edit = YES;
			[self.navigationController.view.layer addAnimation:transition forKey:nil];
			[self.navigationController pushViewController:viewController animated:NO];
		}
			break;

		case 4:
		case 5:
		case 6:
		{
			InputAddressViewController *viewController = [[InputAddressViewController alloc] init];
			viewController.edit = YES;
			if (button.tag == 4) {
				viewController.cursor = 1;
			} else if (button.tag == 5) {
				viewController.cursor = 4;
			} else if (button.tag == 6) {
				viewController.cursor = 5;
			}
			[self.navigationController.view.layer addAnimation:transition forKey:nil];
			[self.navigationController pushViewController:viewController animated:NO];
		}
			break;

		case 7:
		{
			InputPhoneViewController *viewController = [[InputPhoneViewController alloc] init];
			viewController.edit = YES;
			[self.navigationController.view.layer addAnimation:transition forKey:nil];
			[self.navigationController pushViewController:viewController animated:NO];
		}
			break;

		case 8:
		{
			InputEmailViewController *viewController = [[InputEmailViewController alloc] init];
			viewController.edit = YES;
			[self.navigationController.view.layer addAnimation:transition forKey:nil];
			[self.navigationController pushViewController:viewController animated:NO];
		}
			break;

		default:
			break;
	}
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中の内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		[UtilityFunction resetPersonalInfo];
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}
		
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
