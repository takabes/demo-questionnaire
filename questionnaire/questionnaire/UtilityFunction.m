//
//  UtilityFunction.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "FMDB.h"

@interface UtilityFunction ()
@end

@implementation UtilityFunction

// カラーコード変更
+ (UIColor*)colorWithRBGA:(unsigned int)rgba {
	unsigned char red = rgba >> 16 & 0xFF;
	unsigned char green = rgba >> 8 & 0xFF;
	unsigned char blue = rgba & 0xFF;
	return [UIColor colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:255.0 / 255.0];
}

// ボタンカスタマイズ
+ (void)customizeButton:(UIButton*)button {
	button.layer.borderColor = [UIColor blackColor].CGColor;
	button.layer.borderWidth = 1.5f;
	button.layer.cornerRadius = 7.5f;
	[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[button setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
}

// ボタンカスタマイズ
+ (void)customizeButton2:(UIButton*)button {
	button.layer.borderColor = [self colorWithRBGA:0x167efb].CGColor;
	button.layer.borderWidth = 1.5f;
	button.layer.cornerRadius = 7.5f;
	[button setTitleColor:[self colorWithRBGA:0x167efb] forState:UIControlStateNormal];
	[button setBackgroundColor:[UIColor whiteColor]];
}

// キーボードボタンカスタマイズ
+ (void)customizeKeyboardButton:(UIButton*)button {
	button.layer.borderColor = [UIColor grayColor].CGColor;
	button.layer.borderWidth = 1.0f;
	button.layer.cornerRadius = 7.5f;
	if (button.tag != 999) {
		[button.titleLabel setFont:[UIFont systemFontOfSize:25]];
	}
}

// 文字取得
+ (NSString*)getCharacter:(NSUInteger)tag {
	
	switch (tag) {
		case 0:
			return @"0";
			break;
		case 1:
			return @"1";
			break;
		case 2:
			return @"2";
			break;
		case 3:
			return @"3";
			break;
		case 4:
			return @"4";
			break;
		case 5:
			return @"5";
			break;
		case 6:
			return @"6";
			break;
		case 7:
			return @"7";
			break;
		case 8:
			return @"8";
			break;
		case 9:
			return @"9";
			break;
		case 10:
			return @"q";
			break;
		case 11:
			return @"w";
			break;
		case 12:
			return @"e";
			break;
		case 13:
			return @"r";
			break;
		case 14:
			return @"t";
			break;
		case 15:
			return @"y";
			break;
		case 16:
			return @"u";
			break;
		case 17:
			return @"i";
			break;
		case 18:
			return @"o";
			break;
		case 19:
			return @"p";
			break;
		case 20:
			return @"a";
			break;
		case 21:
			return @"s";
			break;
		case 22:
			return @"d";
			break;
		case 23:
			return @"f";
			break;
		case 24:
			return @"g";
			break;
		case 25:
			return @"h";
			break;
		case 26:
			return @"j";
			break;
		case 27:
			return @"k";
			break;
		case 28:
			return @"l";
			break;
		case 29:
			return @"z";
			break;
		case 30:
			return @"x";
			break;
		case 31:
			return @"c";
			break;
		case 32:
			return @"v";
			break;
		case 33:
			return @"b";
			break;
		case 34:
			return @"n";
			break;
		case 35:
			return @"m";
			break;
		case 100:
			return @"-";
			break;
		case 101:
			return @".";
			break;
		case 102:
			return @"_";
			break;
		default:
			break;
	}
	return @"";
}

// 個人情報リセット
+ (void)resetPersonalInfo {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.regular = @"";
	appDelegate.serialNumber = @"";
	appDelegate.lastName = @"";
	appDelegate.firstName = @"";
	appDelegate.lastNameKana = @"";
	appDelegate.firstNameKana = @"";
	appDelegate.zipCode = @"";
	appDelegate.prefecture = @"";
	appDelegate.address1 = @"";
	appDelegate.address2 = @"";
	appDelegate.address3 = @"";
	appDelegate.address4 = @"";
	appDelegate.phone = @"";
	appDelegate.emailAccount = @"";
	appDelegate.emailDomain = @"";
	appDelegate.directMail = @"";
	appDelegate.mailMagazie = @"";
	appDelegate.publicInfo = @"";
	appDelegate.selectedPrefecture = 0;
	appDelegate.otherPrefecture = @"";
	
	appDelegate.q1Answer01 = @"0";
	appDelegate.q1Answer02 = @"0";
	appDelegate.q1Answer03 = @"0";

	appDelegate.q2Answer01 = @"0";
	appDelegate.q2Answer02 = @"0";
	appDelegate.q2Answer03 = @"0";
	appDelegate.q2Answer04 = @"0";
	appDelegate.q2Answer05 = @"0";
	appDelegate.q2Answer20 = @"0";

	appDelegate.q3Answer01 = @"0";
	appDelegate.q3Answer02 = @"0";
	appDelegate.q3Answer03 = @"0";
	appDelegate.q3Answer04 = @"0";
	appDelegate.q3Answer05 = @"0";
	appDelegate.q3Answer06 = @"0";
	appDelegate.q3Answer07 = @"0";
	appDelegate.q3Answer08 = @"0";
	appDelegate.q3Answer09 = @"0";
	appDelegate.q3Answer10 = @"0";
	appDelegate.q3Answer11 = @"0";
	appDelegate.q3Answer12 = @"0";
	appDelegate.q3Answer13 = @"0";
	appDelegate.q3Answer14 = @"0";
	appDelegate.q3Answer15 = @"0";
	appDelegate.q3Answer16 = @"0";
	appDelegate.q3Answer17 = @"0";
	appDelegate.q3Answer18 = @"0";
	appDelegate.q3Answer19 = @"0";
	appDelegate.q3Answer20 = @"0";

	appDelegate.q4Answer01 = @"0";
	appDelegate.q4Answer02 = @"0";
	appDelegate.q4Answer03 = @"0";
	appDelegate.q4Answer04 = @"0";
	appDelegate.q4Answer05 = @"0";
	appDelegate.q4Answer06 = @"0";
	appDelegate.q4Answer07 = @"0";
	appDelegate.q4Answer08 = @"0";
	appDelegate.q4Answer09 = @"0";
	appDelegate.q4Answer10 = @"0";
	appDelegate.q4Answer11 = @"0";
	appDelegate.q4Answer12 = @"0";
	appDelegate.q4Answer20 = @"0";

	appDelegate.q5Answer01 = @"0";
	appDelegate.q5Answer02 = @"0";
	appDelegate.q5Answer03 = @"0";
	appDelegate.q5Answer04 = @"0";
	appDelegate.q5Answer20 = @"0";
	appDelegate.q5Answer02Text = @"";

	appDelegate.q6Answer01 = @"0";
	appDelegate.q6Answer02 = @"0";
	appDelegate.q6Answer03 = @"0";
	appDelegate.q6Answer04 = @"0";
	
	appDelegate.q7Answer01 = @"0";
	appDelegate.q7Answer02 = @"0";
	appDelegate.q7Answer03 = @"0";
	appDelegate.q7Answer04 = @"0";
	appDelegate.q7Answer01Text = @"";

	appDelegate.q8Answer01 = @"0";
	appDelegate.q8Answer02 = @"0";
	appDelegate.q8Answer03 = @"0";
	appDelegate.q8Answer04 = @"0";
	appDelegate.q8Answer05 = @"0";
	appDelegate.q8Answer06 = @"0";
	appDelegate.q8Answer20 = @"0";

	appDelegate.q9Answer01 = @"0";
	appDelegate.q9Answer02 = @"0";
	appDelegate.q9Answer03 = @"0";
	appDelegate.q9Answer04 = @"0";
	appDelegate.q9Answer05 = @"0";
	appDelegate.q9Answer06 = @"0";
	appDelegate.q9Answer07 = @"0";

	appDelegate.q10Answer01 = @"0";
	appDelegate.q10Answer02 = @"0";
	appDelegate.q10Answer03 = @"0";
	appDelegate.q10Answer04 = @"0";
	appDelegate.q10Answer05 = @"0";
	appDelegate.q10Answer06 = @"0";
	appDelegate.q10Answer07 = @"0";
	appDelegate.q10Answer08 = @"0";
	appDelegate.q10Answer09 = @"0";
	appDelegate.q10Answer10 = @"0";

	appDelegate.q11Answer01 = @"0";
	appDelegate.q11Answer02 = @"0";
	appDelegate.q11Answer03 = @"0";

	appDelegate.q12Answer01 = @"0";
	appDelegate.q12Answer02 = @"0";
	appDelegate.q12Answer03 = @"0";
	appDelegate.q12Answer04 = @"0";
	appDelegate.q12Answer05 = @"0";
	appDelegate.q12Answer06 = @"0";
	
	appDelegate.q13Answer01 = @"0";
	appDelegate.q13Answer02 = @"0";
	appDelegate.q13Answer03 = @"0";
	appDelegate.q13Answer04 = @"0";
	appDelegate.q13Answer05 = @"0";
	appDelegate.q13Answer06 = @"0";
	appDelegate.q13Answer07 = @"0";
	appDelegate.q13Answer08 = @"0";
	appDelegate.q13Answer09 = @"0";
	appDelegate.q13Answer10 = @"0";
	appDelegate.q13Answer11 = @"0";
	appDelegate.q13Answer12 = @"0";
	appDelegate.q13Answer13 = @"0";
	appDelegate.q13Answer14 = @"0";
	appDelegate.q13Answer15 = @"0";
	appDelegate.q13Answer16 = @"0";
	appDelegate.q13Answer17 = @"0";
	appDelegate.q13Answer18 = @"0";
	appDelegate.q13Answer19 = @"0";
	appDelegate.q13Answer20 = @"0";

	appDelegate.q14Answer01 = @"0";
	appDelegate.q14Answer02 = @"0";
	appDelegate.q14Answer03 = @"0";
	appDelegate.q14Answer04 = @"0";
	appDelegate.q14Answer05 = @"0";

	appDelegate.q15Answer01 = @"0";
	appDelegate.q15Answer02 = @"0";
	appDelegate.q15Answer03 = @"0";
	appDelegate.q15Answer04 = @"0";
	appDelegate.q15Answer05 = @"0";
	
	appDelegate.q16Answer01 = @"0";
	appDelegate.q16Answer02 = @"0";
	appDelegate.q16Answer03 = @"0";
	appDelegate.q16Answer04 = @"0";
	appDelegate.q16Answer05 = @"0";
	appDelegate.q16Answer20 = @"0";
	
	appDelegate.q17Answer01 = @"0";
	appDelegate.q17Answer02 = @"0";
	appDelegate.q17Answer03 = @"0";
	appDelegate.q17Answer20 = @"0";
	
	appDelegate.q18Answer01 = @"0";
	appDelegate.q18Answer02 = @"0";
	appDelegate.q18Answer03 = @"0";
	appDelegate.q18Answer04 = @"0";
	appDelegate.q18Answer05 = @"0";
	appDelegate.q18Answer06 = @"0";
	appDelegate.q18Answer07 = @"0";
	appDelegate.q18Answer08 = @"0";
	appDelegate.q18Answer09 = @"0";
	
	appDelegate.q19Answer01 = @"0";
	appDelegate.q19Answer02 = @"0";
	appDelegate.q19Answer03 = @"0";
	appDelegate.q19Answer04 = @"0";
	appDelegate.q19Answer05 = @"0";
	appDelegate.q19Answer06 = @"0";
	appDelegate.q19Answer07 = @"0";
	appDelegate.q19Answer08 = @"0";
	appDelegate.q19Answer09 = @"0";
	appDelegate.q19Answer10 = @"0";
	appDelegate.q19Answer11 = @"0";
	appDelegate.q19Answer12 = @"0";
	appDelegate.q19Answer13 = @"0";
	appDelegate.q19Answer14 = @"0";
	appDelegate.q19Answer15 = @"0";
	appDelegate.q19Answer16 = @"0";
	appDelegate.q19Answer17 = @"0";
	appDelegate.q19Answer18 = @"0";
	appDelegate.q19Answer19 = @"0";
	appDelegate.q19Answer20 = @"0";

	appDelegate.q20Answer01 = @"0";
	appDelegate.q20Answer02 = @"0";
	appDelegate.q20Answer03 = @"0";
	appDelegate.q20Answer04 = @"0";
	appDelegate.q20Answer05 = @"0";
	appDelegate.q20Answer06 = @"0";
	appDelegate.q20Answer07 = @"0";
	appDelegate.q20Answer08 = @"0";
	appDelegate.q20Answer09 = @"0";
	appDelegate.q20Answer10 = @"0";
	appDelegate.q20Answer11 = @"0";
	appDelegate.q20Answer12 = @"0";
	appDelegate.q20Answer13 = @"0";
	appDelegate.q20Answer14 = @"0";
	appDelegate.q20Answer15 = @"0";
	appDelegate.q20Answer16 = @"0";
	appDelegate.q20Answer17 = @"0";
	appDelegate.q20Answer18 = @"0";
	appDelegate.q20Answer19 = @"0";
	appDelegate.q20Answer20 = @"0";
}

+ (NSString*)getCurrentDate {

	// 現在日時を取得
	NSDate *now = [NSDate date];
 
	// NsDate→NSString変換用のフォーマッタを作成
	NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
	[outputFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
 
	// 日付から文字列に変換
	return [outputFormatter stringFromDate:now];
}

// 現在時刻ログ表示
+ (void)showNowDate {
	// ログ出力
	_Log(@"現在日時：%@", [self getCurrentDate]);
}

// アカウント情報保存
+ (void)saveAccounts:(NSMutableArray*)accounts {
	_Log(@"%@", accounts);
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
 
	// CREATE TABLE
	NSString* sql = @"CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id TEXT, password TEXT, place_code TEXT, show_public_info TEXT);";
	[db open];
	[db executeUpdate:sql];
	[db close];
	
	// DELETE TABLE
	sql = @"DELETE FROM user";
	[db open];
	[db executeUpdate:sql];
	[db close];
	
	for (int i = 0; i < [accounts count]; i++) {
		_Log(@"%@", [accounts objectAtIndex:i]);
		NSArray *items = [[accounts objectAtIndex:i] componentsSeparatedByString:@","];
		_Log(@"%@", items);
		
		// INSERT
		NSString* sql = @"INSERT INTO user (user_id, password, place_code, show_public_info) VALUES (?, ?, ?, ?)";
		[db open];
		[db executeUpdate:sql, [items objectAtIndex:0], [items objectAtIndex:1], [items objectAtIndex:2], [items objectAtIndex:3]];
		[db close];
	}
	
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:@"1" forKey:@"saveUser"];
	BOOL successful = [defaults synchronize];
	if (successful) {
		_Log(@"%@", @"データの保存に成功しました。");
	}
}

+ (NSMutableArray*)getPersonalInfos {
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
	
	NSString* sql = @"SELECT id, personal_info, delete_flag FROM answer WHERE delete_flag = '0';";
	
	[db open];
 
	FMResultSet* results = [db executeQuery:sql];
	_Log(@"%@", results);
	
	NSMutableArray* personalInfos = [[NSMutableArray alloc] init];
	
	while([results next]) {
		NSString* id = [results stringForColumnIndex:0];
		NSString* aa = [results stringForColumnIndex:1];
		NSString* bb = [results stringForColumnIndex:2];
		
		_Log(@"%@", id);
		_Log(@"%@", aa);
		_Log(@"%@", bb);
		[personalInfos addObject:[results stringForColumnIndex:1]];
	}
	_Log(@"personalInfos = %@", personalInfos);
	[db close];
	
	return personalInfos;
}

+ (void)deletePeronalInfos {
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
	
	NSString* sql = @"UPDATE answer SET delete_flag = '1' WHERE delete_flag = '0';";
	
	[db open];
	[db executeUpdate:sql];
	[db close];
}

+ (NSMutableArray*)getAddress:(NSString*)zipCode {
	
	NSMutableArray* array = [[NSMutableArray alloc] init];
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
	
	NSString* sql = @"SELECT id, prefecture, address1, address2 FROM zip_code WHERE code = ?;";
 
	[db open];
 
	FMResultSet* results = [db executeQuery:sql, zipCode];
	
	while([results next]) {
		[array addObject:[results stringForColumnIndex:1]];
		[array addObject:[results stringForColumnIndex:2]];
		[array addObject:[results stringForColumnIndex:3]];
	}
	
	[db close];
	
	return array;
}

// 数字のみか （引数は文字列なので注意）
+ (BOOL)chkNumeric:(NSString *)checkString
{
	@autoreleasepool
	{
		NSCharacterSet *digitCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
		bool ret = [self chkCompareString:checkString baseString:digitCharSet];
		return ret;
	}
}

+ (BOOL)chkCompareString:(NSString *)checkString baseString:(NSCharacterSet *)baseString
{
	@autoreleasepool
	{
		NSScanner *aScanner = [NSScanner localizedScannerWithString:checkString];
		// NSScannerはﾃﾞﾌｫﾙﾄでは前後のｽﾍﾟｰｽなどを読み飛ばしてくれるのだが､あえて-setCharactersToBeSkipped:でnilを渡して抑制している｡
		[aScanner setCharactersToBeSkipped:nil];
		
		[aScanner scanCharactersFromSet:baseString intoString:NULL];
		return [aScanner isAtEnd];
	}
}

// 全角文字ば含まれるか
+ (BOOL)isAllHalfWidthCharacter:(NSString*)checkString
{
	NSUInteger nsStringlen = [checkString length];
	const char *utf8 = [checkString UTF8String];
	size_t cStringlen = strlen(utf8);
	if (nsStringlen == cStringlen) {
		return YES;
	} else {
		return NO;
	}
}

// 全角チェック
+ (BOOL)isSerialNumber:(NSString*)checkString
{
	NSArray *items = [checkString componentsSeparatedByString:@"-"];

	if ([items count] != 3) {
		return NO;
	}

	for (int i = 0; i < [items count]; i++) {
		if (![self chkNumeric:[items objectAtIndex:i]]) {
			return NO;
		}
	}
	
	if ([[items objectAtIndex:0] length] != 3) {
		return NO;
	}

	if ([[items objectAtIndex:1] length] != 5) {
		return NO;
	}

	if ([[items objectAtIndex:2] length] != 1) {
		return NO;
	}

	return YES;
}

// 全角チェック
+ (BOOL)isZenkaku:(NSString*)checkString
{
	NSError* error = nil;
	NSRegularExpression* regex = nil;
	
	regex = [NSRegularExpression regularExpressionWithPattern:@"^[^\x01-\x7E]+$"
													  options:NSRegularExpressionCaseInsensitive
														error:&error];
	
	if (error != nil) {
		_Log(@"%@", error);
		return NO;
	}
	
	// 比較
	NSTextCheckingResult *match = [regex firstMatchInString:checkString
													options:0
													  range:NSMakeRange(0, checkString.length)];
	
	if (match) {
		// マッチした時の処理
		return YES;
	}
	return NO;
}

// 全角カタカナチェック
+ (BOOL)isZenkakuKatakana:(NSString*)checkString
{
	NSError* error = nil;
	NSRegularExpression* regex = nil;

	regex = [NSRegularExpression regularExpressionWithPattern:@"^[ァ-ー]+$"
													  options:NSRegularExpressionCaseInsensitive
														error:&error];
	
	if (error != nil) {
		_Log(@"%@", error);
		return NO;
	}

	// 比較
	NSTextCheckingResult *match = [regex firstMatchInString:checkString
													options:0
													  range:NSMakeRange(0, checkString.length)];
	
	if (match) {
		// マッチした時の処理
		return YES;
	}
	return NO;
}

@end
