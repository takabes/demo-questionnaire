//
//  PublickCheckViewController
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublickCheckViewController : UIViewController {
	UIButton*		_check01Button;
	UIButton*		_check02Button;

	NSString*		_publicInfo;

	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UIButton* check01Button;
@property (nonatomic) IBOutlet UIButton* check02Button;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)checkButtonTapped:(UIButton*)button;
- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
