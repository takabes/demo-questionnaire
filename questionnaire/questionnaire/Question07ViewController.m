//
//  Question07ViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "Question07ViewController.h"
#import "InputNameViewController.h"
#import "AppDelegate.h"
#import "QuestionSingleViewController.h"
#import "QuestionMultiViewController.h"
#import "UtilityFunction.h"
#import "QuestionListViewController.h"

@interface Question07ViewController ()
@end

@implementation Question07ViewController

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"Question07" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"アンケート入力";

    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	[self initHeaderLabel];
	
	[self initButton];

	[self initQ17Answer];
	[self initQ18Answer];
	
	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initHeaderLabel {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	if (questions[@"q19"]) {
		if (questions[@"q20"]) {
			_headerLabel.text = @"7 / 9";
		} else {
			_headerLabel.text = @"7 / 8";
		}
	} else {
		_headerLabel.text = @"7 / 7";
	}
}

- (void)initButton {
	
	[_q17Answer01Button addTarget:self action:@selector(q17AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q17Answer02Button addTarget:self action:@selector(q17AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q17Answer03Button addTarget:self action:@selector(q17AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q17Answer20Button addTarget:self action:@selector(q17AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[_q18Answer01Button addTarget:self action:@selector(q18AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q18Answer02Button addTarget:self action:@selector(q18AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q18Answer03Button addTarget:self action:@selector(q18AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q18Answer04Button addTarget:self action:@selector(q18AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q18Answer05Button addTarget:self action:@selector(q18AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q18Answer06Button addTarget:self action:@selector(q18AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q18Answer07Button addTarget:self action:@selector(q18AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q18Answer08Button addTarget:self action:@selector(q18AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q18Answer09Button addTarget:self action:@selector(q18AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
}

- (void)initQ17Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q17Answer01 isEqualToString:@"1"]) {
		[_q17Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q17Answer02 isEqualToString:@"1"]) {
		[_q17Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q17Answer03 isEqualToString:@"1"]) {
		[_q17Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q17Answer20 isEqualToString:@"1"]) {
		[_q17Answer20Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ18Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q18Answer01 isEqualToString:@"1"]) {
		[_q18Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q18Answer02 isEqualToString:@"1"]) {
		[_q18Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q18Answer03 isEqualToString:@"1"]) {
		[_q18Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q18Answer04 isEqualToString:@"1"]) {
		[_q18Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)q17AnswerButtonTapped:(UIButton*)button {
	
	[_q17Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q17Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q17Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q17Answer20Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q17Answer01 = @"0";
	appDelegate.q17Answer02 = @"0";
	appDelegate.q17Answer03 = @"0";
	appDelegate.q17Answer20 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q17Answer01 = @"1";
			break;
		case 2:
			appDelegate.q17Answer02 = @"1";
			break;
		case 3:
			appDelegate.q17Answer03 = @"1";
			break;
		case 20:
			appDelegate.q17Answer20 = @"1";
			break;
	}
}

- (void)q18AnswerButtonTapped:(UIButton*)button {
	
	[_q18Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q18Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q18Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q18Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q18Answer05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q18Answer06Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q18Answer07Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q18Answer08Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q18Answer09Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q18Answer01 = @"0";
	appDelegate.q18Answer02 = @"0";
	appDelegate.q18Answer03 = @"0";
	appDelegate.q18Answer04 = @"0";
	appDelegate.q18Answer05 = @"0";
	appDelegate.q18Answer06 = @"0";
	appDelegate.q18Answer07 = @"0";
	appDelegate.q18Answer08 = @"0";
	appDelegate.q18Answer09 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q18Answer01 = @"1";
			break;
		case 2:
			appDelegate.q18Answer02 = @"1";
			break;
		case 3:
			appDelegate.q18Answer03 = @"1";
			break;
		case 4:
			appDelegate.q18Answer04 = @"1";
			break;
		case 5:
			appDelegate.q18Answer05 = @"1";
			break;
		case 6:
			appDelegate.q18Answer06 = @"1";
			break;
		case 7:
			appDelegate.q18Answer07 = @"1";
			break;
		case 8:
			appDelegate.q18Answer08 = @"1";
			break;
		case 9:
			appDelegate.q18Answer09 = @"1";
			break;
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	NSString* message = @"";

	if ([appDelegate.q17Answer01 isEqualToString:@"0"] &&
		[appDelegate.q17Answer02 isEqualToString:@"0"] &&
		[appDelegate.q17Answer03 isEqualToString:@"0"] &&
		[appDelegate.q17Answer20 isEqualToString:@"0"]) {
		message = @"Q17の回答を選択してください";
	}

	if ([appDelegate.q18Answer01 isEqualToString:@"0"] &&
		[appDelegate.q18Answer02 isEqualToString:@"0"] &&
		[appDelegate.q18Answer03 isEqualToString:@"0"] &&
		[appDelegate.q18Answer04 isEqualToString:@"0"] &&
		[appDelegate.q18Answer05 isEqualToString:@"0"] &&
		[appDelegate.q18Answer06 isEqualToString:@"0"] &&
		[appDelegate.q18Answer07 isEqualToString:@"0"] &&
		[appDelegate.q18Answer08 isEqualToString:@"0"] &&
		[appDelegate.q18Answer09 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q18の回答を選択してください"];
		} else {
			message = @"Q18の回答を選択してください";
		}
	}
	
	if ([message length]) {
		[self showAlert:@"必須項目" message:message];
		return;
	}

	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	if (questions[@"q19"]) {
		if ([questions[@"q19"][@"type"] isEqualToString:@"1"]) {
			[self nextPageSingle];
		} else {
			[self nextPageMulti];
		}
	} else {
		[self nextPageInputName];
	}
}

- (void)nextPageSingle {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	QuestionSingleViewController *viewController = [[QuestionSingleViewController alloc] init];
	viewController.questionNumber = 19;
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (void)nextPageMulti {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	QuestionMultiViewController *viewController = [[QuestionMultiViewController alloc] init];
	viewController.questionNumber = 19;
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (void)nextPageInputName {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	InputNameViewController *viewController = [[InputNameViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中のアンケート内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[UtilityFunction resetPersonalInfo];
		QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
