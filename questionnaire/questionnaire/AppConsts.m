//
//  AppConsts.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "AppConsts.h"

@implementation AppConsts

#if DEF_MODE == VAL_MODE_ONLY_PERSONAL_INFO
// ------------------------------------
// 個人情報のみ
// ------------------------------------
NSUInteger const MODE = MODE_ONLY_PERSONAL_INFO;

#elif DEF_MODE == VAL_MODE_PERSONAL_IFNO_ANSWER
// ------------------------------------
// 個人情報 + アンケート回答
// ------------------------------------
NSUInteger const MODE = MODE_PERSONAL_IFNO_ANSWER;

#endif


#if DEF_ENVIRONMENT == VAL_ENV_PROD
#if DEF_MODE == VAL_MODE_ONLY_PERSONAL_INFO
// ------------------------------------
// 本番環境　(個人情報のみ)
// ------------------------------------

// スタッフアカウント取得API URL
NSString* const API_URL_GET_ACCOUNT = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete/getAccount.php";

// 郵便番号取得API URL
NSString* const API_URL_GET_ZIP_CODE = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete/getZipCode.php";

// 可変アンケート取得API URL
NSString* const API_URL_GET_QUESTIONS = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete/getQuestions.php";

// 個人情報登録API URL
NSString* const API_URL_REGISTER_PERSONAL_INFO = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete/registerPersonalInfo.php";

// 個人情報 + アンケート回答登録API URL
NSString* const API_URL_REGISTER_PERSONAL_INFO_ANSWER = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete/registerPersonalInfoAnswer.php";


#elif DEF_MODE == VAL_MODE_PERSONAL_IFNO_ANSWER
// ------------------------------------
// 本番環境 (個人情報 + アンケート回答)
// ------------------------------------

// スタッフアカウント取得API URL
NSString* const API_URL_GET_ACCOUNT = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete2/getAccount.php";

// 郵便番号取得API URL
NSString* const API_URL_GET_ZIP_CODE = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete2/getZipCode.php";

// 可変アンケート取得API URL
NSString* const API_URL_GET_QUESTIONS = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete2/getQuestions.php";

// 個人情報登録API URL
NSString* const API_URL_REGISTER_PERSONAL_INFO = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete2/registerPersonalInfo.php";

// 個人情報 + アンケート回答登録API URL
NSString* const API_URL_REGISTER_PERSONAL_INFO_ANSWER = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete2/registerPersonalInfoAnswer.php";

#endif


#elif DEF_ENVIRONMENT == VAL_ENV_DEV
// ------------------------------------
// 開発環境
// ------------------------------------

// スタッフアカウント取得API URL
NSString* const API_URL_GET_ACCOUNT = @"https://sanfuji:sanfuji@www.sanfuji-ank.com/enquete_test/getAccount.php";

// 郵便番号取得API URL
NSString* const API_URL_GET_ZIP_CODE = @"https://sanfuji:sanfuji@www.sanfuji-ank.com/enquete_test/getZipCode.php";

// 可変アンケート取得API URL
NSString* const API_URL_GET_QUESTIONS = @"https://sanfuji:sanfuji@www.sanfuji-ank.com/enquete_test/getQuestions.php";

// 個人情報登録API URL
NSString* const API_URL_REGISTER_PERSONAL_INFO = @"https://sanfuji:sanfuji@www.sanfuji-ank.com/enquete_test/registerPersonalInfo.php";

// 個人情報 + アンケート回答登録API URL
NSString* const API_URL_REGISTER_PERSONAL_INFO_ANSWER = @"http://sanfuji:sanfuji@www.sanfuji-ank.com/enquete_test/registerPersonalInfoAnswer.php";

#endif

@end