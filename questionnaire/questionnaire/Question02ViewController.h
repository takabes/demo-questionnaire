//
//  Question02ViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Question02ViewController : UIViewController <UITextFieldDelegate> {
	UILabel*		_headerLabel;

	UIButton*		_q4Answer01Button;
	UIButton*		_q4Answer02Button;
	UIButton*		_q4Answer03Button;
	UIButton*		_q4Answer04Button;
	UIButton*		_q4Answer05Button;
	UIButton*		_q4Answer06Button;
	UIButton*		_q4Answer07Button;
	UIButton*		_q4Answer08Button;
	UIButton*		_q4Answer09Button;
	UIButton*		_q4Answer10Button;
	UIButton*		_q4Answer11Button;
	UIButton*		_q4Answer12Button;
	UIButton*		_q4Answer20Button;
	
	UIButton*		_q5Answer01Button;
	UIButton*		_q5Answer02Button;
	UIButton*		_q5Answer03Button;
	UIButton*		_q5Answer04Button;
	UIButton*		_q5Answer20Button;

	UITextField*	_q5Answer02TextField;

	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UILabel* headerLabel;

@property (nonatomic) IBOutlet UIButton* q4Answer01Button;
@property (nonatomic) IBOutlet UIButton* q4Answer02Button;
@property (nonatomic) IBOutlet UIButton* q4Answer03Button;
@property (nonatomic) IBOutlet UIButton* q4Answer04Button;
@property (nonatomic) IBOutlet UIButton* q4Answer05Button;
@property (nonatomic) IBOutlet UIButton* q4Answer06Button;
@property (nonatomic) IBOutlet UIButton* q4Answer07Button;
@property (nonatomic) IBOutlet UIButton* q4Answer08Button;
@property (nonatomic) IBOutlet UIButton* q4Answer09Button;
@property (nonatomic) IBOutlet UIButton* q4Answer10Button;
@property (nonatomic) IBOutlet UIButton* q4Answer11Button;
@property (nonatomic) IBOutlet UIButton* q4Answer12Button;
@property (nonatomic) IBOutlet UIButton* q4Answer20Button;

@property (nonatomic) IBOutlet UIButton* q5Answer01Button;
@property (nonatomic) IBOutlet UIButton* q5Answer02Button;
@property (nonatomic) IBOutlet UIButton* q5Answer03Button;
@property (nonatomic) IBOutlet UIButton* q5Answer04Button;
@property (nonatomic) IBOutlet UIButton* q5Answer20Button;

@property (nonatomic) IBOutlet UITextField* q5Answer02TextField;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
