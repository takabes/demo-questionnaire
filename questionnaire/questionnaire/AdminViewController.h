//
//  AdminViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdminViewController : UIViewController {
	UILabel*		_dataCountLabel;
	UIButton*		_accountButton;
	UIButton*		_zipCodeButton;
	UIButton*		_registerButton;
	UIButton*		_questionButton;
	UIButton*		_closeButton;
	UILabel*		_versionLabel;
}

@property (nonatomic) IBOutlet UILabel* dataCountLabel;
@property (nonatomic) IBOutlet UIButton* accountButton;
@property (nonatomic) IBOutlet UIButton* zipCodeButton;
@property (nonatomic) IBOutlet UIButton* registerButton;
@property (nonatomic) IBOutlet UIButton* questionButton;
@property (nonatomic) IBOutlet UIButton* closeButton;
@property (nonatomic) IBOutlet UILabel* versionLabel;

- (IBAction)accountButtonTapped:(UIButton*)button;
- (IBAction)zipCodeButtonTapped:(UIButton*)button;
- (IBAction)registerButtonTapped:(UIButton*)button;
- (IBAction)questionButtonTapped:(UIButton*)button;
- (IBAction)closeButtonTapped:(UIButton*)button;

@end
