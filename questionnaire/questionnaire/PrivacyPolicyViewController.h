//
//  PrivacyPolicyViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrivacyPolicyViewController : UIViewController <UIScrollViewDelegate> {
	UIScrollView*	_scrollView;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)backButtonTapped:(UIButton*)button;

@end
