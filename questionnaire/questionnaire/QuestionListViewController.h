//
//  QuestionListViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/09.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionListViewController : UIViewController {
	id				_questions;
	
	UILabel*		_q19TitleLabel;
	UILabel*		_q20TitleLabel;

	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UILabel* q19TitleLabel;
@property (nonatomic) IBOutlet UILabel* q20TitleLabel;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
