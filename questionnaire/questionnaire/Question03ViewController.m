//
//  Question03ViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "Question03ViewController.h"
#import "Question04ViewController.h"
#import "AppDelegate.h"
#import "UtilityFunction.h"
#import "QuestionListViewController.h"

@interface Question03ViewController ()
@end

@implementation Question03ViewController

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"Question03" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"アンケート入力";

    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];
	
	[_q7Answer01TextField setDelegate:self];

	[self initHeaderLabel];
	
	[self initButton];

	[self initQ6Answer];
	[self initQ7Answer];
	[self initQ8Answer];
	[self initQ9Answer];
	
	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initHeaderLabel {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	if (questions[@"q19"]) {
		if (questions[@"q20"]) {
			_headerLabel.text = @"3 / 9";
		} else {
			_headerLabel.text = @"3 / 8";
		}
	} else {
		_headerLabel.text = @"3 / 7";
	}
}

- (void)initButton {
	
	[_q6Answer01Button addTarget:self action:@selector(q6AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q6Answer02Button addTarget:self action:@selector(q6AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q6Answer03Button addTarget:self action:@selector(q6AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q6Answer04Button addTarget:self action:@selector(q6AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[_q7Answer01Button addTarget:self action:@selector(q7AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q7Answer02Button addTarget:self action:@selector(q7AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q7Answer03Button addTarget:self action:@selector(q7AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q7Answer04Button addTarget:self action:@selector(q7AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_q8Answer01Button addTarget:self action:@selector(q8AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q8Answer02Button addTarget:self action:@selector(q8AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q8Answer03Button addTarget:self action:@selector(q8AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q8Answer04Button addTarget:self action:@selector(q8AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q8Answer05Button addTarget:self action:@selector(q8AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q8Answer06Button addTarget:self action:@selector(q8AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q8Answer20Button addTarget:self action:@selector(q8AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[_q9Answer01Button addTarget:self action:@selector(q9AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q9Answer02Button addTarget:self action:@selector(q9AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q9Answer03Button addTarget:self action:@selector(q9AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q9Answer04Button addTarget:self action:@selector(q9AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q9Answer05Button addTarget:self action:@selector(q9AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q9Answer06Button addTarget:self action:@selector(q9AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q9Answer07Button addTarget:self action:@selector(q9AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
}

- (void)initQ6Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q6Answer01 isEqualToString:@"1"]) {
		[_q6Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q6Answer02 isEqualToString:@"1"]) {
		[_q6Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q6Answer03 isEqualToString:@"1"]) {
		[_q6Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q6Answer04 isEqualToString:@"1"]) {
		[_q6Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ7Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q7Answer01 isEqualToString:@"1"]) {
		[_q7Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q7Answer02 isEqualToString:@"1"]) {
		[_q7Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q7Answer03 isEqualToString:@"1"]) {
		[_q7Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q7Answer04 isEqualToString:@"1"]) {
		[_q7Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ8Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q8Answer01 isEqualToString:@"1"]) {
		[_q8Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q8Answer02 isEqualToString:@"1"]) {
		[_q8Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q8Answer03 isEqualToString:@"1"]) {
		[_q8Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q8Answer04 isEqualToString:@"1"]) {
		[_q8Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q8Answer05 isEqualToString:@"1"]) {
		[_q8Answer05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q8Answer06 isEqualToString:@"1"]) {
		[_q8Answer06Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q8Answer20 isEqualToString:@"1"]) {
		[_q8Answer20Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ9Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q9Answer01 isEqualToString:@"1"]) {
		[_q9Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q9Answer02 isEqualToString:@"1"]) {
		[_q9Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q9Answer03 isEqualToString:@"1"]) {
		[_q9Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q9Answer04 isEqualToString:@"1"]) {
		[_q9Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q9Answer05 isEqualToString:@"1"]) {
		[_q9Answer05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q9Answer06 isEqualToString:@"1"]) {
		[_q9Answer06Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q9Answer07 isEqualToString:@"1"]) {
		[_q9Answer07Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)q6AnswerButtonTapped:(UIButton*)button {
	
	[_q7Answer01TextField resignFirstResponder];

	[_q6Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q6Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q6Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q6Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q6Answer01 = @"0";
	appDelegate.q6Answer02 = @"0";
	appDelegate.q6Answer03 = @"0";
	appDelegate.q6Answer04 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q6Answer01 = @"1";
			break;
		case 2:
			appDelegate.q6Answer02 = @"1";
			break;
		case 3:
			appDelegate.q6Answer03 = @"1";
			break;
		case 4:
			appDelegate.q6Answer04 = @"1";
			break;
	}
}

- (void)q7AnswerButtonTapped:(UIButton*)button {
	
	[_q7Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q7Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q7Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q7Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q7Answer01 = @"0";
	appDelegate.q7Answer02 = @"0";
	appDelegate.q7Answer03 = @"0";
	appDelegate.q7Answer04 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q7Answer01 = @"1";
			break;
		case 2:
			appDelegate.q7Answer02 = @"1";
			break;
		case 3:
			appDelegate.q7Answer03 = @"1";
			break;
		case 4:
			appDelegate.q7Answer04 = @"1";
			break;
	}
}

- (void)q8AnswerButtonTapped:(UIButton*)button {
	
	[_q7Answer01TextField resignFirstResponder];

	[_q8Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q8Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q8Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q8Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q8Answer05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q8Answer06Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q8Answer20Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q8Answer01 = @"0";
	appDelegate.q8Answer02 = @"0";
	appDelegate.q8Answer03 = @"0";
	appDelegate.q8Answer04 = @"0";
	appDelegate.q8Answer05 = @"0";
	appDelegate.q8Answer06 = @"0";
	appDelegate.q8Answer20 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q8Answer01 = @"1";
			break;
		case 2:
			appDelegate.q8Answer02 = @"1";
			break;
		case 3:
			appDelegate.q8Answer03 = @"1";
			break;
		case 4:
			appDelegate.q8Answer04 = @"1";
			break;
		case 5:
			appDelegate.q8Answer05 = @"1";
			break;
		case 6:
			appDelegate.q8Answer06 = @"1";
			break;
		case 20:
			appDelegate.q8Answer20 = @"1";
			break;
	}
}

- (void)q9AnswerButtonTapped:(UIButton*)button {
	
	[_q7Answer01TextField resignFirstResponder];

	[_q9Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q9Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q9Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q9Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q9Answer05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q9Answer06Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q9Answer07Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q9Answer01 = @"0";
	appDelegate.q9Answer02 = @"0";
	appDelegate.q9Answer03 = @"0";
	appDelegate.q9Answer04 = @"0";
	appDelegate.q9Answer05 = @"0";
	appDelegate.q9Answer06 = @"0";
	appDelegate.q9Answer07 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q9Answer01 = @"1";
			break;
		case 2:
			appDelegate.q9Answer02 = @"1";
			break;
		case 3:
			appDelegate.q9Answer03 = @"1";
			break;
		case 4:
			appDelegate.q9Answer04 = @"1";
			break;
		case 5:
			appDelegate.q9Answer05 = @"1";
			break;
		case 6:
			appDelegate.q9Answer06 = @"1";
			break;
		case 7:
			appDelegate.q9Answer07 = @"1";
			break;
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	NSString* message = @"";

	if ([appDelegate.q6Answer01 isEqualToString:@"0"] &&
		[appDelegate.q6Answer02 isEqualToString:@"0"] &&
		[appDelegate.q6Answer03 isEqualToString:@"0"] &&
		[appDelegate.q6Answer04 isEqualToString:@"0"]) {
		message = @"Q6の回答を選択してください";
	}

	if ([appDelegate.q7Answer01 isEqualToString:@"0"] &&
		[appDelegate.q7Answer02 isEqualToString:@"0"] &&
		[appDelegate.q7Answer03 isEqualToString:@"0"] &&
		[appDelegate.q7Answer04 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q7の回答を選択してください"];
		} else {
			message = @"Q7の回答を選択してください";
		}
	}
	
	if ([appDelegate.q7Answer01 isEqualToString:@"1"]) {
		if ([_q7Answer01TextField.text length]) {
			appDelegate.q7Answer01Text = _q7Answer01TextField.text;
		}
	}

	if ([appDelegate.q8Answer01 isEqualToString:@"0"] &&
		[appDelegate.q8Answer02 isEqualToString:@"0"] &&
		[appDelegate.q8Answer03 isEqualToString:@"0"] &&
		[appDelegate.q8Answer04 isEqualToString:@"0"] &&
		[appDelegate.q8Answer05 isEqualToString:@"0"] &&
		[appDelegate.q8Answer06 isEqualToString:@"0"] &&
		[appDelegate.q8Answer20 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q8の回答を選択してください"];
		} else {
			message = @"Q8の回答を選択してください";
		}
	}

	if ([appDelegate.q9Answer01 isEqualToString:@"0"] &&
		[appDelegate.q9Answer02 isEqualToString:@"0"] &&
		[appDelegate.q9Answer03 isEqualToString:@"0"] &&
		[appDelegate.q9Answer04 isEqualToString:@"0"] &&
		[appDelegate.q9Answer05 isEqualToString:@"0"] &&
		[appDelegate.q9Answer06 isEqualToString:@"0"] &&
		[appDelegate.q9Answer07 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q9の回答を選択してください"];
		} else {
			message = @"Q9の回答を選択してください";
		}
	}
	
	if ([message length]) {
		[self showAlert:@"必須項目" message:message];
		return;
	}

	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	Question04ViewController *viewController = [[Question04ViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

//--------------------------------------------------------------//
#pragma mark -- UITextFieldDelegate Delegate Methods --
//--------------------------------------------------------------//

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
	[textField resignFirstResponder];
	return YES;
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中のアンケート内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[UtilityFunction resetPersonalInfo];
		QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
