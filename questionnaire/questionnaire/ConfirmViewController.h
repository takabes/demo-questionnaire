//
//  ConfirmViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmViewController : UIViewController {
	UILabel*		_nameLabel;
	UILabel*		_nameKanaLabel;
	UILabel*		_zipCodeLabel;;
	UILabel*		_address1Label;
	UILabel*		_address2Label;
	UILabel*		_address3Label;
	UILabel*		_phoneLabel;
	UILabel*		_emailLabel;
	
	UIButton*		_nextButton;
	UIButton*		_backButton;
	UIButton*		_editNameButton;
	UIButton*		_editNameKanaButton;
	UIButton*		_editZipCodeButton;
	UIButton*		_editAddress1Button;
	UIButton*		_editAddress2Button;
	UIButton*		_editAddress3Button;
	UIButton*		_editPhoneButton;
	UIButton*		_editEmailButton;
}

@property (nonatomic) IBOutlet UILabel* nameLabel;
@property (nonatomic) IBOutlet UILabel* nameKanaLabel;
@property (nonatomic) IBOutlet UILabel* zipCodeLabel;
@property (nonatomic) IBOutlet UILabel* address1Label;
@property (nonatomic) IBOutlet UILabel* address2Label;
@property (nonatomic) IBOutlet UILabel* address3Label;
@property (nonatomic) IBOutlet UILabel* phoneLabel;
@property (nonatomic) IBOutlet UILabel* emailLabel;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;
@property (nonatomic) IBOutlet UIButton* editNameButton;
@property (nonatomic) IBOutlet UIButton* editNameKanaButton;
@property (nonatomic) IBOutlet UIButton* editZipCodeButton;
@property (nonatomic) IBOutlet UIButton* editAddress1Button;
@property (nonatomic) IBOutlet UIButton* editAddress2Button;
@property (nonatomic) IBOutlet UIButton* editAddress3Button;
@property (nonatomic) IBOutlet UIButton* editPhoneButton;
@property (nonatomic) IBOutlet UIButton* editEmailButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;
- (IBAction)editButtonTapped:(UIButton*)button;

@end
