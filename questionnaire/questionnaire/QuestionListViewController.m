//
//  QuestionListViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "QuestionListViewController.h"
#import "Question01ViewController.h"
#import "AppDelegate.h"
#import "UtilityFunction.h"
#import "FMDB.h"

@interface QuestionListViewController ()
@end

@implementation QuestionListViewController

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"QuestionList" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"";

    return self;
}

- (void)viewDidLoad {
	
	[self initSwipe];
	
	[UtilityFunction resetPersonalInfo];
		
	//[self initLabel];
	
	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];

	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
	
	[self loadQuestions];
	
	[super viewWillAppear:animated];
}

- (void)initSwipe {
	
	//SwipeGestureのインスタンスを生成
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	//スワイプの方向（右から左）
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	//self.viewにジェスチャーをのせる
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	//SwipeGestureのインスタンスを生成
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	//スワイプの方向(左から右）
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	//self.viewにジェスチャーをのせる
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)loadQuestions {
	
	_questions = [NSMutableDictionary dictionary];

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	[self loadQuestionItem:@"3" placeCode:appDelegate.placeCode];
	if ([_questions[@"q3"][@"choices"] count] == 0) {
		[self loadQuestionItem:@"3" placeCode:@"0"];
	}
	
	[self loadQuestionItem:@"13" placeCode:appDelegate.placeCode];
	if ([_questions[@"q13"][@"choices"] count] == 0) {
		[self loadQuestionItem:@"13" placeCode:@"0"];
	}

	[self loadQuestionTitle:@"19" placeCode:appDelegate.placeCode];
	if (_questions[@"q19"]) {
		[self loadQuestionItem:@"19" placeCode:appDelegate.placeCode];
		if ([_questions[@"q19"][@"choices"] count] == 0) {
			[self loadQuestionItem:@"19" placeCode:@"0"];
		}
	}
	_Log(@"q19 choice = %@", _questions[@"q19"][@"choices"]);
	
	[self loadQuestionTitle:@"20" placeCode:appDelegate.placeCode];
	if (_questions[@"q20"]) {
		[self loadQuestionItem:@"20" placeCode:appDelegate.placeCode];
		if ([_questions[@"q20"][@"choices"] count] == 0) {
			[self loadQuestionItem:@"20" placeCode:@"0"];
		}
	}
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:_questions forKey:@"questions"];
}

- (void)loadQuestionTitle:(NSString*)quetsionNumber placeCode:(NSString*)placeCode {
	
	NSString*   title = @"";
	NSString*   type = @"";
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];

	NSString* sql = [NSString stringWithFormat:
					 @"SELECT id, question_number, type, title, place_code, start_date, end_date FROM question_title WHERE question_number = %@ AND place_code IN ('0', '%@') AND start_date <= CURRENT_DATE AND end_date >= CURRENT_DATE ORDER BY place_code DESC", quetsionNumber, placeCode];
	
	[db open];
 
	FMResultSet* results = [db executeQuery:sql];
	_Log(@"%@", results);
	
	while([results next]) {
		title = [results stringForColumnIndex:3];
		type = [results stringForColumnIndex:2];
		break;
	}
	[db close];

	if ([title length]) {
		if ([quetsionNumber isEqualToString:@"19"]) {
			_questions[@"q19"] = [NSMutableDictionary dictionary];
			_questions[@"q19"][@"title"] = title;
			_questions[@"q19"][@"type"] = type;
			_Log(@"Q19 %@", title);
		} else if ([quetsionNumber isEqualToString:@"20"]) {
			_questions[@"q20"] = [NSMutableDictionary dictionary];
			_questions[@"q20"][@"title"] = title;
			_questions[@"q20"][@"type"] = type;
			_Log(@"Q20 %@", title);
		}
	}
}

- (void)loadQuestionItem:(NSString*)quetsionNumber placeCode:(NSString*)placeCode {
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
	
	NSString* sql = [NSString stringWithFormat:@"SELECT id, question_number, item_number, title, place_code, start_date, end_date FROM question_item WHERE question_number = %@ AND start_date <= CURRENT_DATE AND end_date >= CURRENT_DATE AND place_code = %@ ORDER BY item_number", quetsionNumber, placeCode];
	
	[db open];
	
	FMResultSet* results = [db executeQuery:sql];
	_Log(@"%@", results);
	
	NSMutableArray*	choices = [[NSMutableArray alloc] init];
	NSString* otherChoice = nil;

	while([results next]) {
		NSString* a1 = [results stringForColumnIndex:0];
		NSString* a2 = [results stringForColumnIndex:1];
		NSString* a3 = [results stringForColumnIndex:2];
		NSString* a4 = [results stringForColumnIndex:3];
		NSString* a5 = [results stringForColumnIndex:4];
		NSString* a6 = [results stringForColumnIndex:5];
		
		_Log(@"%@", a1);
		_Log(@"%@", a2);
		_Log(@"%@", a3);
		_Log(@"%@", a4);
		_Log(@"%@", a5);
		_Log(@"%@", a6);
		_Log(@"--------------");
		
		if ([results intForColumnIndex:4] != 20) {
			[choices addObject:[results stringForColumnIndex:3]];
		} else {
			otherChoice = [results stringForColumnIndex:3];
		}
	}
	[db close];
	
	if ([choices count]) {
		NSString* quetsionNumberIndex = [NSString stringWithFormat:@"q%@", quetsionNumber];
		if (!_questions[quetsionNumberIndex]) {
			_questions[quetsionNumberIndex] = [NSMutableDictionary dictionary];
		}
		_questions[quetsionNumberIndex][@"choices"] = choices;
		if (otherChoice) {
			_questions[quetsionNumberIndex][@"otherChoice"] = otherChoice;
		}
	}
}

- (void)initLabel {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSMutableDictionary* questions = [defaults objectForKey:@"questions"];
	
	NSString* q19Title = questions[@"q19"][@"title"];
	if (q19Title) {
		_q19TitleLabel.text = [NSString stringWithFormat:@"Q19 %@", q19Title];
	}
	
	NSString* q20Title = questions[@"q20"][@"title"];
	if (q20Title) {
		_q20TitleLabel.text = [NSString stringWithFormat:@"Q20 %@", q20Title];
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	Question01ViewController *viewController = [[Question01ViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
}

@end
