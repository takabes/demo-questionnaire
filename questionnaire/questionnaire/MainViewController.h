//
//  MainViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController {
	UIButton*		_accountButton;
	UIButton*		_startButton;
}

@property (nonatomic) IBOutlet UIButton* accountButton;
@property (nonatomic) IBOutlet UIButton* startButton;

- (IBAction)accountButtonTapped:(UIButton*)button;
- (IBAction)startButtonTapped:(UIButton*)button;

@end
