//
//  InputPhoneViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "InputPhoneViewController.h"
#import "InputAddressViewController.h"
#import "InputEmailViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "AppConsts.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"

@interface InputPhoneViewController ()
@end

@implementation InputPhoneViewController

@synthesize edit = _edit;

@synthesize phoneTextField = _phoneTextField;
@synthesize oneButton = _oneButton;
@synthesize twoButton = _twoButton;
@synthesize threeButton = _threeButton;
@synthesize fourButton = _fourButton;
@synthesize fiveButton = _fiveButton;
@synthesize sixButton = _sixButton;
@synthesize sevenButton = _sevenButton;
@synthesize eightButton = _eightButton;
@synthesize nineButton = _nineButton;
@synthesize zeroButton = _zeroButton;
@synthesize deleteButton = _deleteButton;

@synthesize nextButton = _nextButton;
@synthesize showKeyboardButton = _showKeyboardButton;

- (id)init {
    self = [super initWithNibName:@"InputPhone" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"電話番号入力";
    
    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	[self initButton];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	_phoneTextField.text = appDelegate.phone;

	_phoneTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];

	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initButton {

	[_oneButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_twoButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_threeButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fourButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fiveButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sixButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sevenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_eightButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_nineButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_zeroButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
	[UtilityFunction customizeButton:_showKeyboardButton];

	[UtilityFunction customizeKeyboardButton:_oneButton];
	[UtilityFunction customizeKeyboardButton:_twoButton];
	[UtilityFunction customizeKeyboardButton:_threeButton];
	[UtilityFunction customizeKeyboardButton:_fourButton];
	[UtilityFunction customizeKeyboardButton:_fiveButton];
	[UtilityFunction customizeKeyboardButton:_sixButton];
	[UtilityFunction customizeKeyboardButton:_sevenButton];
	[UtilityFunction customizeKeyboardButton:_eightButton];
	[UtilityFunction customizeKeyboardButton:_nineButton];
	[UtilityFunction customizeKeyboardButton:_zeroButton];
	[UtilityFunction customizeKeyboardButton:_deleteButton];
}

- (void)viewDidAppear:(BOOL)animated {
	[_phoneTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
	[super viewDidAppear:animated];
}

- (void)keyboardButtonTapped:(UIButton*)button {
	_phoneTextField.text = [NSString stringWithFormat:@"%@%@", _phoneTextField.text, [UtilityFunction getCharacter:button.tag]];
}

- (void)deleteButtonTapped:(UIButton*)button {
	_Log(@"deleteButtonTapped");
	
	if ([_phoneTextField.text length]) {
		NSString* text = [_phoneTextField.text substringToIndex:[_phoneTextField.text length] - 1];
		_phoneTextField.text = text;
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	if ([_phoneTextField.text length] > 0) {
		if ([UtilityFunction chkNumeric:_phoneTextField.text] != YES) {
			[self showAlert:@"入力項目" message:@"電話番号は半角数字で入力してください"];
			return;
		}
		
		if ([_phoneTextField.text length] != 10 && [_phoneTextField.text length] != 11) {
			[self showAlert:@"入力項目" message:@"電話番号は10桁または11桁で入力してください"];
			return;
		}
	}

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.phone = _phoneTextField.text;

	if (_edit) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		InputEmailViewController *viewController = [[InputEmailViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)showKeyboardButtonTapped:(UIButton*)button {
	[_phoneTextField becomeFirstResponder];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中の内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		[UtilityFunction resetPersonalInfo];
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}
		
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
