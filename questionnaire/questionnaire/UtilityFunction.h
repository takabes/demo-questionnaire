//
//  UtilityFunction.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonCryptor.h>

@interface UtilityFunction : NSObject {
}

+ (UIColor*)colorWithRBGA:(unsigned int)rgba;
+ (void)customizeButton:(UIButton*)button;
+ (void)customizeButton2:(UIButton*)button;
+ (void)customizeKeyboardButton:(UIButton*)button;
+ (NSString*)getCharacter:(NSUInteger)tag;
+ (void)resetPersonalInfo;
+ (NSString*)getCurrentDate;
+ (void)showNowDate;
+ (void)saveAccounts:(NSMutableArray*)accounts;
+ (NSMutableArray*)getPersonalInfos;
+ (void)deletePeronalInfos;
+ (NSMutableArray*)getAddress:(NSString*)zipCode;
+ (BOOL)chkNumeric:(NSString *)checkString;
+ (BOOL)isAllHalfWidthCharacter:(NSString*)checkString;
+ (BOOL)isSerialNumber:(NSString*)checkString;
+ (BOOL)isZenkaku:(NSString*)checkString;
+ (BOOL)isZenkakuKatakana:(NSString*)checkString;

@end
