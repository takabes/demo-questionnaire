//
//  InputPrefectureViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputPrefectureViewController : UIViewController <UITextFieldDelegate> {
	UIButton*		_prefecture01Button;
	UIButton*		_prefecture02Button;
	UIButton*		_prefecture03Button;
	UIButton*		_prefecture04Button;
	UIButton*		_prefecture05Button;
	UIButton*		_prefecture06Button;
	UIButton*		_prefecture07Button;
	UIButton*		_prefecture08Button;
	UIButton*		_prefecture09Button;
	UIButton*		_prefecture10Button;
	UITextField*	_prefectureTextField;
	UIImageView*	_guideImageView;
	UIButton*		_nextButton;
	UIButton*		_backButton;
	BOOL			_edit;
	NSUInteger		_guideMode;
	NSUInteger		_selectedPrefecture;
}

@property (nonatomic) IBOutlet UITextField* prefectureTextField;
@property (nonatomic) IBOutlet UIButton* prefecture01Button;
@property (nonatomic) IBOutlet UIButton* prefecture02Button;
@property (nonatomic) IBOutlet UIButton* prefecture03Button;
@property (nonatomic) IBOutlet UIButton* prefecture04Button;
@property (nonatomic) IBOutlet UIButton* prefecture05Button;
@property (nonatomic) IBOutlet UIButton* prefecture06Button;
@property (nonatomic) IBOutlet UIButton* prefecture07Button;
@property (nonatomic) IBOutlet UIButton* prefecture08Button;
@property (nonatomic) IBOutlet UIButton* prefecture09Button;
@property (nonatomic) IBOutlet UIButton* prefecture10Button;
@property (nonatomic) IBOutlet UIImageView* guideImageView;
@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;
@property (nonatomic) BOOL edit;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;
- (IBAction)switchGuideButtonTapped:(UIButton*)button;
- (IBAction)prefectureButtonTapped:(UIButton*)button;

@end
