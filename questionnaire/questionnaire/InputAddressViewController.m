//
//  InputAddressViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "InputAddressViewController.h"
#import "InputEmailViewController.h"
#import "InputPhoneViewController.h"
#import "UtilityFunction.h"
#import "FMDB.h"
#import "AppDelegate.h"
#import "AppConsts.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"

@interface InputAddressViewController ()
@end

@implementation InputAddressViewController

@synthesize edit = _edit;
@synthesize cursor = _cursor;
@synthesize prefecture = _prefecture;
@synthesize address1 = _address1;
@synthesize address2 = _address2;

@synthesize prefectureTextField = _prefectureTextField;
@synthesize address1TextField = _address1TextField;
@synthesize address2TextField = _address2TextField;
@synthesize address3TextField = _address3TextField;
@synthesize address4TextField = _address4TextField;

@synthesize address1SubmitButton = _address1SubmitButton;
@synthesize address2SubmitButton = _address2SubmitButton;
@synthesize address3SubmitButton = _address3SubmitButton;
@synthesize address4SubmitButton = _address4SubmitButton;
@synthesize address5SubmitButton = _address5SubmitButton;

@synthesize oneButton = _oneButton;
@synthesize twoButton = _twoButton;
@synthesize threeButton = _threeButton;
@synthesize fourButton = _fourButton;
@synthesize fiveButton = _fiveButton;
@synthesize sixButton = _sixButton;
@synthesize sevenButton = _sevenButton;
@synthesize eightButton = _eightButton;
@synthesize nineButton = _nineButton;
@synthesize zeroButton = _zeroButton;
@synthesize deleteButton = _deleteButton;

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;
@synthesize showKeyboardButton = _showKeyboardButton;

- (id)init {
    self = [super initWithNibName:@"InputAddress" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"住所入力";

    return self;
}

- (void)viewDidLoad {
	
	_currentTextFieldTag = 1;

	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];
	
	[_prefectureTextField setDelegate:self];
	[_address1TextField setDelegate:self];
	[_address2TextField setDelegate:self];
	[_address3TextField setDelegate:self];
	[_address4TextField setDelegate:self];

	[self initButton];
	
	[self hideNumberKeyboard];

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	if (_prefecture) {
		_prefectureTextField.text = _prefecture;
	} else {
		_prefectureTextField.text = appDelegate.prefecture;
	}
	
	if (_address1) {
		_address1TextField.text = _address1;
	} else {
		_address1TextField.text = appDelegate.address1;
	}
	
	if (_address2) {
		_address2TextField.text = _address2;
	} else {
		_address2TextField.text = appDelegate.address2;
	}

	_address3TextField.text = appDelegate.address3;
	_address4TextField.text = appDelegate.address4;
	
	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initButton {
	
	[_oneButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_twoButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_threeButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fourButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fiveButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sixButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sevenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_eightButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_nineButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_zeroButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_hyphenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[UtilityFunction customizeButton:_address1SubmitButton];
	[UtilityFunction customizeButton:_address2SubmitButton];
	[UtilityFunction customizeButton:_address3SubmitButton];
	[UtilityFunction customizeButton:_address4SubmitButton];
	[UtilityFunction customizeButton:_address5SubmitButton];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
	[UtilityFunction customizeButton:_showKeyboardButton];
	
	[UtilityFunction customizeKeyboardButton:_oneButton];
	[UtilityFunction customizeKeyboardButton:_twoButton];
	[UtilityFunction customizeKeyboardButton:_threeButton];
	[UtilityFunction customizeKeyboardButton:_fourButton];
	[UtilityFunction customizeKeyboardButton:_fiveButton];
	[UtilityFunction customizeKeyboardButton:_sixButton];
	[UtilityFunction customizeKeyboardButton:_sevenButton];
	[UtilityFunction customizeKeyboardButton:_eightButton];
	[UtilityFunction customizeKeyboardButton:_nineButton];
	[UtilityFunction customizeKeyboardButton:_zeroButton];
	[UtilityFunction customizeKeyboardButton:_hyphenButton];
	[UtilityFunction customizeKeyboardButton:_deleteButton];
}

- (void)initAddress:(NSString*)zipCode {
	
	_cursor = 1;

	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
	
	NSString* sql = @"SELECT id, prefecture, address1, address2 FROM zip_code WHERE code = ?;";
 
	[db open];
 
	FMResultSet* results = [db executeQuery:sql, zipCode];
	
	while([results next]) {
		_prefectureTextField.text = [results stringForColumnIndex:1];
		_address1TextField.text = [results stringForColumnIndex:2];
		_address2TextField.text = [results stringForColumnIndex:3];
		_cursor = 4;
	}
	
	[db close];
}

- (void)viewDidAppear:(BOOL)animated {
	
	switch (_cursor) {
		case 1:
			[_prefectureTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
			break;

		case 2:
			[_address1TextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
			break;

		case 4:
			[_address3TextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
			break;

		case 5:
			[_address4TextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
			break;

		default:
			break;
	}

	[super viewDidAppear:animated];
}

- (void)hideNumberKeyboard {
	[_oneButton setHidden:YES];
	[_twoButton setHidden:YES];
	[_threeButton setHidden:YES];
	[_fourButton setHidden:YES];
	[_fiveButton setHidden:YES];
	[_sixButton setHidden:YES];
	[_sevenButton setHidden:YES];
	[_eightButton setHidden:YES];
	[_nineButton setHidden:YES];
	[_zeroButton setHidden:YES];
	[_hyphenButton setHidden:YES];
	[_deleteButton setHidden:YES];
}

- (void)showNumberKeyboard {
	[_oneButton setHidden:NO];
	[_twoButton setHidden:NO];
	[_threeButton setHidden:NO];
	[_fourButton setHidden:NO];
	[_fiveButton setHidden:NO];
	[_sixButton setHidden:NO];
	[_sevenButton setHidden:NO];
	[_eightButton setHidden:NO];
	[_nineButton setHidden:NO];
	[_zeroButton setHidden:NO];
	[_hyphenButton setHidden:NO];
	[_deleteButton setHidden:NO];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	
	_Log(@"textFieldShouldBeginEditing");
	_currentTextFieldTag = textField.tag;

	if (_currentTextFieldTag == 1) {
		_prefectureTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
		_address1TextField.backgroundColor = [UIColor whiteColor];
		_address2TextField.backgroundColor = [UIColor whiteColor];
		_address3TextField.backgroundColor = [UIColor whiteColor];
		_address4TextField.backgroundColor = [UIColor whiteColor];
	} else if (_currentTextFieldTag == 2) {
		_prefectureTextField.backgroundColor = [UIColor whiteColor];
		_address1TextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
		_address2TextField.backgroundColor = [UIColor whiteColor];
		_address3TextField.backgroundColor = [UIColor whiteColor];
		_address4TextField.backgroundColor = [UIColor whiteColor];
	} else if (_currentTextFieldTag == 3) {
		_prefectureTextField.backgroundColor = [UIColor whiteColor];
		_address1TextField.backgroundColor = [UIColor whiteColor];
		_address2TextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
		_address3TextField.backgroundColor = [UIColor whiteColor];
		_address4TextField.backgroundColor = [UIColor whiteColor];
	} else if (_currentTextFieldTag == 4) {
		_prefectureTextField.backgroundColor = [UIColor whiteColor];
		_address1TextField.backgroundColor = [UIColor whiteColor];
		_address2TextField.backgroundColor = [UIColor whiteColor];
		_address3TextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
		_address4TextField.backgroundColor = [UIColor whiteColor];
	} else if (_currentTextFieldTag == 5) {
		_prefectureTextField.backgroundColor = [UIColor whiteColor];
		_address1TextField.backgroundColor = [UIColor whiteColor];
		_address2TextField.backgroundColor = [UIColor whiteColor];
		_address3TextField.backgroundColor = [UIColor whiteColor];
		_address4TextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
	}
	
	if (_currentTextFieldTag == 4) {
		[self showNumberKeyboard];
	} else {
		[self hideNumberKeyboard];
	}
	
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
	
	if (textField.tag == 1) {
		[_address1TextField becomeFirstResponder];
		return NO;
	} else if (textField.tag == 2) {
		[_address2TextField becomeFirstResponder];
		return NO;
	} else if (textField.tag == 3) {
		[_address3TextField becomeFirstResponder];
		return NO;
	} else if (textField.tag == 4) {
		[_address4TextField becomeFirstResponder];
		return NO;
	}
	
	return YES;
}

- (IBAction)addressSubmitButtonTapped:(UIButton*)button {
	
	if (button.tag == 1) {
		[_address1TextField becomeFirstResponder];
	} else if (button.tag == 2) {
		[_address2TextField becomeFirstResponder];
	} else if (button.tag == 3) {
		[_address3TextField becomeFirstResponder];
	} else if (button.tag == 4) {
		[_address4TextField becomeFirstResponder];
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {

	NSString* message = @"";

	if ([_prefectureTextField.text length] == 0) {
		message = @"都道府県を入力してください";
	}

	if ([_address1TextField.text length] == 0) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"市区町村を入力してください"];
		} else {
			message = @"市区町村を入力してください";
		}
	}

	if ([_address2TextField.text length] == 0) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"町丁名を入力してください"];
		} else {
			message = @"町丁名を入力してください";
		}
	}

	if ([_address3TextField.text length] == 0) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"番地を入力してください"];
		} else {
			message = @"番地を入力してください";
		}
	}
	
	if ([message length]) {
		[self showAlert:@"必須項目" message:message];
		return;
	}

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.prefecture = _prefectureTextField.text;
	appDelegate.address1 = _address1TextField.text;
	appDelegate.address2 = _address2TextField.text;
	appDelegate.address3 = _address3TextField.text;
	appDelegate.address4 = _address4TextField.text;

	[self getZipCode];

	if (_edit) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		InputPhoneViewController* viewController = [[InputPhoneViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

// 郵便番号逆引き
- (void)getZipCode {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	if ([appDelegate.zipCode length] > 0) {
		return;
	}
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
	
	NSString* sql = @"SELECT id, code FROM zip_code WHERE prefecture = ? AND address1 = ? AND address2 = ?;";
 
	[db open];
 
	FMResultSet* results = [db executeQuery:sql, _prefectureTextField.text, _address1TextField.text, _address2TextField.text];
	
	while([results next]) {
		appDelegate.zipCode = [results stringForColumnIndex:1];
		_Log(@"zipCode: %@", appDelegate.zipCode);
	}
	
	[db close];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)keyboardButtonTapped:(UIButton*)button {
	_address3TextField.text = [NSString stringWithFormat:@"%@%@", _address3TextField.text, [UtilityFunction getCharacter:button.tag]];
}

- (void)deleteButtonTapped:(UIButton*)button {
	_Log(@"deleteButtonTapped");
	
	if ([_address3TextField.text length]) {
		NSString* text = [_address3TextField.text substringToIndex:[_address3TextField.text length] - 1];
		_address3TextField.text = text;
	}
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)showKeyboardButtonTapped:(UIButton*)button {
	if (_currentTextFieldTag == 1) {
		[_prefectureTextField becomeFirstResponder];
	} else if (_currentTextFieldTag == 2) {
		[_address1TextField becomeFirstResponder];
	} else if (_currentTextFieldTag == 3) {
		[_address2TextField becomeFirstResponder];
	} else if (_currentTextFieldTag == 4) {
		[_address3TextField becomeFirstResponder];
	} else if (_currentTextFieldTag == 5) {
		[_address4TextField becomeFirstResponder];
	}
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中の内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		[UtilityFunction resetPersonalInfo];
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}
		
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
