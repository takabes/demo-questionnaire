//
//  Question02ViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "Question02ViewController.h"
#import "Question03ViewController.h"
#import "AppDelegate.h"
#import "UtilityFunction.h"
#import "QuestionListViewController.h"

@interface Question02ViewController ()
@end

@implementation Question02ViewController

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"Question02" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"アンケート入力";

    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	[_q5Answer02TextField setDelegate:self];

	[self initHeaderLabel];
	
	[self initButton];

	[self initQ4Answer];
	[self initQ5Answer];
	
	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initHeaderLabel {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	if (questions[@"q19"]) {
		if (questions[@"q20"]) {
			_headerLabel.text = @"2 / 9";
		} else {
			_headerLabel.text = @"2 / 8";
		}
	} else {
		_headerLabel.text = @"2 / 7";
	}
}

- (void)initButton {
	
	[_q4Answer01Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer02Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer03Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer04Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer05Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer06Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer07Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer08Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer09Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer10Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer11Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer12Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q4Answer20Button addTarget:self action:@selector(q4AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_q5Answer01Button addTarget:self action:@selector(q5AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q5Answer02Button addTarget:self action:@selector(q5AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q5Answer03Button addTarget:self action:@selector(q5AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q5Answer04Button addTarget:self action:@selector(q5AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q5Answer20Button addTarget:self action:@selector(q5AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
}

- (void)initQ4Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q4Answer01 isEqualToString:@"1"]) {
		[_q4Answer01Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer02 isEqualToString:@"1"]) {
		[_q4Answer02Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer03 isEqualToString:@"1"]) {
		[_q4Answer03Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer04 isEqualToString:@"1"]) {
		[_q4Answer04Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer05 isEqualToString:@"1"]) {
		[_q4Answer05Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer06 isEqualToString:@"1"]) {
		[_q4Answer06Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer07 isEqualToString:@"1"]) {
		[_q4Answer07Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer08 isEqualToString:@"1"]) {
		[_q4Answer08Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer09 isEqualToString:@"1"]) {
		[_q4Answer09Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer10 isEqualToString:@"1"]) {
		[_q4Answer10Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer11 isEqualToString:@"1"]) {
		[_q4Answer11Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer12 isEqualToString:@"1"]) {
		[_q4Answer12Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q4Answer20 isEqualToString:@"1"]) {
		[_q4Answer20Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ5Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q5Answer01 isEqualToString:@"1"]) {
		[_q5Answer01Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q5Answer02 isEqualToString:@"1"]) {
		[_q5Answer02Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q5Answer03 isEqualToString:@"1"]) {
		[_q5Answer03Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q5Answer04 isEqualToString:@"1"]) {
		[_q5Answer04Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q5Answer20 isEqualToString:@"1"]) {
		[_q5Answer20Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	
	_q5Answer02TextField.text = appDelegate.q5Answer02Text;
}

- (void)q4AnswerButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	BOOL check = YES;
	
	switch (button.tag) {
		case 1:
			if ([appDelegate.q4Answer01 isEqualToString:@"0"]) {
				appDelegate.q4Answer01 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer01 = @"0";
				check = NO;
			}
			break;
		case 2:
			if ([appDelegate.q4Answer02 isEqualToString:@"0"]) {
				appDelegate.q4Answer02 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer02 = @"0";
				check = NO;
			}
			break;
		case 3:
			if ([appDelegate.q4Answer03 isEqualToString:@"0"]) {
				appDelegate.q4Answer03 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer03 = @"0";
				check = NO;
			}
			break;
		case 4:
			if ([appDelegate.q4Answer04 isEqualToString:@"0"]) {
				appDelegate.q4Answer04 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer04 = @"0";
				check = NO;
			}
			break;
		case 5:
			if ([appDelegate.q4Answer05 isEqualToString:@"0"]) {
				appDelegate.q4Answer05 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer05 = @"0";
				check = NO;
			}
			break;
		case 6:
			if ([appDelegate.q4Answer06 isEqualToString:@"0"]) {
				appDelegate.q4Answer06 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer06 = @"0";
				check = NO;
			}
			break;
		case 7:
			if ([appDelegate.q4Answer07 isEqualToString:@"0"]) {
				appDelegate.q4Answer07 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer07 = @"0";
				check = NO;
			}
			break;
		case 8:
			if ([appDelegate.q4Answer08 isEqualToString:@"0"]) {
				appDelegate.q4Answer08 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer08 = @"0";
				check = NO;
			}
			break;
		case 9:
			if ([appDelegate.q4Answer09 isEqualToString:@"0"]) {
				appDelegate.q4Answer09 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer09 = @"0";
				check = NO;
			}
			break;
		case 10:
			if ([appDelegate.q4Answer10 isEqualToString:@"0"]) {
				appDelegate.q4Answer10 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer10 = @"0";
				check = NO;
			}
			break;
		case 11:
			if ([appDelegate.q4Answer11 isEqualToString:@"0"]) {
				appDelegate.q4Answer11 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer11 = @"0";
				check = NO;
			}
			break;
		case 12:
			if ([appDelegate.q4Answer12 isEqualToString:@"0"]) {
				appDelegate.q4Answer12 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer12 = @"0";
				check = NO;
			}
			break;
		case 20:
			if ([appDelegate.q4Answer20 isEqualToString:@"0"]) {
				appDelegate.q4Answer20 = @"1";
				check = YES;
			} else {
				appDelegate.q4Answer20 = @"0";
				check = NO;
			}
			break;
	}
	
	if (check) {
		[button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	} else {
		[button setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
}

- (void)q5AnswerButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	BOOL check = YES;
	
	switch (button.tag) {
		case 1:
			if ([appDelegate.q5Answer01 isEqualToString:@"0"]) {
				appDelegate.q5Answer01 = @"1";
				check = YES;
			} else {
				appDelegate.q5Answer01 = @"0";
				check = NO;
			}
			break;
		case 2:
			if ([appDelegate.q5Answer02 isEqualToString:@"0"]) {
				appDelegate.q5Answer02 = @"1";
				check = YES;
			} else {
				appDelegate.q5Answer02 = @"0";
				check = NO;
			}
			break;
		case 3:
			if ([appDelegate.q5Answer03 isEqualToString:@"0"]) {
				appDelegate.q5Answer03 = @"1";
				check = YES;
			} else {
				appDelegate.q5Answer03 = @"0";
				check = NO;
			}
			break;
		case 4:
			if ([appDelegate.q5Answer04 isEqualToString:@"0"]) {
				appDelegate.q5Answer04 = @"1";
				check = YES;
			} else {
				appDelegate.q5Answer04 = @"0";
				check = NO;
			}
			break;
		case 20:
			if ([appDelegate.q5Answer20 isEqualToString:@"0"]) {
				appDelegate.q5Answer20 = @"1";
				check = YES;
			} else {
				appDelegate.q5Answer20 = @"0";
				check = NO;
			}
			break;
	}
	
	if (check) {
		[button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	} else {
		[button setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	NSString* message = @"";

	if ([appDelegate.q4Answer01 isEqualToString:@"0"] &&
		[appDelegate.q4Answer02 isEqualToString:@"0"] &&
		[appDelegate.q4Answer03 isEqualToString:@"0"] &&
		[appDelegate.q4Answer04 isEqualToString:@"0"] &&
		[appDelegate.q4Answer05 isEqualToString:@"0"] &&
		[appDelegate.q4Answer06 isEqualToString:@"0"] &&
		[appDelegate.q4Answer07 isEqualToString:@"0"] &&
		[appDelegate.q4Answer08 isEqualToString:@"0"] &&
		[appDelegate.q4Answer09 isEqualToString:@"0"] &&
		[appDelegate.q4Answer10 isEqualToString:@"0"] &&
		[appDelegate.q4Answer11 isEqualToString:@"0"] &&
		[appDelegate.q4Answer12 isEqualToString:@"0"] &&
		[appDelegate.q4Answer20 isEqualToString:@"0"]) {
		message = @"Q4の回答を選択してください";
	}

	if ([appDelegate.q5Answer01 isEqualToString:@"0"] &&
		[appDelegate.q5Answer02 isEqualToString:@"0"] &&
		[appDelegate.q5Answer03 isEqualToString:@"0"] &&
		[appDelegate.q5Answer04 isEqualToString:@"0"] &&
		[appDelegate.q5Answer20 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q5の回答を選択してください"];
		} else {
			message = @"Q5の回答を選択してください";
		}
	}

	if ([message length]) {
		[self showAlert:@"必須項目" message:message];
		return;
	}

	if ([appDelegate.q5Answer02 isEqualToString:@"1"]) {
		if ([_q5Answer02TextField.text length]) {
			appDelegate.q5Answer02Text = _q5Answer02TextField.text;
		}
	}
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	Question03ViewController *viewController = [[Question03ViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

//--------------------------------------------------------------//
#pragma mark -- UITextFieldDelegate Delegate Methods --
//--------------------------------------------------------------//

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
	[textField resignFirstResponder];
	return YES;
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中のアンケート内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[UtilityFunction resetPersonalInfo];
		QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
