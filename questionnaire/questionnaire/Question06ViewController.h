//
//  Question06ViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Question06ViewController : UIViewController {
	UILabel*		_headerLabel;

	UIButton*		_q14Answer01Button;
	UIButton*		_q14Answer02Button;
	UIButton*		_q14Answer03Button;
	UIButton*		_q14Answer04Button;
	UIButton*		_q14Answer05Button;

	UIButton*		_q15Answer01Button;
	UIButton*		_q15Answer02Button;
	UIButton*		_q15Answer03Button;
	UIButton*		_q15Answer04Button;
	UIButton*		_q15Answer05Button;

	UIButton*		_q16Answer01Button;
	UIButton*		_q16Answer02Button;
	UIButton*		_q16Answer03Button;
	UIButton*		_q16Answer04Button;
	UIButton*		_q16Answer05Button;
	UIButton*		_q16Answer20Button;

	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UILabel* headerLabel;

@property (nonatomic) IBOutlet UIButton* q14Answer01Button;
@property (nonatomic) IBOutlet UIButton* q14Answer02Button;
@property (nonatomic) IBOutlet UIButton* q14Answer03Button;
@property (nonatomic) IBOutlet UIButton* q14Answer04Button;
@property (nonatomic) IBOutlet UIButton* q14Answer05Button;

@property (nonatomic) IBOutlet UIButton* q15Answer01Button;
@property (nonatomic) IBOutlet UIButton* q15Answer02Button;
@property (nonatomic) IBOutlet UIButton* q15Answer03Button;
@property (nonatomic) IBOutlet UIButton* q15Answer04Button;
@property (nonatomic) IBOutlet UIButton* q15Answer05Button;

@property (nonatomic) IBOutlet UIButton* q16Answer01Button;
@property (nonatomic) IBOutlet UIButton* q16Answer02Button;
@property (nonatomic) IBOutlet UIButton* q16Answer03Button;
@property (nonatomic) IBOutlet UIButton* q16Answer04Button;
@property (nonatomic) IBOutlet UIButton* q16Answer05Button;
@property (nonatomic) IBOutlet UIButton* q16Answer20Button;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
