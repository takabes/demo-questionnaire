//
//  Complete2ViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "Complete2ViewController.h"
#import "UtilityFunction.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"
#import "AppDelegate.h"
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "FMDB.h"
#import "AppConsts.h"
#import "FBEncryptorAES.h"

@interface Complete2ViewController ()
@end

@implementation Complete2ViewController

@synthesize serialCodeLabel = _serialCodeLabel;
@synthesize checkBoxButton = _checkBoxButton;
@synthesize closeButton = _closeButton;

- (id)init {
    self = [super initWithNibName:@"Complete2" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"登録完了";

    return self;
}

- (void)viewDidLoad {

	_register = NO;

	// 戻るボタン非表示
	[self.navigationItem setHidesBackButton:YES];

	[UtilityFunction customizeButton:_registerButton];
	[UtilityFunction customizeButton:_closeButton];

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	if (MODE == MODE_ONLY_PERSONAL_INFO) {
		_serialCodeLabel.text = [NSString stringWithFormat:@"No%@の登録を受け付けました。", appDelegate.serialNumber];
	} else {
		_serialCodeLabel.text = @"登録を受け付けました。";
	}
	
	if ([appDelegate.regular isEqualToString:@"1"]) {
		_checkStatus = YES;
		[_checkBoxButton setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	} else {
		_checkStatus = NO;
		[_checkBoxButton setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}

	[super viewDidLoad];
}

- (IBAction)checkButtonTapped:(UIButton*)button {
	if(_checkStatus){
		[_checkBoxButton setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
	else {
		[_checkBoxButton setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	_checkStatus = !_checkStatus;
}

- (IBAction)registerButtonTapped:(UIButton*)button {
	_Log(@"registerButtonTapped");
	
	[self saveAnswer];

	NSString* url = @"";
	if (MODE == MODE_ONLY_PERSONAL_INFO) {
		url = API_URL_REGISTER_PERSONAL_INFO;
	} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
		url = API_URL_REGISTER_PERSONAL_INFO_ANSWER;
	} else {
		return;
	}
	_Log(@"url = %@", url);

	NSMutableArray* personalInfos = [UtilityFunction getPersonalInfos];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	_Log(@"appDelegate.userId = %@", appDelegate.userId);

	// インディケータ
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
	
	AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSDictionary* param = @{@"accessKey" : API_ACCESS_KEY, @"userId": appDelegate.userId, @"personalInfos":personalInfos};
	[manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
		
		_Log(@"response: %@", responseObject);
		
		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		if ([responseObject[@"result"] isEqualToString:@"success"]) {
			[self showAlert:@"確認" message:@"データを登録しました"];
			_registerButton.hidden = YES;
			[UtilityFunction deletePeronalInfos];
		} else {
			NSString* message = [NSString stringWithFormat:@"%@:%@", responseObject[@"errorCode"], responseObject[@"errorMessage"]];
			[self showNetworkAlert:@"エラー" message:message];
		}
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		
		_Log(@"error = %@", error);
		
		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		[self showNetworkAlert:@"エラー" message:@"通信エラーが発生しました。ネットワーク状況を確認して下さい。"];
	}];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}

	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (void)showNetworkAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)closeButtonTapped:(UIButton*)button {

	[self saveAnswer];	
	
	if (MODE == MODE_ONLY_PERSONAL_INFO) {
		InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
		QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	}
}

// 回答データ保存
- (void)saveAnswer {
	
	if (_register) {
		return;
	}
	_register = YES;

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if (_checkStatus) {
		appDelegate.regular = @"1";
		_Log(@"Regular");
	} else {
		appDelegate.regular = @"0";
		_Log(@"Not Regular");
	}

	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
 
	// CREATE TABLE
	NSString* sql = @"CREATE TABLE IF NOT EXISTS answer (id INTEGER PRIMARY KEY AUTOINCREMENT, personal_info TEXT, delete_flag TEXT);";
	[db open];
	[db executeUpdate:sql];
	[db close];
	
	NSString* encryptedLastName = [FBEncryptorAES encryptBase64String:appDelegate.lastName
															keyString:AES_KEY
														separateLines:NO];
	
	NSString* encryptedFirstName = [FBEncryptorAES encryptBase64String:appDelegate.firstName
															 keyString:AES_KEY
														 separateLines:NO];
	
	NSString* encryptedLastNameKana = [FBEncryptorAES encryptBase64String:appDelegate.lastNameKana
																keyString:AES_KEY
															separateLines:NO];
	
	NSString* encryptedFirstNameKana = [FBEncryptorAES encryptBase64String:appDelegate.firstNameKana
																 keyString:AES_KEY
															 separateLines:NO];
	
	NSString* encryptedZipCode = [FBEncryptorAES encryptBase64String:appDelegate.zipCode
														   keyString:AES_KEY
													   separateLines:NO];
	
	NSString* encryptedPrefecture = [FBEncryptorAES encryptBase64String:appDelegate.prefecture
															  keyString:AES_KEY
														  separateLines:NO];
	
	NSString* encryptedAddress1 = [FBEncryptorAES encryptBase64String:appDelegate.address1
															keyString:AES_KEY
														separateLines:NO];
	
	NSString* encryptedAddress2 = [FBEncryptorAES encryptBase64String:appDelegate.address2
															keyString:AES_KEY
														separateLines:NO];
	
	NSString* encryptedAddress3 = [FBEncryptorAES encryptBase64String:appDelegate.address3
															keyString:AES_KEY
														separateLines:NO];
	
	NSString* encryptedAddress4 = [FBEncryptorAES encryptBase64String:appDelegate.address4
															keyString:AES_KEY
														separateLines:NO];
	
	NSString* encryptedPhone = [FBEncryptorAES encryptBase64String:appDelegate.phone
														 keyString:AES_KEY
													 separateLines:NO];
	
	NSString* email;
	if ([appDelegate.emailAccount length] && [appDelegate.emailDomain length]) {
		email = [NSString stringWithFormat:@"%@@%@", appDelegate.emailAccount, appDelegate.emailDomain];
	} else {
		email = @"";
	}
	
	NSString* encryptedEmail = [FBEncryptorAES encryptBase64String:email
														 keyString:AES_KEY
													 separateLines:NO];
	
	NSString* currentDate = [UtilityFunction getCurrentDate];
	
	NSString* personalInfo = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",
							  appDelegate.userId, appDelegate.placeCode, currentDate, appDelegate.regular, appDelegate.serialNumber,
							  encryptedLastName, encryptedFirstName, encryptedLastNameKana, encryptedFirstNameKana,
							  encryptedZipCode, encryptedPrefecture, encryptedAddress1, encryptedAddress2, encryptedAddress3,
							  encryptedAddress4, encryptedPhone, encryptedEmail, appDelegate.directMail, appDelegate.mailMagazie, appDelegate.publicInfo];
	
	if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
		NSString* q1Answer = [NSString stringWithFormat:@"%@%@%@00000000000000000",
							  appDelegate.q1Answer01, appDelegate.q1Answer02, appDelegate.q1Answer03];
		NSString* q2Answer = [NSString stringWithFormat:@"%@%@%@%@%@00000000000000%@",
							  appDelegate.q2Answer01, appDelegate.q2Answer02, appDelegate.q2Answer03, appDelegate.q2Answer04,
							  appDelegate.q2Answer05, appDelegate.q2Answer20];
		NSString* q3Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",
							  appDelegate.q3Answer01, appDelegate.q3Answer02, appDelegate.q3Answer03, appDelegate.q3Answer04,
							  appDelegate.q3Answer05, appDelegate.q3Answer06, appDelegate.q3Answer07, appDelegate.q3Answer08,
							  appDelegate.q3Answer09, appDelegate.q3Answer10, appDelegate.q3Answer11, appDelegate.q3Answer12,
							  appDelegate.q3Answer13, appDelegate.q3Answer14, appDelegate.q3Answer15, appDelegate.q3Answer16,
							  appDelegate.q3Answer17, appDelegate.q3Answer18, appDelegate.q3Answer19, appDelegate.q3Answer20];
		NSString* q4Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@0000000%@",
							  appDelegate.q4Answer01, appDelegate.q4Answer02, appDelegate.q4Answer03, appDelegate.q4Answer04,
							  appDelegate.q4Answer05, appDelegate.q4Answer06, appDelegate.q4Answer07, appDelegate.q4Answer08,
							  appDelegate.q4Answer09, appDelegate.q4Answer10, appDelegate.q4Answer11, appDelegate.q4Answer12,
							  appDelegate.q4Answer20];
		NSString* q5Answer = [NSString stringWithFormat:@"%@%@%@%@000000000000000%@",
							   appDelegate.q5Answer01, appDelegate.q5Answer02, appDelegate.q5Answer03, appDelegate.q5Answer04,
							   appDelegate.q5Answer20];
		NSString* q6Answer = [NSString stringWithFormat:@"%@%@%@%@0000000000000000",
							  appDelegate.q6Answer01, appDelegate.q6Answer02, appDelegate.q6Answer03, appDelegate.q6Answer04];
		NSString* q7Answer = [NSString stringWithFormat:@"%@%@%@%@0000000000000000",
							  appDelegate.q7Answer01, appDelegate.q7Answer02, appDelegate.q7Answer03, appDelegate.q7Answer04];
		NSString* q8Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@0000000000000%@",
							  appDelegate.q8Answer01, appDelegate.q8Answer02, appDelegate.q8Answer03, appDelegate.q8Answer04,
							  appDelegate.q8Answer05, appDelegate.q8Answer06, appDelegate.q8Answer20];
		NSString* q9Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@%@0000000000000",
							  appDelegate.q9Answer01, appDelegate.q9Answer02, appDelegate.q9Answer03, appDelegate.q9Answer04,
							  appDelegate.q9Answer05, appDelegate.q9Answer06, appDelegate.q9Answer07];
		NSString* q10Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@0000000000",
							   appDelegate.q10Answer01, appDelegate.q10Answer02, appDelegate.q10Answer03, appDelegate.q10Answer04,
							   appDelegate.q10Answer05, appDelegate.q10Answer06, appDelegate.q10Answer07, appDelegate.q10Answer08,
							   appDelegate.q10Answer09, appDelegate.q10Answer10];
		NSString* q11Answer = [NSString stringWithFormat:@"%@%@%@00000000000000000",
							  appDelegate.q11Answer01, appDelegate.q11Answer02, appDelegate.q11Answer03];
		NSString* q12Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@00000000000000",
							  appDelegate.q12Answer01, appDelegate.q12Answer02, appDelegate.q12Answer03, appDelegate.q12Answer04,
							  appDelegate.q12Answer05, appDelegate.q12Answer06];
		NSString* q13Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",
							  appDelegate.q13Answer01, appDelegate.q13Answer02, appDelegate.q13Answer03, appDelegate.q13Answer04,
							  appDelegate.q13Answer05, appDelegate.q13Answer06, appDelegate.q13Answer07, appDelegate.q13Answer08,
							  appDelegate.q13Answer09, appDelegate.q13Answer10, appDelegate.q13Answer11, appDelegate.q13Answer12,
							  appDelegate.q13Answer13, appDelegate.q13Answer14, appDelegate.q13Answer15, appDelegate.q13Answer16,
							  appDelegate.q13Answer17, appDelegate.q13Answer18, appDelegate.q13Answer19, appDelegate.q13Answer20];
		NSString* q14Answer = [NSString stringWithFormat:@"%@%@%@%@%@000000000000000",
							  appDelegate.q14Answer01, appDelegate.q14Answer02, appDelegate.q14Answer03, appDelegate.q14Answer04,
							  appDelegate.q14Answer05];
		NSString* q15Answer = [NSString stringWithFormat:@"%@%@%@%@%@000000000000000",
							  appDelegate.q15Answer01, appDelegate.q15Answer02, appDelegate.q15Answer03, appDelegate.q15Answer04,
							  appDelegate.q15Answer05];
		NSString* q16Answer = [NSString stringWithFormat:@"%@%@%@%@%@00000000000000%@",
							   appDelegate.q16Answer01, appDelegate.q16Answer02, appDelegate.q16Answer03, appDelegate.q16Answer04,
							   appDelegate.q16Answer05, appDelegate.q16Answer20];
		NSString* q17Answer = [NSString stringWithFormat:@"%@%@%@0000000000000000%@",
							  appDelegate.q17Answer01, appDelegate.q17Answer02, appDelegate.q17Answer03, appDelegate.q17Answer20];
		NSString* q18Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@00000000000",
							   appDelegate.q18Answer01, appDelegate.q18Answer02, appDelegate.q18Answer03, appDelegate.q18Answer04,
							   appDelegate.q18Answer05, appDelegate.q18Answer06, appDelegate.q18Answer07, appDelegate.q18Answer08,
							   appDelegate.q18Answer09];
		NSString* q19Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",
							   appDelegate.q19Answer01, appDelegate.q19Answer02, appDelegate.q19Answer03, appDelegate.q19Answer04,
							   appDelegate.q19Answer05, appDelegate.q19Answer06, appDelegate.q19Answer07, appDelegate.q19Answer08,
							   appDelegate.q19Answer09, appDelegate.q19Answer10, appDelegate.q19Answer11, appDelegate.q19Answer12,
							   appDelegate.q19Answer13, appDelegate.q19Answer14, appDelegate.q19Answer15, appDelegate.q19Answer16,
							   appDelegate.q19Answer17, appDelegate.q19Answer18, appDelegate.q19Answer19, appDelegate.q19Answer20];
		NSString* q20Answer = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@",
							   appDelegate.q20Answer01, appDelegate.q20Answer02, appDelegate.q20Answer03, appDelegate.q20Answer04,
							   appDelegate.q20Answer05, appDelegate.q20Answer06, appDelegate.q20Answer07, appDelegate.q20Answer08,
							   appDelegate.q20Answer09, appDelegate.q20Answer10, appDelegate.q20Answer11, appDelegate.q20Answer12,
							   appDelegate.q20Answer13, appDelegate.q20Answer14, appDelegate.q20Answer15, appDelegate.q20Answer16,
							   appDelegate.q20Answer17, appDelegate.q20Answer18, appDelegate.q20Answer19, appDelegate.q20Answer20];

		personalInfo = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@",
						personalInfo, q1Answer, q2Answer, q3Answer, q4Answer, q5Answer, q6Answer, q7Answer, q8Answer, q9Answer, q10Answer,
						q11Answer, q12Answer, q13Answer, q14Answer, q15Answer, q16Answer, q17Answer, q18Answer, q19Answer, q20Answer,
						appDelegate.q5Answer02Text, appDelegate.q7Answer01Text];
	}
	_Log(@"personalInfo = %@", personalInfo);

	sql = @"INSERT INTO answer (personal_info, delete_flag) VALUES (?, ?)";
	[db open];
	[db executeUpdate:sql, personalInfo, @"0"];
	[db close];
	
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:@"1" forKey:@"saveAnswer"];
	BOOL successful = [defaults synchronize];
	if (successful) {
		[UtilityFunction resetPersonalInfo];
		_Log(@"%@", @"データの保存に成功しました。");
	}
}

@end
