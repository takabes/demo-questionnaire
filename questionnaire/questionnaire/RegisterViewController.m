//
//  RegisterViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "RegisterViewController.h"
#import "CompleteViewController.h"
#import "PublickCheckViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "AppConsts.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"

@interface RegisterViewController ()
@end

@implementation RegisterViewController

@synthesize showPublicInfo = _showPublicInfo;

@synthesize nameLabel = _nameLabel;
@synthesize nameKanaLabel = _nameKanaLabel;
@synthesize zipCodeLabel = _zipCodeLabel;
@synthesize address1Label = _address1Label;
@synthesize address2Label = _address2Label;
@synthesize address3Label = _address3Label;
@synthesize phoneLabel = _phoneLabel;
@synthesize emailLabel = _emailLabel;

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;
@synthesize checkBox1Button = _checkBox1Button;
@synthesize checkBox2Button = _checkBox2Button;
@synthesize label1 = _label1;
@synthesize label2 = _label2;
@synthesize label3 = _label3;
@synthesize label4 = _label4;
@synthesize imageView = _imageView;

- (id)init {
    self = [super initWithNibName:@"Register" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"登録画面";

    return self;
}

- (void)viewDidLoad {
	
	_checkStatus1 = NO;
	_checkStatus2 = NO;
	_checkStatus3 = NO;
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	if ([_showPublicInfo isEqualToString:@"1"]) {
		_headerLabel.text = @"入力した内容でよろしければ、【次へ】ボタンを押して下さい。";
		[_nextButton setTitle:@"次へ" forState:UIControlStateNormal];
		_checkBox1Button.hidden = YES;
		_checkBox2Button.hidden = YES;
		_label1.hidden = YES;
		_label2.hidden = YES;
		_label3.hidden = YES;
		_label4.hidden = YES;
		_imageView.hidden = YES;
	}
	
	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
	
	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	_nameLabel.text = [NSString stringWithFormat:@"%@　%@", appDelegate.lastName, appDelegate.firstName];
	_nameKanaLabel.text = [NSString stringWithFormat:@"%@　%@", appDelegate.lastNameKana, appDelegate.firstNameKana];
	_zipCodeLabel.text = appDelegate.zipCode;
	_address1Label.text = [NSString stringWithFormat:@"%@%@%@", appDelegate.prefecture, appDelegate.address1, appDelegate.address2];
	_address2Label.text = appDelegate.address3;
	_address3Label.text = appDelegate.address4;
	_phoneLabel.text = appDelegate.phone;
	if ([appDelegate.emailAccount length] && [appDelegate.emailDomain length]) {
		_emailLabel.text = [NSString stringWithFormat:@"%@@%@", appDelegate.emailAccount, appDelegate.emailDomain];
	} else {
		_emailLabel.text = @"";
	}

	[super viewWillAppear:animated];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (IBAction)checkButton1Tapped:(UIButton*)button {
	if(_checkStatus1){
		[_checkBox1Button setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
	else {
		[_checkBox1Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	_checkStatus1  = !_checkStatus1;
}

- (IBAction)checkButton2Tapped:(UIButton*)button {
	if(_checkStatus2){
		[_checkBox2Button setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
	else {
		[_checkBox2Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	_checkStatus2  = !_checkStatus2;
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	if (_checkStatus1) {
		appDelegate.directMail = @"1";
	} else {
		appDelegate.directMail = @"0";
	}
	
	if (_checkStatus2) {
		appDelegate.mailMagazie = @"1";
	} else {
		appDelegate.mailMagazie = @"0";
	}
	
//	if ([_showPublicInfo isEqualToString:@"1"]) {
//		if (_checkStatus3) {
//			appDelegate.publicInfo = @"1";
//		} else {
//			appDelegate.publicInfo = @"0";
//		}
//	}
	_Log(@"%@", appDelegate.directMail);
	_Log(@"%@", appDelegate.mailMagazie);
	_Log(@"%@", appDelegate.publicInfo);
	
	[self showNextPage];
}

- (void)showNextPage {
	
	if ([_showPublicInfo isEqualToString:@"1"]) {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		 PublickCheckViewController *viewController = [[PublickCheckViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	}
	else {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		CompleteViewController *viewController = [[CompleteViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中の内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		[UtilityFunction resetPersonalInfo];
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}
		
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
