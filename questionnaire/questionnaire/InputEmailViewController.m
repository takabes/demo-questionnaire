//
//  InputEmailViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "InputEmailViewController.h"
#import "CompleteViewController.h"
#import "ConfirmViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "AppConsts.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"

@interface InputEmailViewController ()
@end

@implementation InputEmailViewController

@synthesize edit = _edit;

@synthesize accountTextField = _accountTextField;
@synthesize domainTextField = _domainTextField;

@synthesize oneButton = _oneButton;
@synthesize twoButton = _twoButton;
@synthesize threeButton = _threeButton;
@synthesize fourButton = _fourButton;
@synthesize fiveButton = _fiveButton;
@synthesize sixButton = _sixButton;
@synthesize sevenButton = _sevenButton;
@synthesize eightButton = _eightButton;
@synthesize nineButton = _nineButton;
@synthesize zeroButton = _zeroButton;

@synthesize qButton = _qButton;
@synthesize wButton = _wButton;
@synthesize eButton = _eButton;
@synthesize rButton = _rButton;
@synthesize tButton = _tButton;
@synthesize yButton = _yButton;
@synthesize uButton = _uButton;
@synthesize iButton = _iButton;
@synthesize oButton = _oButton;
@synthesize pButton = _pButton;
@synthesize hyphenButton = _hyphenButton;

@synthesize aButton = _aButton;
@synthesize sButton = _sButton;
@synthesize dButton = _dButton;
@synthesize fButton = _fButton;
@synthesize gButton = _gButton;
@synthesize hButton = _hButton;
@synthesize jButton = _jButton;
@synthesize kButton = _kButton;
@synthesize lButton = _lButton;

@synthesize zButton = _zButton;
@synthesize xButton = _xButton;
@synthesize cButton = _cButton;
@synthesize vButton = _vButton;
@synthesize bButton = _bButton;
@synthesize nButton = _nButton;
@synthesize mButton = _mButton;
@synthesize dotButton = _dotButton;
@synthesize underscoreButton = _underscoreButton;

@synthesize deleteButton = _deleteButton;
@synthesize domainBUtton = _domainBUtton;

@synthesize domain1BUtton = _domain1BUtton;
@synthesize domain2BUtton = _domain2BUtton;
@synthesize domain3BUtton = _domain3BUtton;
@synthesize domain4BUtton = _domain4BUtton;
@synthesize domain5BUtton = _domain5BUtton;
@synthesize domain6BUtton = _domain6BUtton;
@synthesize domain7BUtton = _domain7BUtton;
@synthesize domain8BUtton = _domain8BUtton;

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;
@synthesize showKeyboardButton = _showKeyboardButton;

@synthesize picker = _picker;
@synthesize submitDomainButton = _submitDomainButton;
@synthesize cancelDomainButton = _cancelDomainButton;

- (id)init {
    self = [super initWithNibName:@"InputEmail" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"メールアドレス入力";

    return self;
}

- (void)viewDidLoad {
	
	_currentTextFieldTag = 1;
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;
	
	[self initSwipe];

	[_accountTextField setDelegate:self];
	[_domainTextField setDelegate:self];

	[self initButton];
	[self initDomainButton];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	_accountTextField.text = appDelegate.emailAccount;
	_domainTextField.text = appDelegate.emailDomain;

	[super viewDidLoad];
}


- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initPicker {
	
	_domainBUtton.hidden = YES;
	_submitDomainButton.hidden = NO;
	_cancelDomainButton.hidden = YES;
	
	_picker = [[UIPickerView alloc] init];
	_picker.frame = CGRectMake(_domainTextField.frame.origin.x, 216, _domainTextField.frame.size.width, 150);
	_picker.backgroundColor = [UtilityFunction colorWithRBGA:0xf5f5f5];
	_picker.hidden = NO;
	[self.view addSubview:_picker];
	
	[_picker setDelegate:self];
	[_picker setDataSource:self];
}

- (void)initButton {

	[_oneButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_twoButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_threeButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fourButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fiveButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sixButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sevenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_eightButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_nineButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_zeroButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_qButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_wButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_eButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_rButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_tButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_yButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_uButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_iButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_oButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_pButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_hyphenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_aButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_dButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_gButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_hButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_jButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_kButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_lButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_zButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_xButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_cButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_vButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_bButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_nButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_mButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_dotButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_underscoreButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
	[UtilityFunction customizeButton:_showKeyboardButton];
	[UtilityFunction customizeButton:_submitDomainButton];

	[UtilityFunction customizeKeyboardButton:_oneButton];
	[UtilityFunction customizeKeyboardButton:_twoButton];
	[UtilityFunction customizeKeyboardButton:_threeButton];
	[UtilityFunction customizeKeyboardButton:_fourButton];
	[UtilityFunction customizeKeyboardButton:_fiveButton];
	[UtilityFunction customizeKeyboardButton:_sixButton];
	[UtilityFunction customizeKeyboardButton:_sevenButton];
	[UtilityFunction customizeKeyboardButton:_eightButton];
	[UtilityFunction customizeKeyboardButton:_nineButton];
	[UtilityFunction customizeKeyboardButton:_zeroButton];
	[UtilityFunction customizeKeyboardButton:_deleteButton];
	
	[UtilityFunction customizeKeyboardButton:_qButton];
	[UtilityFunction customizeKeyboardButton:_wButton];
	[UtilityFunction customizeKeyboardButton:_eButton];
	[UtilityFunction customizeKeyboardButton:_rButton];
	[UtilityFunction customizeKeyboardButton:_tButton];
	[UtilityFunction customizeKeyboardButton:_yButton];
	[UtilityFunction customizeKeyboardButton:_uButton];
	[UtilityFunction customizeKeyboardButton:_iButton];
	[UtilityFunction customizeKeyboardButton:_oButton];
	[UtilityFunction customizeKeyboardButton:_pButton];
	[UtilityFunction customizeKeyboardButton:_hyphenButton];
	
	[UtilityFunction customizeKeyboardButton:_aButton];
	[UtilityFunction customizeKeyboardButton:_sButton];
	[UtilityFunction customizeKeyboardButton:_dButton];
	[UtilityFunction customizeKeyboardButton:_fButton];
	[UtilityFunction customizeKeyboardButton:_gButton];
	[UtilityFunction customizeKeyboardButton:_hButton];
	[UtilityFunction customizeKeyboardButton:_jButton];
	[UtilityFunction customizeKeyboardButton:_kButton];
	[UtilityFunction customizeKeyboardButton:_lButton];
	
	[UtilityFunction customizeKeyboardButton:_zButton];
	[UtilityFunction customizeKeyboardButton:_xButton];
	[UtilityFunction customizeKeyboardButton:_cButton];
	[UtilityFunction customizeKeyboardButton:_vButton];
	[UtilityFunction customizeKeyboardButton:_bButton];
	[UtilityFunction customizeKeyboardButton:_nButton];
	[UtilityFunction customizeKeyboardButton:_mButton];
	[UtilityFunction customizeKeyboardButton:_dotButton];
	[UtilityFunction customizeKeyboardButton:_underscoreButton];
}

- (void)initDomainButton {
	
	[_domain1BUtton addTarget:self action:@selector(domainButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_domain2BUtton addTarget:self action:@selector(domainButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_domain3BUtton addTarget:self action:@selector(domainButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_domain4BUtton addTarget:self action:@selector(domainButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_domain5BUtton addTarget:self action:@selector(domainButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_domain6BUtton addTarget:self action:@selector(domainButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_domain7BUtton addTarget:self action:@selector(domainButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_domain8BUtton addTarget:self action:@selector(domainButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[UtilityFunction customizeKeyboardButton:_domain1BUtton];
	[UtilityFunction customizeKeyboardButton:_domain2BUtton];
	[UtilityFunction customizeKeyboardButton:_domain3BUtton];
	[UtilityFunction customizeKeyboardButton:_domain4BUtton];
	[UtilityFunction customizeKeyboardButton:_domain5BUtton];
	[UtilityFunction customizeKeyboardButton:_domain6BUtton];
	[UtilityFunction customizeKeyboardButton:_domain7BUtton];
	[UtilityFunction customizeKeyboardButton:_domain8BUtton];
}

- (void)viewDidAppear:(BOOL)animated {
	[_accountTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
	[super viewDidAppear:animated];
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
	
	if (textField.tag == 1) {
		[_domainTextField becomeFirstResponder];
		return NO;
	}
	
	return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	
	_currentTextFieldTag = textField.tag;
	
	if (_currentTextFieldTag == 1) {
		_accountTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
		_domainTextField.backgroundColor = [UIColor whiteColor];
	} else if (_currentTextFieldTag == 2) {
		_accountTextField.backgroundColor = [UIColor whiteColor];
		_domainTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
	}

	return YES;
}

- (void)keyboardButtonTapped:(UIButton*)button {
	
	if (_currentTextFieldTag == 1) {
		_accountTextField.text = [NSString stringWithFormat:@"%@%@", _accountTextField.text, [UtilityFunction getCharacter:button.tag]];
	} else {
		_domainTextField.text = [NSString stringWithFormat:@"%@%@", _domainTextField.text, [UtilityFunction getCharacter:button.tag]];
	}
}

- (void)deleteButtonTapped:(UIButton*)button {
	_Log(@"deleteButtonTapped");
	
	if (_currentTextFieldTag == 1) {
		if ([_accountTextField.text length]) {
			NSString* text = [_accountTextField.text substringToIndex:[_accountTextField.text length] - 1];
			_accountTextField.text = text;
		}
	} else {
		if ([_domainTextField.text length]) {
			NSString* text = [_domainTextField.text substringToIndex:[_domainTextField.text length] - 1];
			_domainTextField.text = text;
		}
	}
}

- (void)domainButtonTapped:(UIButton*)button {
	_Log(@"domainButtonTapped");
	
	_accountTextField.backgroundColor = [UIColor whiteColor];
	_domainTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
	[_domainTextField becomeFirstResponder];

	_domainTextField.text = button.titleLabel.text;
}

- (IBAction)submitDomainButtonTapped:(UIButton*)button {
	//_picker.hidden = YES;
	//_submitDomainButton.hidden = YES;
	//_cancelDomainButton.hidden = YES;
	//_domainBUtton.hidden = NO;
	//_nextButton.hidden = NO;
	
	if ([_domainTextField.text length] == 0) {
		NSUInteger row = [_picker selectedRowInComponent:0];
		_domainTextField.text = [self getDomain:row];
	} else {
		if ([_domainTextField.text isEqualToString:@"docomo.ne.jp"] ||
			[_domainTextField.text isEqualToString:@"ezweb.ne.jp"] ||
			[_domainTextField.text isEqualToString:@"i.softbank.jp"] ||
			[_domainTextField.text isEqualToString:@"softbank.ne.jp"] ||
			[_domainTextField.text isEqualToString:@"yahoo.co.jp"] ||
			[_domainTextField.text isEqualToString:@"gmail.com"] ||
			[_domainTextField.text isEqualToString:@"hotmail.com"] ||
			[_domainTextField.text isEqualToString:@"hotmail.co.jp"]) {
			NSUInteger row = [_picker selectedRowInComponent:0];
			_domainTextField.text = [self getDomain:row];
		}
	}
}

- (IBAction)cancelDomainButtonTapped:(UIButton*)button {
	_picker.hidden = YES;
	_submitDomainButton.hidden = YES;
	//_cancelDomainButton.hidden = YES;
	_domainBUtton.hidden = NO;
	//_nextButton.hidden = NO;
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	if ([_accountTextField.text length]) {
		if ([UtilityFunction isAllHalfWidthCharacter:_accountTextField.text] != YES) {
			[self showAlert:@"入力項目" message:@"メールアドレスは半角で入力してください"];
			return;
		}
		
		if ([_domainTextField.text length] == 0) {
			[self showAlert:@"入力項目" message:@"メールアドレスを入力してください"];
			return;
		}
	}

	if ([_domainTextField.text length]) {
		if ([UtilityFunction isAllHalfWidthCharacter:_domainTextField.text] != YES) {
			[self showAlert:@"入力項目" message:@"メールアドレスは半角で入力してください"];
			return;
		}

		if ([_accountTextField.text length] == 0) {
			[self showAlert:@"入力項目" message:@"メールアドレスを入力してください"];
			return;
		}
	}

	if ([_accountTextField.text length] + [_domainTextField.text length] >= 254) {
		[self showAlert:@"入力項目" message:@"メールアドレスは256文字以内で入力してください"];
		return;
	}
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.emailAccount = _accountTextField.text;
	appDelegate.emailDomain = _domainTextField.text;

	if (_edit) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		ConfirmViewController *viewController = [[ConfirmViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

//--------------------------------------------------------------//
#pragma mark -- Picker Data Source Methods --
//--------------------------------------------------------------//

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	return 8;
}

//--------------------------------------------------------------//
#pragma mark -- Picker Delegate Methods --
//--------------------------------------------------------------//

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
	return 200;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	
	return [self getDomain:row];
}

//--------------------------------------------------------------//
#pragma mark -- Common Methods --
//--------------------------------------------------------------//

- (NSString*)getDomain:(NSUInteger)row {
	
	switch (row) {
		case 0:
			return @"docomo.ne.jp";
			break;
		case 1:
			return @"ezweb.ne.jp";
			break;
		case 2:
			return @"i.softbank.jp";
			break;
		case 3:
			return @"softbank.ne.jp";
			break;
		case 4:
			return @"yahoo.co.jp";
			break;
		case 5:
			return @"gmail.com";
			break;
		case 6:
			return @"hotmail.com";
			break;
		case 7:
			return @"hotmail.co.jp";
			break;
		default:
			break;
	}
	return @"";
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)showKeyboardButtonTapped:(UIButton*)button {
	if (_currentTextFieldTag == 1) {
		[_accountTextField becomeFirstResponder];
	} else {
		[_domainTextField becomeFirstResponder];
	}
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中の内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		[UtilityFunction resetPersonalInfo];
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}
		
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
