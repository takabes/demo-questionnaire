//
//  Question05ViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "Question05ViewController.h"
#import "Question06ViewController.h"
#import "AppDelegate.h"
#import "UtilityFunction.h"
#import "QuestionListViewController.h"

@interface Question05ViewController ()
@end

@implementation Question05ViewController

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"Question05" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"アンケート入力";

    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];
	
	[self initHeaderLabel];
	
	[self initButton];
	
	[self initQ13Choice];

	[self initQ13Answer];

	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initHeaderLabel {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	if (questions[@"q19"]) {
		if (questions[@"q20"]) {
			_headerLabel.text = @"5 / 9";
		} else {
			_headerLabel.text = @"5 / 8";
		}
	} else {
		_headerLabel.text = @"5 / 7";
	}
}

- (void)initButton {
	
	[_q13Answer01Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer02Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer03Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer04Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer05Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer06Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer07Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer08Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer09Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer10Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer11Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer12Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer13Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer14Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer15Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer16Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer17Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer18Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer19Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q13Answer20Button addTarget:self action:@selector(q13AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
}

- (void)initQ13Choice {

	_q13Answer01Button.hidden = YES;
	_q13Answer02Button.hidden = YES;
	_q13Answer03Button.hidden = YES;
	_q13Answer04Button.hidden = YES;
	_q13Answer05Button.hidden = YES;
	_q13Answer06Button.hidden = YES;
	_q13Answer07Button.hidden = YES;
	_q13Answer08Button.hidden = YES;
	_q13Answer09Button.hidden = YES;
	_q13Answer10Button.hidden = YES;
	_q13Answer11Button.hidden = YES;
	_q13Answer12Button.hidden = YES;
	_q13Answer13Button.hidden = YES;
	_q13Answer14Button.hidden = YES;
	_q13Answer15Button.hidden = YES;
	_q13Answer16Button.hidden = YES;
	_q13Answer17Button.hidden = YES;
	_q13Answer18Button.hidden = YES;
	_q13Answer19Button.hidden = YES;
	_q13Answer20Button.hidden = YES;
		
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	NSArray* q13Choices = questions[@"q13"][@"choices"];
	
	for (int i=0; i < [q13Choices count]; i++) {
		
		switch (i) {
			case 0:
				_q13Answer01Button.hidden = NO;
				[_q13Answer01Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 1:
				_q13Answer02Button.hidden = NO;
				[_q13Answer02Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 2:
				_q13Answer03Button.hidden = NO;
				[_q13Answer03Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 3:
				_q13Answer04Button.hidden = NO;
				[_q13Answer04Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 4:
				_q13Answer05Button.hidden = NO;
				[_q13Answer05Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 5:
				_q13Answer06Button.hidden = NO;
				[_q13Answer06Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 6:
				_q13Answer07Button.hidden = NO;
				[_q13Answer07Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 7:
				_q13Answer08Button.hidden = NO;
				[_q13Answer08Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 8:
				_q13Answer09Button.hidden = NO;
				[_q13Answer09Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 9:
				_q13Answer10Button.hidden = NO;
				[_q13Answer10Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 10:
				_q13Answer11Button.hidden = NO;
				[_q13Answer11Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 11:
				_q13Answer12Button.hidden = NO;
				[_q13Answer12Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 12:
				_q13Answer13Button.hidden = NO;
				[_q13Answer13Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 13:
				_q13Answer14Button.hidden = NO;
				[_q13Answer14Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 14:
				_q13Answer15Button.hidden = NO;
				[_q13Answer15Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 15:
				_q13Answer16Button.hidden = NO;
				[_q13Answer16Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 16:
				_q13Answer17Button.hidden = NO;
				[_q13Answer17Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 17:
				_q13Answer18Button.hidden = NO;
				[_q13Answer18Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 18:
				_q13Answer19Button.hidden = NO;
				[_q13Answer19Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 19:
				_q13Answer20Button.hidden = NO;
				[_q13Answer20Button setTitle:[q13Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
		}
	}
	
	NSString* q13OtherChoice = questions[@"q13"][@"otherChoice"];
	
	if (q13OtherChoice && [q13OtherChoice length]) {
		switch ([q13Choices count]) {
			case 1:
				_q13Answer02Button.hidden = NO;
				_q13Answer02Button.tag = 20;
				[_q13Answer02Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 2:
				_q13Answer03Button.hidden = NO;
				_q13Answer03Button.tag = 20;
				[_q13Answer03Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 3:
				_q13Answer04Button.hidden = NO;
				_q13Answer04Button.tag = 20;
				[_q13Answer04Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 4:
				_q13Answer05Button.hidden = NO;
				_q13Answer05Button.tag = 20;
				[_q13Answer05Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 5:
				_q13Answer06Button.hidden = NO;
				_q13Answer06Button.tag = 20;
				[_q13Answer06Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 6:
				_q13Answer07Button.hidden = NO;
				_q13Answer07Button.tag = 20;
				[_q13Answer07Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 7:
				_q13Answer07Button.hidden = NO;
				_q13Answer07Button.tag = 20;
				[_q13Answer07Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 8:
				_q13Answer09Button.hidden = NO;
				_q13Answer09Button.tag = 20;
				[_q13Answer09Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 9:
				_q13Answer10Button.hidden = NO;
				_q13Answer10Button.tag = 20;
				[_q13Answer10Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 10:
				_q13Answer11Button.hidden = NO;
				_q13Answer11Button.tag = 20;
				[_q13Answer11Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 11:
				_q13Answer12Button.hidden = NO;
				_q13Answer12Button.tag = 20;
				[_q13Answer12Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 12:
				_q13Answer13Button.hidden = NO;
				_q13Answer13Button.tag = 20;
				[_q13Answer13Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 13:
				_q13Answer14Button.hidden = NO;
				_q13Answer14Button.tag = 20;
				[_q13Answer14Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 14:
				_q13Answer15Button.hidden = NO;
				_q13Answer15Button.tag = 20;
				[_q13Answer15Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 15:
				_q13Answer16Button.hidden = NO;
				_q13Answer16Button.tag = 20;
				[_q13Answer16Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 16:
				_q13Answer17Button.hidden = NO;
				_q13Answer17Button.tag = 20;
				[_q13Answer17Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 17:
				_q13Answer18Button.hidden = NO;
				_q13Answer18Button.tag = 20;
				[_q13Answer18Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 18:
				_q13Answer19Button.hidden = NO;
				_q13Answer19Button.tag = 20;
				[_q13Answer19Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
			case 19:
				_q13Answer20Button.hidden = NO;
				_q13Answer20Button.tag = 20;
				[_q13Answer20Button setTitle:q13OtherChoice forState:UIControlStateNormal];
				break;
		}
	}
}

- (void)initQ13Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q13Answer01 isEqualToString:@"1"]) {
		[_q13Answer01Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer02 isEqualToString:@"1"]) {
		[_q13Answer02Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer03 isEqualToString:@"1"]) {
		[_q13Answer03Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer04 isEqualToString:@"1"]) {
		[_q13Answer04Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer05 isEqualToString:@"1"]) {
		[_q13Answer05Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer06 isEqualToString:@"1"]) {
		[_q13Answer06Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer07 isEqualToString:@"1"]) {
		[_q13Answer07Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer08 isEqualToString:@"1"]) {
		[_q13Answer08Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer09 isEqualToString:@"1"]) {
		[_q13Answer09Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer10 isEqualToString:@"1"]) {
		[_q13Answer10Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer11 isEqualToString:@"1"]) {
		[_q13Answer11Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer12 isEqualToString:@"1"]) {
		[_q13Answer12Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer13 isEqualToString:@"1"]) {
		[_q13Answer13Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer14 isEqualToString:@"1"]) {
		[_q13Answer14Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer15 isEqualToString:@"1"]) {
		[_q13Answer15Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer16 isEqualToString:@"1"]) {
		[_q13Answer16Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer17 isEqualToString:@"1"]) {
		[_q13Answer17Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer18 isEqualToString:@"1"]) {
		[_q13Answer18Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer19 isEqualToString:@"1"]) {
		[_q13Answer19Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q13Answer20 isEqualToString:@"1"]) {
		
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		id questions = [defaults objectForKey:@"questions"];
		NSArray* q13Choices = questions[@"q13"][@"choices"];
		
		switch ([q13Choices count]) {
			case 1:
				[_q13Answer02Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 2:
				[_q13Answer03Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 3:
				[_q13Answer04Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 4:
				[_q13Answer05Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 5:
				[_q13Answer06Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 6:
				[_q13Answer07Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 7:
				[_q13Answer08Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 8:
				[_q13Answer09Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 9:
				[_q13Answer10Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 10:
				[_q13Answer11Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 11:
				[_q13Answer12Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 12:
				[_q13Answer13Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 13:
				[_q13Answer14Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 14:
				[_q13Answer15Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 15:
				[_q13Answer16Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 16:
				[_q13Answer17Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 17:
				[_q13Answer18Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 18:
				[_q13Answer19Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
			case 19:
				[_q13Answer20Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
				break;
		}
	}
}

- (void)q13AnswerButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	BOOL check = YES;
	
	switch (button.tag) {
		case 1:
			if ([appDelegate.q13Answer01 isEqualToString:@"0"]) {
				appDelegate.q13Answer01 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer01 = @"0";
				check = NO;
			}
			break;
		case 2:
			if ([appDelegate.q13Answer02 isEqualToString:@"0"]) {
				appDelegate.q13Answer02 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer02 = @"0";
				check = NO;
			}
			break;
		case 3:
			if ([appDelegate.q13Answer03 isEqualToString:@"0"]) {
				appDelegate.q13Answer03 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer03 = @"0";
				check = NO;
			}
			break;
		case 4:
			if ([appDelegate.q13Answer04 isEqualToString:@"0"]) {
				appDelegate.q13Answer04 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer04 = @"0";
				check = NO;
			}
			break;
		case 5:
			if ([appDelegate.q13Answer05 isEqualToString:@"0"]) {
				appDelegate.q13Answer05 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer05 = @"0";
				check = NO;
			}
			break;
		case 6:
			if ([appDelegate.q13Answer06 isEqualToString:@"0"]) {
				appDelegate.q13Answer06 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer06 = @"0";
				check = NO;
			}
			break;
		case 7:
			if ([appDelegate.q13Answer07 isEqualToString:@"0"]) {
				appDelegate.q13Answer07 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer07 = @"0";
				check = NO;
			}
			break;
		case 8:
			if ([appDelegate.q13Answer08 isEqualToString:@"0"]) {
				appDelegate.q13Answer08 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer08 = @"0";
				check = NO;
			}
			break;
		case 9:
			if ([appDelegate.q13Answer09 isEqualToString:@"0"]) {
				appDelegate.q13Answer09 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer09 = @"0";
				check = NO;
			}
			break;
		case 10:
			if ([appDelegate.q13Answer10 isEqualToString:@"0"]) {
				appDelegate.q13Answer10 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer10 = @"0";
				check = NO;
			}
			break;
		case 11:
			if ([appDelegate.q13Answer11 isEqualToString:@"0"]) {
				appDelegate.q13Answer11 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer11 = @"0";
				check = NO;
			}
			break;
		case 12:
			if ([appDelegate.q13Answer12 isEqualToString:@"0"]) {
				appDelegate.q13Answer12 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer12 = @"0";
				check = NO;
			}
			break;
		case 13:
			if ([appDelegate.q13Answer13 isEqualToString:@"0"]) {
				appDelegate.q13Answer13 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer13 = @"0";
				check = NO;
			}
			break;
		case 14:
			if ([appDelegate.q13Answer14 isEqualToString:@"0"]) {
				appDelegate.q13Answer14 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer14 = @"0";
				check = NO;
			}
			break;
		case 15:
			if ([appDelegate.q13Answer15 isEqualToString:@"0"]) {
				appDelegate.q13Answer15 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer15 = @"0";
				check = NO;
			}
			break;
		case 16:
			if ([appDelegate.q13Answer16 isEqualToString:@"0"]) {
				appDelegate.q13Answer16 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer16 = @"0";
				check = NO;
			}
			break;
		case 17:
			if ([appDelegate.q13Answer17 isEqualToString:@"0"]) {
				appDelegate.q13Answer17 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer17 = @"0";
				check = NO;
			}
			break;
		case 18:
			if ([appDelegate.q13Answer18 isEqualToString:@"0"]) {
				appDelegate.q13Answer18 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer18 = @"0";
				check = NO;
			}
			break;
		case 19:
			if ([appDelegate.q13Answer19 isEqualToString:@"0"]) {
				appDelegate.q13Answer19 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer19 = @"0";
				check = NO;
			}
			break;
		case 20:
			if ([appDelegate.q13Answer20 isEqualToString:@"0"]) {
				appDelegate.q13Answer20 = @"1";
				check = YES;
			} else {
				appDelegate.q13Answer20 = @"0";
				check = NO;
			}
			break;
	}
	
	if (check) {
		[button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	} else {
		[button setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	NSString* message = @"";

	if ([appDelegate.q13Answer01 isEqualToString:@"0"] &&
		[appDelegate.q13Answer02 isEqualToString:@"0"] &&
		[appDelegate.q13Answer03 isEqualToString:@"0"] &&
		[appDelegate.q13Answer04 isEqualToString:@"0"] &&
		[appDelegate.q13Answer05 isEqualToString:@"0"] &&
		[appDelegate.q13Answer06 isEqualToString:@"0"] &&
		[appDelegate.q13Answer07 isEqualToString:@"0"] &&
		[appDelegate.q13Answer08 isEqualToString:@"0"] &&
		[appDelegate.q13Answer09 isEqualToString:@"0"] &&
		[appDelegate.q13Answer10 isEqualToString:@"0"] &&
		[appDelegate.q13Answer11 isEqualToString:@"0"] &&
		[appDelegate.q13Answer12 isEqualToString:@"0"] &&
		[appDelegate.q13Answer13 isEqualToString:@"0"] &&
		[appDelegate.q13Answer14 isEqualToString:@"0"] &&
		[appDelegate.q13Answer15 isEqualToString:@"0"] &&
		[appDelegate.q13Answer16 isEqualToString:@"0"] &&
		[appDelegate.q13Answer17 isEqualToString:@"0"] &&
		[appDelegate.q13Answer18 isEqualToString:@"0"] &&
		[appDelegate.q13Answer19 isEqualToString:@"0"] &&
		[appDelegate.q13Answer20 isEqualToString:@"0"]) {
		message = @"Q13の回答を選択してください";
	}

	if ([message length]) {
		[self showAlert:@"必須項目" message:message];
		return;
	}

	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	Question06ViewController *viewController = [[Question06ViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中のアンケート内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[UtilityFunction resetPersonalInfo];
		QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
