//
//  Question07ViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Question07ViewController : UIViewController {
	UILabel*		_headerLabel;

	UIButton*		_q17Answer01Button;
	UIButton*		_q17Answer02Button;
	UIButton*		_q17Answer03Button;
	UIButton*		_q17Answer20Button;

	UIButton*		_q18Answer01Button;
	UIButton*		_q18Answer02Button;
	UIButton*		_q18Answer03Button;
	UIButton*		_q18Answer04Button;
	UIButton*		_q18Answer05Button;
	UIButton*		_q18Answer06Button;
	UIButton*		_q18Answer07Button;
	UIButton*		_q18Answer08Button;
	UIButton*		_q18Answer09Button;

	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UILabel* headerLabel;

@property (nonatomic) IBOutlet UIButton* q17Answer01Button;
@property (nonatomic) IBOutlet UIButton* q17Answer02Button;
@property (nonatomic) IBOutlet UIButton* q17Answer03Button;
@property (nonatomic) IBOutlet UIButton* q17Answer20Button;

@property (nonatomic) IBOutlet UIButton* q18Answer01Button;
@property (nonatomic) IBOutlet UIButton* q18Answer02Button;
@property (nonatomic) IBOutlet UIButton* q18Answer03Button;
@property (nonatomic) IBOutlet UIButton* q18Answer04Button;
@property (nonatomic) IBOutlet UIButton* q18Answer05Button;
@property (nonatomic) IBOutlet UIButton* q18Answer06Button;
@property (nonatomic) IBOutlet UIButton* q18Answer07Button;
@property (nonatomic) IBOutlet UIButton* q18Answer08Button;
@property (nonatomic) IBOutlet UIButton* q18Answer09Button;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
