//
//  InputNameKanaViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "InputNameKanaViewController.h"
#import "InputZipCodeViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "AppConsts.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"

@interface InputNameKanaViewController ()
@end

@implementation InputNameKanaViewController

@synthesize edit = _edit;
@synthesize lastNameTextField = _lastNameTextField;
@synthesize firstNameTextField = _firstNameTextField;
@synthesize lastNameSubmitButton = _lastNameSubmitButton;
@synthesize firstNameSubmitButton = _firstNameSubmitButton;
@synthesize guideImageView = _guideImageView;
@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;
@synthesize showKeyboardButton = _showKeyboardButton;

- (id)init {
    self = [super initWithNibName:@"InputNameKana" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"フリガナ入力";

    return self;
}

- (void)viewDidLoad {
	
	_guideMode = 1;
	_currentTextFieldTag = 1;

	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	[UtilityFunction customizeButton:_lastNameSubmitButton];
	[UtilityFunction customizeButton:_firstNameSubmitButton];
	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
	[UtilityFunction customizeButton:_switchGuiteButton];
	[UtilityFunction customizeButton:_showGuiteButton];
	[UtilityFunction customizeButton:_hideGuiteButton];
	[UtilityFunction customizeButton:_showKeyboardButton];

	[_lastNameTextField setDelegate:self];
	[_firstNameTextField setDelegate:self];

	_switchGuiteButton.hidden = YES;
	_guideImageView.hidden = YES;
	_hideGuiteButton.hidden = YES;

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	_lastNameTextField.text = appDelegate.lastNameKana;
	_firstNameTextField.text = appDelegate.firstNameKana;

	[super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
	[_lastNameTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
	[super viewDidAppear:animated];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	
	_currentTextFieldTag = textField.tag;
	
	if (_currentTextFieldTag == 1) {
		_lastNameTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
		_firstNameTextField.backgroundColor = [UIColor whiteColor];
	} else if (_currentTextFieldTag == 2) {
		_lastNameTextField.backgroundColor = [UIColor whiteColor];
		_firstNameTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
	}

	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
	
	if (textField.tag == 1) {
		[_firstNameTextField becomeFirstResponder];
		return NO;
	}
	
	return YES;
}

- (IBAction)lastNameSubmitButtonTapped:(UIButton*)button {
	[_firstNameTextField becomeFirstResponder];
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	NSString* message = @"";

	if ([_lastNameTextField.text length] == 0) {
		message = @"苗字を入力してください";
	}
	
	if ([_firstNameTextField.text length] == 0) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"名前を入力してください"];
		} else {
			message = @"名前を入力してください";
		}
	}
	
	if ([message length]) {
		[self showAlert:@"入力項目" message:message];
		return;
	}

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.lastNameKana = _lastNameTextField.text;
	appDelegate.firstNameKana = _firstNameTextField.text;
	
	if (_edit) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		InputZipCodeViewController *viewController = [[InputZipCodeViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)switchGuideButtonTapped:(UIButton*)button {
	if (_guideMode == 1) {
		_guideMode = 2;
		[button setTitle:@"未入力状態の説明に切り替える" forState:UIControlStateNormal];
		[_guideImageView setImage:[UIImage imageNamed:@"guide1.jpg"]];
	} else {
		_guideMode = 1;
		[button setTitle:@"入力中状態の説明に切り替える" forState:UIControlStateNormal];
		[_guideImageView setImage:[UIImage imageNamed:@"guide2.jpg"]];
	}
}

- (IBAction)showGuideButtonTapped:(UIButton*)button {
	button.hidden = YES;
	_hideGuiteButton.hidden = NO;
	_switchGuiteButton.hidden = NO;
	_guideImageView.hidden = NO;
}

- (IBAction)hideGuideButtonTapped:(UIButton*)button {
	button.hidden = YES;
	_showGuiteButton.hidden = NO;
	_switchGuiteButton.hidden = YES;
	_guideImageView.hidden = YES;
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)showKeyboardButtonTapped:(UIButton*)button {
	if (_currentTextFieldTag == 1) {
		[_lastNameTextField becomeFirstResponder];
	} else {
		[_firstNameTextField becomeFirstResponder];
	}
}


// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中の内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		[UtilityFunction resetPersonalInfo];
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}
		
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
