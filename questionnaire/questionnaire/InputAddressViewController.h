//
//  InputAddressViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputAddressViewController : UIViewController <UITextFieldDelegate> {
	BOOL			_edit;
	NSUInteger		_cursor;
	NSString*		_prefecture;
	NSString*		_address1;
	NSString*		_address2;
	NSUInteger		_currentTextFieldTag;

	UITextField*	_prefectureTextField;
	UITextField*	_address1TextField;
	UITextField*	_address2TextField;
	UITextField*	_address3TextField;
	UITextField*	_address4TextField;
	
	UIButton*		_address1SubmitButton;
	UIButton*		_address2SubmitButton;
	UIButton*		_address3SubmitButton;
	UIButton*		_address4SubmitButton;
	UIButton*		_address5SubmitButton;

	UIButton*		_oneButton;
	UIButton*		_twoButton;
	UIButton*		_threeButton;
	UIButton*		_fourButton;
	UIButton*		_fiveButton;
	UIButton*		_sixButton;
	UIButton*		_sevenButton;
	UIButton*		_eightButton;
	UIButton*		_nineButton;
	UIButton*		_zeroButton;
	UIButton*		_hyphenButton;
	UIButton*		_deleteButton;

	UIButton*		_nextButton;
	UIButton*		_backButton;
	UIButton*		_showKeyboardButton;
}

@property (nonatomic) BOOL edit;
@property (nonatomic) NSUInteger cursor;
@property (nonatomic) NSString* prefecture;
@property (nonatomic) NSString* address1;
@property (nonatomic) NSString* address2;

@property (nonatomic) IBOutlet UITextField* prefectureTextField;
@property (nonatomic) IBOutlet UITextField* address1TextField;
@property (nonatomic) IBOutlet UITextField* address2TextField;
@property (nonatomic) IBOutlet UITextField* address3TextField;
@property (nonatomic) IBOutlet UITextField* address4TextField;

@property (nonatomic) IBOutlet UIButton* address1SubmitButton;
@property (nonatomic) IBOutlet UIButton* address2SubmitButton;
@property (nonatomic) IBOutlet UIButton* address3SubmitButton;
@property (nonatomic) IBOutlet UIButton* address4SubmitButton;
@property (nonatomic) IBOutlet UIButton* address5SubmitButton;

@property (nonatomic) IBOutlet UIButton* oneButton;
@property (nonatomic) IBOutlet UIButton* twoButton;
@property (nonatomic) IBOutlet UIButton* threeButton;
@property (nonatomic) IBOutlet UIButton* fourButton;
@property (nonatomic) IBOutlet UIButton* fiveButton;
@property (nonatomic) IBOutlet UIButton* sixButton;
@property (nonatomic) IBOutlet UIButton* sevenButton;
@property (nonatomic) IBOutlet UIButton* eightButton;
@property (nonatomic) IBOutlet UIButton* nineButton;
@property (nonatomic) IBOutlet UIButton* zeroButton;
@property (nonatomic) IBOutlet UIButton* hyphenButton;
@property (nonatomic) IBOutlet UIButton* deleteButton;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;
@property (nonatomic) IBOutlet UIButton* showKeyboardButton;

- (IBAction)addressSubmitButtonTapped:(UIButton*)button;
- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;
- (IBAction)deleteButtonTapped:(UIButton*)button;
- (IBAction)showKeyboardButtonTapped:(UIButton*)button;

@end
