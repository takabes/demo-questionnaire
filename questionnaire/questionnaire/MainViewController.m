//
//  MainViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "MainViewController.h"
#import "InputNameViewController.h"
#import "LoginViewController.h"
#import "SampleViewController.h"
#import "InputEmailViewController.h"
#import "InputSerialNumberViewController.h"
#import "CompleteViewController.h"
#import "QuestionListViewController.h"
#import "UtilityFunction.h"
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "AppConsts.h"
#import "CompleteViewController.h"
#import "RegisterViewController.h"

@interface MainViewController ()
@end

@implementation MainViewController


- (id)init {
    self = [super initWithNibName:@"Main" bundle:nil];
    if (!self) {
        return nil;
    }
    
    return self;
}

- (void)viewDidLoad {
	
	[UtilityFunction customizeButton:_accountButton];
	[UtilityFunction customizeButton:_startButton];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSString* saveUser = [defaults stringForKey:@"saveUser"];
	if (saveUser) {
		_accountButton.hidden = YES;
	} else {
		_startButton.hidden = YES;
	}
		
	[super viewDidLoad];
}

- (IBAction)accountButtonTapped:(UIButton*)button {
	
	// インディケータ
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
	
	AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSDictionary* param = @{@"accessKey" : API_ACCESS_KEY};
	[manager POST:API_URL_GET_ACCOUNT parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
		
		_Log(@"response: %@", responseObject);
		
		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		if ([responseObject[@"result"] isEqualToString:@"success"]) {
			[UtilityFunction saveAccounts:responseObject[@"response"][@"accounts"]];
			[self showNextPage];
		} else {
			NSString* message = [NSString stringWithFormat:@"%@:%@", responseObject[@"errorCode"], responseObject[@"errorMessage"]];
			[self showAlert:@"エラー" message:message];
		}
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		
		_Log(@"error = %@", error);
		
		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		[self showAlert:@"エラー" message:@"通信エラーが発生しました。ネットワーク状況を確認して下さい。"];
	}];
}

- (IBAction)startButtonTapped:(UIButton*)button {
	_Log(@"startButtonTapped");
	[self showNextPage];
}

- (void)showNextPage {
	LoginViewController* viewController = [[LoginViewController alloc] init];
	//RegisterViewController* viewController = [[RegisterViewController alloc] init];
	UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
	[self presentViewController:navigationController animated:YES completion:nil];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
