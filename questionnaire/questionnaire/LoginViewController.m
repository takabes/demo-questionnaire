//
//  LoginViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "LoginViewController.h"
#import "CompleteViewController.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"
#import "AdminViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "FBEncryptorAES.h"
#import "FMDB.h"
#import "AppConsts.h"
#import <QuartzCore/QuartzCore.h>

@interface LoginViewController ()
@end

@implementation LoginViewController

@synthesize userIdTextField = _userIdTextField;
@synthesize passwordTextField = _passwordTextField;

@synthesize dataCountLabel = _dataCountLabel;

@synthesize oneButton = _oneButton;
@synthesize twoButton = _twoButton;
@synthesize threeButton = _threeButton;
@synthesize fourButton = _fourButton;
@synthesize fiveButton = _fiveButton;
@synthesize sixButton = _sixButton;
@synthesize sevenButton = _sevenButton;
@synthesize eightButton = _eightButton;
@synthesize nineButton = _nineButton;
@synthesize zeroButton = _zeroButton;

@synthesize qButton = _qButton;
@synthesize wButton = _wButton;
@synthesize eButton = _eButton;
@synthesize rButton = _rButton;
@synthesize tButton = _tButton;
@synthesize yButton = _yButton;
@synthesize uButton = _uButton;
@synthesize iButton = _iButton;
@synthesize oButton = _oButton;
@synthesize pButton = _pButton;
@synthesize hyphenButton = _hyphenButton;

@synthesize aButton = _aButton;
@synthesize sButton = _sButton;
@synthesize dButton = _dButton;
@synthesize fButton = _fButton;
@synthesize gButton = _gButton;
@synthesize hButton = _hButton;
@synthesize jButton = _jButton;
@synthesize kButton = _kButton;
@synthesize lButton = _lButton;

@synthesize zButton = _zButton;
@synthesize xButton = _xButton;
@synthesize cButton = _cButton;
@synthesize vButton = _vButton;
@synthesize bButton = _bButton;
@synthesize nButton = _nButton;
@synthesize mButton = _mButton;
@synthesize dotButton = _dotButton;
@synthesize underscoreButton = _underscoreButton;

@synthesize deleteButton = _deleteButton;
@synthesize nextButton = _nextButton;
@synthesize adminButton = _adminButton;
@synthesize showKeyboardButton = _showKeyboardButton;

- (id)init {
    self = [super initWithNibName:@"Login" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"ログイン";

    return self;
}

- (void)viewDidLoad {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.userId = @"";
	
	_currentTextFieldTag = 1;

	[_userIdTextField setDelegate:self];
	[_passwordTextField setDelegate:self];

	[self initButton];

	[super viewDidLoad];
}

- (void)initButton {
	
	[_oneButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_twoButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_threeButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fourButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fiveButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sixButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sevenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_eightButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_nineButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_zeroButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_qButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_wButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_eButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_rButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_tButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_yButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_uButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_iButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_oButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_pButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_hyphenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_aButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_dButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_gButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_hButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_jButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_kButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_lButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_zButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_xButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_cButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_vButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_bButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_nButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_mButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_dotButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_underscoreButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_adminButton];
	[UtilityFunction customizeButton:_showKeyboardButton];
	
	[UtilityFunction customizeKeyboardButton:_oneButton];
	[UtilityFunction customizeKeyboardButton:_twoButton];
	[UtilityFunction customizeKeyboardButton:_threeButton];
	[UtilityFunction customizeKeyboardButton:_fourButton];
	[UtilityFunction customizeKeyboardButton:_fiveButton];
	[UtilityFunction customizeKeyboardButton:_sixButton];
	[UtilityFunction customizeKeyboardButton:_sevenButton];
	[UtilityFunction customizeKeyboardButton:_eightButton];
	[UtilityFunction customizeKeyboardButton:_nineButton];
	[UtilityFunction customizeKeyboardButton:_zeroButton];
	
	[UtilityFunction customizeKeyboardButton:_deleteButton];
	
	[UtilityFunction customizeKeyboardButton:_qButton];
	[UtilityFunction customizeKeyboardButton:_wButton];
	[UtilityFunction customizeKeyboardButton:_eButton];
	[UtilityFunction customizeKeyboardButton:_rButton];
	[UtilityFunction customizeKeyboardButton:_tButton];
	[UtilityFunction customizeKeyboardButton:_yButton];
	[UtilityFunction customizeKeyboardButton:_uButton];
	[UtilityFunction customizeKeyboardButton:_iButton];
	[UtilityFunction customizeKeyboardButton:_oButton];
	[UtilityFunction customizeKeyboardButton:_pButton];
	[UtilityFunction customizeKeyboardButton:_hyphenButton];
	
	[UtilityFunction customizeKeyboardButton:_aButton];
	[UtilityFunction customizeKeyboardButton:_sButton];
	[UtilityFunction customizeKeyboardButton:_dButton];
	[UtilityFunction customizeKeyboardButton:_fButton];
	[UtilityFunction customizeKeyboardButton:_gButton];
	[UtilityFunction customizeKeyboardButton:_hButton];
	[UtilityFunction customizeKeyboardButton:_jButton];
	[UtilityFunction customizeKeyboardButton:_kButton];
	[UtilityFunction customizeKeyboardButton:_lButton];
	
	[UtilityFunction customizeKeyboardButton:_zButton];
	[UtilityFunction customizeKeyboardButton:_xButton];
	[UtilityFunction customizeKeyboardButton:_cButton];
	[UtilityFunction customizeKeyboardButton:_vButton];
	[UtilityFunction customizeKeyboardButton:_bButton];
	[UtilityFunction customizeKeyboardButton:_nButton];
	[UtilityFunction customizeKeyboardButton:_mButton];
	[UtilityFunction customizeKeyboardButton:_dotButton];
	[UtilityFunction customizeKeyboardButton:_underscoreButton];
}

- (void)viewWillAppear:(BOOL)animated {
	_userIdTextField.text = @"";
	_passwordTextField.text = @"";
	[self initDataCountLabel];
}

- (void)viewDidAppear:(BOOL)animated {
	[_userIdTextField becomeFirstResponder];
	[super viewDidAppear:animated];
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField {
	
	if (textField.tag == 1) {
		[_passwordTextField becomeFirstResponder];
		return NO;
	}
	
	return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	
	_currentTextFieldTag = textField.tag;
	
	if (_currentTextFieldTag == 1) {
		_userIdTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
		_passwordTextField.backgroundColor = [UIColor whiteColor];
	} else if (_currentTextFieldTag == 2) {
		_userIdTextField.backgroundColor = [UIColor whiteColor];
		_passwordTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
	}
	
	return YES;
}

- (void)keyboardButtonTapped:(UIButton*)button {
	
	if (_currentTextFieldTag == 1) {
		_userIdTextField.text = [NSString stringWithFormat:@"%@%@", _userIdTextField.text, [UtilityFunction getCharacter:button.tag]];
	} else {
		_passwordTextField.text = [NSString stringWithFormat:@"%@%@", _passwordTextField.text, [UtilityFunction getCharacter:button.tag]];
	}
}

- (void)deleteButtonTapped:(UIButton*)button {
	
	if (_currentTextFieldTag == 1) {
		if ([_userIdTextField.text length]) {
			NSString* text = [_userIdTextField.text substringToIndex:[_userIdTextField.text length] - 1];
			_userIdTextField.text = text;
		}
	} else {
		if ([_passwordTextField.text length]) {
			NSString* text = [_passwordTextField.text substringToIndex:[_passwordTextField.text length] - 1];
			_passwordTextField.text = text;
		}
	}
}

- (void)initDataCountLabel {
	
	NSString* dataCount;
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSString* saveAnswer = [defaults stringForKey:@"saveAnswer"];
	if (saveAnswer) {
		dataCount = [self getDataCount];
	} else {
		dataCount = @"0";
	}
		
	_dataCountLabel.text = [NSString stringWithFormat:@"登録済未送信件数  %@", dataCount];
}

- (NSString*)getDataCount {
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
	
	NSString* sql = @"SELECT COUNT(id) FROM answer WHERE delete_flag = '0';";
 
	[db open];
	
	FMResultSet* results = [db executeQuery:sql];
	NSString* count;
	while([results next]) {
		count = [results stringForColumnIndex:0];
		_Log(@"%@", count);
	}
	
	[db close];
	
	return count;
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	// ログイン
	if ([self login]) {
		
		NSMutableArray* array = [UtilityFunction getAddress:@"3892702"];
		
		if ([array count]) {
			[self nextPage];
		} else {
			[self showZipCodeAlert:@"エラー" message:@"郵便番号が同期されていません"];
		}
	}
}

- (BOOL)login {
	
	// ユーザIDチェック
	if ([_userIdTextField.text length] == 0) {
		[self showAlert:@"エラー" message:@"ユーザIDを入力してください"];
		return NO;
	}
	
	// パスワードチェック
	if ([_passwordTextField.text length] == 0) {
		[self showAlert:@"エラー" message:@"パスワードを入力してください"];
		return NO;
	}
	
	// ユーザID パスワードチェック
	if ([self certify]) {
		return YES;
	} else {
		[self showAlert:@"エラー" message:@"ユーザIDまたはパスワードが正しくありません"];
		return NO;
	}
}

// 認証
- (BOOL)certify {
	
	BOOL result = NO;
	
//	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//	NSString* saveUser = [defaults stringForKey:@"saveUser"];
//	if (!saveUser) {
//		if ([_userIdTextField.text isEqualToString:ADMIN_USER_ID] && [_passwordTextField.text isEqualToString:ADMIN_PASSWORD]) {
//			return YES;
//		}
//		return NO;
//	}
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
	
	NSString* sql = @"SELECT id, user_id, place_code, show_public_info FROM user WHERE user_id = ? AND password = ?;";
 
	[db open];
 
	FMResultSet* results = [db executeQuery:sql, _userIdTextField.text, _passwordTextField.text];
	
	while([results next]) {
		NSString* id = [results stringForColumnIndex:0];
		NSString* user_id = [results stringForColumnIndex:1];
		NSString* place_code = [results stringForColumnIndex:2];
		NSString* show_public_info = [results stringForColumnIndex:3];
		
		_Log(@"%@", id);
		_Log(@"%@", user_id);
		_Log(@"%@", place_code);
		_Log(@"%@", show_public_info);
		AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
		appDelegate.userId = [results stringForColumnIndex:1];
		appDelegate.placeCode = [results stringForColumnIndex:2];
		appDelegate.showPublicInfo = [results stringForColumnIndex:3];
		
		result = YES;
	}
		
	[db close];

	// デバッグ用
//	if (result != YES) {
//		_Log(@"Login error");
//		AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
//		appDelegate.userId = @"test";
//		appDelegate.placeCode = @"999";
//		appDelegate.showPublicInfo = @"1";
//		return YES;
//	}

	return result;
}

- (void)nextPage {
	
	if (MODE == MODE_ONLY_PERSONAL_INFO) {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		InputSerialNumberViewController *viewController = [[InputSerialNumberViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		QuestionListViewController *viewController = [[QuestionListViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (void)showZipCodeAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		AdminViewController *viewController = [[AdminViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];

	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)adminButtonTapped:(UIButton*)button {
	
	if ([self login]) {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		AdminViewController *viewController = [[AdminViewController alloc] init];
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

- (IBAction)showKeyboardButtonTapped:(UIButton*)button {
	if (_currentTextFieldTag == 1) {
		[_userIdTextField becomeFirstResponder];
	} else {
		[_passwordTextField becomeFirstResponder];
	}
}

@end
