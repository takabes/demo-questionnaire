//
//  Question04ViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "Question04ViewController.h"
#import "Question05ViewController.h"
#import "AppDelegate.h"
#import "InputNameViewController.h"
#import "QuestionMultiViewController.h"
#import "QuestionSingleViewController.h"
#import "UtilityFunction.h"
#import "QuestionListViewController.h"

@interface Question04ViewController ()
@end

@implementation Question04ViewController

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"Question04" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"アンケート入力";

    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];
	
	[self initHeaderLabel];
	
	[self initButton];

	[self initQ10Answer];
	[self initQ11Answer];
	[self initQ12Answer];
	
	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initHeaderLabel {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	if (questions[@"q19"]) {
		if (questions[@"q20"]) {
			_headerLabel.text = @"4 / 9";
		} else {
			_headerLabel.text = @"4 / 8";
		}
	} else {
		_headerLabel.text = @"4 / 7";
	}
}

- (void)initButton {

	[_q10Answer01Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q10Answer02Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q10Answer03Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q10Answer04Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q10Answer05Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q10Answer06Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q10Answer07Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q10Answer08Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q10Answer09Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q10Answer10Button addTarget:self action:@selector(q10AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[_q11Answer01Button addTarget:self action:@selector(q11AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q11Answer02Button addTarget:self action:@selector(q11AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q11Answer03Button addTarget:self action:@selector(q11AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[_q12Answer01Button addTarget:self action:@selector(q12AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q12Answer02Button addTarget:self action:@selector(q12AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q12Answer03Button addTarget:self action:@selector(q12AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q12Answer04Button addTarget:self action:@selector(q12AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q12Answer05Button addTarget:self action:@selector(q12AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q12Answer06Button addTarget:self action:@selector(q12AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
}

- (void)initQ10Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q10Answer01 isEqualToString:@"1"]) {
		[_q10Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q10Answer02 isEqualToString:@"1"]) {
		[_q10Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q10Answer03 isEqualToString:@"1"]) {
		[_q10Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q10Answer04 isEqualToString:@"1"]) {
		[_q10Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q10Answer05 isEqualToString:@"1"]) {
		[_q10Answer05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q10Answer06 isEqualToString:@"1"]) {
		[_q10Answer06Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q10Answer07 isEqualToString:@"1"]) {
		[_q10Answer07Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q10Answer08 isEqualToString:@"1"]) {
		[_q10Answer08Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q10Answer09 isEqualToString:@"1"]) {
		[_q10Answer09Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q10Answer10 isEqualToString:@"1"]) {
		[_q10Answer10Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ11Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q11Answer01 isEqualToString:@"1"]) {
		[_q11Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q11Answer02 isEqualToString:@"1"]) {
		[_q11Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q11Answer03 isEqualToString:@"1"]) {
		[_q11Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ12Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q12Answer01 isEqualToString:@"1"]) {
		[_q12Answer01Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q12Answer02 isEqualToString:@"1"]) {
		[_q12Answer02Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q12Answer03 isEqualToString:@"1"]) {
		[_q12Answer03Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q12Answer04 isEqualToString:@"1"]) {
		[_q12Answer04Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q12Answer05 isEqualToString:@"1"]) {
		[_q12Answer05Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q12Answer06 isEqualToString:@"1"]) {
		[_q12Answer06Button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
}

- (void)q10AnswerButtonTapped:(UIButton*)button {
	
	[_q10Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q10Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q10Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q10Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q10Answer05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q10Answer06Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q10Answer07Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q10Answer08Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q10Answer09Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q10Answer10Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q10Answer01 = @"0";
	appDelegate.q10Answer02 = @"0";
	appDelegate.q10Answer03 = @"0";
	appDelegate.q10Answer04 = @"0";
	appDelegate.q10Answer05 = @"0";
	appDelegate.q10Answer06 = @"0";
	appDelegate.q10Answer07 = @"0";
	appDelegate.q10Answer08 = @"0";
	appDelegate.q10Answer09 = @"0";
	appDelegate.q10Answer10 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q10Answer01 = @"1";
			break;
		case 2:
			appDelegate.q10Answer02 = @"1";
			break;
		case 3:
			appDelegate.q10Answer03 = @"1";
			break;
		case 4:
			appDelegate.q10Answer04 = @"1";
			break;
		case 5:
			appDelegate.q10Answer05 = @"1";
			break;
		case 6:
			appDelegate.q10Answer06 = @"1";
			break;
		case 7:
			appDelegate.q10Answer07 = @"1";
			break;
		case 8:
			appDelegate.q10Answer08 = @"1";
			break;
		case 9:
			appDelegate.q10Answer09 = @"1";
			break;
		case 10:
			appDelegate.q10Answer10 = @"1";
			break;
	}
}

- (void)q11AnswerButtonTapped:(UIButton*)button {
	
	[_q11Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q11Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q11Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q11Answer01 = @"0";
	appDelegate.q11Answer02 = @"0";
	appDelegate.q11Answer03 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q11Answer01 = @"1";
			break;
		case 2:
			appDelegate.q11Answer02 = @"1";
			break;
		case 3:
			appDelegate.q11Answer03 = @"1";
			break;
	}
}

- (void)q12AnswerButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	BOOL check = YES;
	
	switch (button.tag) {
		case 1:
			if ([appDelegate.q12Answer01 isEqualToString:@"0"]) {
				appDelegate.q12Answer01 = @"1";
				check = YES;
			} else {
				appDelegate.q12Answer01 = @"0";
				check = NO;
			}
			break;
		case 2:
			if ([appDelegate.q12Answer02 isEqualToString:@"0"]) {
				appDelegate.q12Answer02 = @"1";
				check = YES;
			} else {
				appDelegate.q12Answer02 = @"0";
				check = NO;
			}
			break;
		case 3:
			if ([appDelegate.q12Answer03 isEqualToString:@"0"]) {
				appDelegate.q12Answer03 = @"1";
				check = YES;
			} else {
				appDelegate.q12Answer03 = @"0";
				check = NO;
			}
			break;
		case 4:
			if ([appDelegate.q12Answer04 isEqualToString:@"0"]) {
				appDelegate.q12Answer04 = @"1";
				check = YES;
			} else {
				appDelegate.q12Answer04 = @"0";
				check = NO;
			}
			break;
		case 5:
			if ([appDelegate.q12Answer05 isEqualToString:@"0"]) {
				appDelegate.q12Answer05 = @"1";
				check = YES;
			} else {
				appDelegate.q12Answer05 = @"0";
				check = NO;
			}
			break;
		case 6:
			if ([appDelegate.q12Answer06 isEqualToString:@"0"]) {
				appDelegate.q12Answer06 = @"1";
				check = YES;
			} else {
				appDelegate.q12Answer06 = @"0";
				check = NO;
			}
			break;
	}
	
	if (check) {
		[button setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	} else {
		[button setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	NSString* message = @"";

	if ([appDelegate.q10Answer01 isEqualToString:@"0"] &&
		[appDelegate.q10Answer02 isEqualToString:@"0"] &&
		[appDelegate.q10Answer03 isEqualToString:@"0"] &&
		[appDelegate.q10Answer04 isEqualToString:@"0"] &&
		[appDelegate.q10Answer05 isEqualToString:@"0"] &&
		[appDelegate.q10Answer06 isEqualToString:@"0"] &&
		[appDelegate.q10Answer07 isEqualToString:@"0"] &&
		[appDelegate.q10Answer08 isEqualToString:@"0"] &&
		[appDelegate.q10Answer09 isEqualToString:@"0"] &&
		[appDelegate.q10Answer10 isEqualToString:@"0"]) {
		message = @"Q10の回答を選択してください";
	}

	if ([appDelegate.q11Answer01 isEqualToString:@"0"] &&
		[appDelegate.q11Answer02 isEqualToString:@"0"] &&
		[appDelegate.q11Answer03 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q11の回答を選択してください"];
		} else {
			message = @"Q11の回答を選択してください";
		}
	}

	if ([appDelegate.q12Answer01 isEqualToString:@"0"] &&
		[appDelegate.q12Answer02 isEqualToString:@"0"] &&
		[appDelegate.q12Answer03 isEqualToString:@"0"] &&
		[appDelegate.q12Answer04 isEqualToString:@"0"] &&
		[appDelegate.q12Answer05 isEqualToString:@"0"] &&
		[appDelegate.q12Answer06 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q12の回答を選択してください"];
		} else {
			message = @"Q12の回答を選択してください";
		}
	}

	if ([message length]) {
		[self showAlert:@"必須項目" message:message];
		return;
	}

	if ([appDelegate.q12Answer01 isEqualToString:@"1"] ||
		[appDelegate.q12Answer02 isEqualToString:@"1"] ||
		[appDelegate.q12Answer03 isEqualToString:@"1"]) {
		[self nextPageQuestion];
	} else {
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		id questions = [defaults objectForKey:@"questions"];
		if (questions[@"q19"]) {
			if ([questions[@"q19"][@"type"] isEqualToString:@"1"]) {
				[self nextPageSingle];
			} else {
				[self nextPageMulti];
			}
		} else {
			[self nextPageInputName];
		}
	}
}

- (void)nextPageQuestion {

	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	Question05ViewController *viewController = [[Question05ViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (void)nextPageSingle {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	QuestionSingleViewController *viewController = [[QuestionSingleViewController alloc] init];
	viewController.questionNumber = 19;
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (void)nextPageMulti {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	QuestionMultiViewController *viewController = [[QuestionMultiViewController alloc] init];
	viewController.questionNumber = 19;
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (void)nextPageInputName {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	InputNameViewController *viewController = [[InputNameViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中のアンケート内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[UtilityFunction resetPersonalInfo];
		QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
