//
//  AppDelegate.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
	NSString*		_userId;
	NSString*		_placeCode;
	NSString*		_showPublicInfo;
	NSString*		_regular;
	NSString*		_serialNumber;
	NSString*		_lastName;
	NSString*		_firstName;
	NSString*		_lastNameKana;
	NSString*		_firstNameKana;
	NSString*		_zipCode;
	NSString*		_prefecture;
	NSString*		_address1;
	NSString*		_address2;
	NSString*		_address3;
	NSString*		_address4;
	NSString*		_phone;
	NSString*		_emailAccount;
	NSString*		_emailDomain;
	NSString*		_directMail;
	NSString*		_mailMagazie;
	NSString*		_publicInfo;
	NSUInteger		_selectedPrefecture;
	NSString*		_otherPrefecture;
	
	NSString*		_q1Answer01;
	NSString*		_q1Answer02;
	NSString*		_q1Answer03;

	NSString*		_q2Answer01;
	NSString*		_q2Answer02;
	NSString*		_q2Answer03;
	NSString*		_q2Answer04;
	NSString*		_q2Answer05;
	NSString*		_q2Answer20;

	NSString*		_q3Answer01;
	NSString*		_q3Answer02;
	NSString*		_q3Answer03;
	NSString*		_q3Answer04;
	NSString*		_q3Answer05;
	NSString*		_q3Answer06;
	NSString*		_q3Answer07;
	NSString*		_q3Answer08;
	NSString*		_q3Answer09;
	NSString*		_q3Answer10;
	NSString*		_q3Answer11;
	NSString*		_q3Answer12;
	NSString*		_q3Answer13;
	NSString*		_q3Answer14;
	NSString*		_q3Answer15;
	NSString*		_q3Answer16;
	NSString*		_q3Answer17;
	NSString*		_q3Answer18;
	NSString*		_q3Answer19;
	NSString*		_q3Answer20;

	NSString*		_q4Answer01;
	NSString*		_q4Answer02;
	NSString*		_q4Answer03;
	NSString*		_q4Answer04;
	NSString*		_q4Answer05;
	NSString*		_q4Answer06;
	NSString*		_q4Answer07;
	NSString*		_q4Answer08;
	NSString*		_q4Answer09;
	NSString*		_q4Answer10;
	NSString*		_q4Answer11;
	NSString*		_q4Answer12;
	NSString*		_q4Answer20;

	NSString*		_q5Answer01;
	NSString*		_q5Answer02;
	NSString*		_q5Answer03;
	NSString*		_q5Answer04;
	NSString*		_q5Answer20;
	NSString*		_q5Answer02Text;
	
	NSString*		_q6Answer01;
	NSString*		_q6Answer02;
	NSString*		_q6Answer03;
	NSString*		_q6Answer04;

	NSString*		_q7Answer01;
	NSString*		_q7Answer02;
	NSString*		_q7Answer03;
	NSString*		_q7Answer04;
	NSString*		_q7Answer01Text;

	NSString*		_q8Answer01;
	NSString*		_q8Answer02;
	NSString*		_q8Answer03;
	NSString*		_q8Answer04;
	NSString*		_q8Answer05;
	NSString*		_q8Answer06;
	NSString*		_q8Answer20;

	NSString*		_q9Answer01;
	NSString*		_q9Answer02;
	NSString*		_q9Answer03;
	NSString*		_q9Answer04;
	NSString*		_q9Answer05;
	NSString*		_q9Answer06;
	NSString*		_q9Answer07;
	
	NSString*		_q10Answer01;
	NSString*		_q10Answer02;
	NSString*		_q10Answer03;
	NSString*		_q10Answer04;
	NSString*		_q10Answer05;
	NSString*		_q10Answer06;
	NSString*		_q10Answer07;
	NSString*		_q10Answer08;
	NSString*		_q10Answer09;
	NSString*		_q10Answer10;

	NSString*		_q11Answer01;
	NSString*		_q11Answer02;
	NSString*		_q11Answer03;

	NSString*		_q12Answer01;
	NSString*		_q12Answer02;
	NSString*		_q12Answer03;
	NSString*		_q12Answer04;
	NSString*		_q12Answer05;
	NSString*		_q12Answer06;
	
	NSString*		_q13Answer01;
	NSString*		_q13Answer02;
	NSString*		_q13Answer03;
	NSString*		_q13Answer04;
	NSString*		_q13Answer05;
	NSString*		_q13Answer06;
	NSString*		_q13Answer07;
	NSString*		_q13Answer08;
	NSString*		_q13Answer09;
	NSString*		_q13Answer10;
	NSString*		_q13Answer11;
	NSString*		_q13Answer12;
	NSString*		_q13Answer13;
	NSString*		_q13Answer14;
	NSString*		_q13Answer15;
	NSString*		_q13Answer16;
	NSString*		_q13Answer17;
	NSString*		_q13Answer18;
	NSString*		_q13Answer19;
	NSString*		_q13Answer20;

	NSString*		_q14Answer01;
	NSString*		_q14Answer02;
	NSString*		_q14Answer03;
	NSString*		_q14Answer04;
	NSString*		_q14Answer05;
	
	NSString*		_q15Answer01;
	NSString*		_q15Answer02;
	NSString*		_q15Answer03;
	NSString*		_q15Answer04;
	NSString*		_q15Answer05;
	
	NSString*		_q16Answer01;
	NSString*		_q16Answer02;
	NSString*		_q16Answer03;
	NSString*		_q16Answer04;
	NSString*		_q16Answer05;
	NSString*		_q16Answer20;

	NSString*		_q17Answer01;
	NSString*		_q17Answer02;
	NSString*		_q17Answer03;
	NSString*		_q17Answer20;
	
	NSString*		_q18Answer01;
	NSString*		_q18Answer02;
	NSString*		_q18Answer03;
	NSString*		_q18Answer04;
	NSString*		_q18Answer05;
	NSString*		_q18Answer06;
	NSString*		_q18Answer07;
	NSString*		_q18Answer08;
	NSString*		_q18Answer09;
	
	NSString*		_q19Answer01;
	NSString*		_q19Answer02;
	NSString*		_q19Answer03;
	NSString*		_q19Answer04;
	NSString*		_q19Answer05;
	NSString*		_q19Answer06;
	NSString*		_q19Answer07;
	NSString*		_q19Answer08;
	NSString*		_q19Answer09;
	NSString*		_q19Answer10;
	NSString*		_q19Answer11;
	NSString*		_q19Answer12;
	NSString*		_q19Answer13;
	NSString*		_q19Answer14;
	NSString*		_q19Answer15;
	NSString*		_q19Answer16;
	NSString*		_q19Answer17;
	NSString*		_q19Answer18;
	NSString*		_q19Answer19;
	NSString*		_q19Answer20;

	NSString*		_q20Answer01;
	NSString*		_q20Answer02;
	NSString*		_q20Answer03;
	NSString*		_q20Answer04;
	NSString*		_q20Answer05;
	NSString*		_q20Answer06;
	NSString*		_q20Answer07;
	NSString*		_q20Answer08;
	NSString*		_q20Answer09;
	NSString*		_q20Answer10;
	NSString*		_q20Answer11;
	NSString*		_q20Answer12;
	NSString*		_q20Answer13;
	NSString*		_q20Answer14;
	NSString*		_q20Answer15;
	NSString*		_q20Answer16;
	NSString*		_q20Answer17;
	NSString*		_q20Answer18;
	NSString*		_q20Answer19;
	NSString*		_q20Answer20;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) NSString* userId;
@property (nonatomic) NSString* placeCode;
@property (nonatomic) NSString* showPublicInfo;
@property (nonatomic) NSString* regular;
@property (nonatomic) NSString* serialNumber;
@property (nonatomic) NSString* lastName;
@property (nonatomic) NSString* firstName;
@property (nonatomic) NSString* lastNameKana;
@property (nonatomic) NSString* firstNameKana;
@property (nonatomic) NSString* zipCode;
@property (nonatomic) NSString* prefecture;
@property (nonatomic) NSString* address1;
@property (nonatomic) NSString* address2;
@property (nonatomic) NSString* address3;
@property (nonatomic) NSString* address4;
@property (nonatomic) NSString* phone;
@property (nonatomic) NSString* emailAccount;
@property (nonatomic) NSString* emailDomain;
@property (nonatomic) NSString* directMail;
@property (nonatomic) NSString* mailMagazie;
@property (nonatomic) NSString* publicInfo;
@property (nonatomic) NSUInteger selectedPrefecture;
@property (nonatomic) NSString* otherPrefecture;

@property (nonatomic) NSString* q1Answer01;
@property (nonatomic) NSString* q1Answer02;
@property (nonatomic) NSString* q1Answer03;

@property (nonatomic) NSString* q2Answer01;
@property (nonatomic) NSString* q2Answer02;
@property (nonatomic) NSString* q2Answer03;
@property (nonatomic) NSString* q2Answer04;
@property (nonatomic) NSString* q2Answer05;
@property (nonatomic) NSString* q2Answer20;

@property (nonatomic) NSString* q3Answer01;
@property (nonatomic) NSString* q3Answer02;
@property (nonatomic) NSString* q3Answer03;
@property (nonatomic) NSString* q3Answer04;
@property (nonatomic) NSString* q3Answer05;
@property (nonatomic) NSString* q3Answer06;
@property (nonatomic) NSString* q3Answer07;
@property (nonatomic) NSString* q3Answer08;
@property (nonatomic) NSString* q3Answer09;
@property (nonatomic) NSString* q3Answer10;
@property (nonatomic) NSString* q3Answer11;
@property (nonatomic) NSString* q3Answer12;
@property (nonatomic) NSString* q3Answer13;
@property (nonatomic) NSString* q3Answer14;
@property (nonatomic) NSString* q3Answer15;
@property (nonatomic) NSString* q3Answer16;
@property (nonatomic) NSString* q3Answer17;
@property (nonatomic) NSString* q3Answer18;
@property (nonatomic) NSString* q3Answer19;
@property (nonatomic) NSString* q3Answer20;

@property (nonatomic) NSString* q4Answer01;
@property (nonatomic) NSString* q4Answer02;
@property (nonatomic) NSString* q4Answer03;
@property (nonatomic) NSString* q4Answer04;
@property (nonatomic) NSString* q4Answer05;
@property (nonatomic) NSString* q4Answer06;
@property (nonatomic) NSString* q4Answer07;
@property (nonatomic) NSString* q4Answer08;
@property (nonatomic) NSString* q4Answer09;
@property (nonatomic) NSString* q4Answer10;
@property (nonatomic) NSString* q4Answer11;
@property (nonatomic) NSString* q4Answer12;
@property (nonatomic) NSString* q4Answer20;

@property (nonatomic) NSString* q5Answer01;
@property (nonatomic) NSString* q5Answer02;
@property (nonatomic) NSString* q5Answer03;
@property (nonatomic) NSString* q5Answer04;
@property (nonatomic) NSString* q5Answer20;
@property (nonatomic) NSString* q5Answer02Text;

@property (nonatomic) NSString* q6Answer01;
@property (nonatomic) NSString* q6Answer02;
@property (nonatomic) NSString* q6Answer03;
@property (nonatomic) NSString* q6Answer04;

@property (nonatomic) NSString* q7Answer01;
@property (nonatomic) NSString* q7Answer02;
@property (nonatomic) NSString* q7Answer03;
@property (nonatomic) NSString* q7Answer04;
@property (nonatomic) NSString* q7Answer01Text;

@property (nonatomic) NSString* q8Answer01;
@property (nonatomic) NSString* q8Answer02;
@property (nonatomic) NSString* q8Answer03;
@property (nonatomic) NSString* q8Answer04;
@property (nonatomic) NSString* q8Answer05;
@property (nonatomic) NSString* q8Answer06;
@property (nonatomic) NSString* q8Answer20;

@property (nonatomic) NSString* q9Answer01;
@property (nonatomic) NSString* q9Answer02;
@property (nonatomic) NSString* q9Answer03;
@property (nonatomic) NSString* q9Answer04;
@property (nonatomic) NSString* q9Answer05;
@property (nonatomic) NSString* q9Answer06;
@property (nonatomic) NSString* q9Answer07;

@property (nonatomic) NSString* q10Answer01;
@property (nonatomic) NSString* q10Answer02;
@property (nonatomic) NSString* q10Answer03;
@property (nonatomic) NSString* q10Answer04;
@property (nonatomic) NSString* q10Answer05;
@property (nonatomic) NSString* q10Answer06;
@property (nonatomic) NSString* q10Answer07;
@property (nonatomic) NSString* q10Answer08;
@property (nonatomic) NSString* q10Answer09;
@property (nonatomic) NSString* q10Answer10;

@property (nonatomic) NSString* q11Answer01;
@property (nonatomic) NSString* q11Answer02;
@property (nonatomic) NSString* q11Answer03;

@property (nonatomic) NSString* q12Answer01;
@property (nonatomic) NSString* q12Answer02;
@property (nonatomic) NSString* q12Answer03;
@property (nonatomic) NSString* q12Answer04;
@property (nonatomic) NSString* q12Answer05;
@property (nonatomic) NSString* q12Answer06;

@property (nonatomic) NSString* q13Answer01;
@property (nonatomic) NSString* q13Answer02;
@property (nonatomic) NSString* q13Answer03;
@property (nonatomic) NSString* q13Answer04;
@property (nonatomic) NSString* q13Answer05;
@property (nonatomic) NSString* q13Answer06;
@property (nonatomic) NSString* q13Answer07;
@property (nonatomic) NSString* q13Answer08;
@property (nonatomic) NSString* q13Answer09;
@property (nonatomic) NSString* q13Answer10;
@property (nonatomic) NSString* q13Answer11;
@property (nonatomic) NSString* q13Answer12;
@property (nonatomic) NSString* q13Answer13;
@property (nonatomic) NSString* q13Answer14;
@property (nonatomic) NSString* q13Answer15;
@property (nonatomic) NSString* q13Answer16;
@property (nonatomic) NSString* q13Answer17;
@property (nonatomic) NSString* q13Answer18;
@property (nonatomic) NSString* q13Answer19;
@property (nonatomic) NSString* q13Answer20;

@property (nonatomic) NSString* q14Answer01;
@property (nonatomic) NSString* q14Answer02;
@property (nonatomic) NSString* q14Answer03;
@property (nonatomic) NSString* q14Answer04;
@property (nonatomic) NSString* q14Answer05;

@property (nonatomic) NSString* q15Answer01;
@property (nonatomic) NSString* q15Answer02;
@property (nonatomic) NSString* q15Answer03;
@property (nonatomic) NSString* q15Answer04;
@property (nonatomic) NSString* q15Answer05;

@property (nonatomic) NSString* q16Answer01;
@property (nonatomic) NSString* q16Answer02;
@property (nonatomic) NSString* q16Answer03;
@property (nonatomic) NSString* q16Answer04;
@property (nonatomic) NSString* q16Answer05;
@property (nonatomic) NSString* q16Answer20;

@property (nonatomic) NSString* q17Answer01;
@property (nonatomic) NSString* q17Answer02;
@property (nonatomic) NSString* q17Answer03;
@property (nonatomic) NSString* q17Answer20;

@property (nonatomic) NSString* q18Answer01;
@property (nonatomic) NSString* q18Answer02;
@property (nonatomic) NSString* q18Answer03;
@property (nonatomic) NSString* q18Answer04;
@property (nonatomic) NSString* q18Answer05;
@property (nonatomic) NSString* q18Answer06;
@property (nonatomic) NSString* q18Answer07;
@property (nonatomic) NSString* q18Answer08;
@property (nonatomic) NSString* q18Answer09;

@property (nonatomic) NSString* q19Answer01;
@property (nonatomic) NSString* q19Answer02;
@property (nonatomic) NSString* q19Answer03;
@property (nonatomic) NSString* q19Answer04;
@property (nonatomic) NSString* q19Answer05;
@property (nonatomic) NSString* q19Answer06;
@property (nonatomic) NSString* q19Answer07;
@property (nonatomic) NSString* q19Answer08;
@property (nonatomic) NSString* q19Answer09;
@property (nonatomic) NSString* q19Answer10;
@property (nonatomic) NSString* q19Answer11;
@property (nonatomic) NSString* q19Answer12;
@property (nonatomic) NSString* q19Answer13;
@property (nonatomic) NSString* q19Answer14;
@property (nonatomic) NSString* q19Answer15;
@property (nonatomic) NSString* q19Answer16;
@property (nonatomic) NSString* q19Answer17;
@property (nonatomic) NSString* q19Answer18;
@property (nonatomic) NSString* q19Answer19;
@property (nonatomic) NSString* q19Answer20;

@property (nonatomic) NSString* q20Answer01;
@property (nonatomic) NSString* q20Answer02;
@property (nonatomic) NSString* q20Answer03;
@property (nonatomic) NSString* q20Answer04;
@property (nonatomic) NSString* q20Answer05;
@property (nonatomic) NSString* q20Answer06;
@property (nonatomic) NSString* q20Answer07;
@property (nonatomic) NSString* q20Answer08;
@property (nonatomic) NSString* q20Answer09;
@property (nonatomic) NSString* q20Answer10;
@property (nonatomic) NSString* q20Answer11;
@property (nonatomic) NSString* q20Answer12;
@property (nonatomic) NSString* q20Answer13;
@property (nonatomic) NSString* q20Answer14;
@property (nonatomic) NSString* q20Answer15;
@property (nonatomic) NSString* q20Answer16;
@property (nonatomic) NSString* q20Answer17;
@property (nonatomic) NSString* q20Answer18;
@property (nonatomic) NSString* q20Answer19;
@property (nonatomic) NSString* q20Answer20;

@end

