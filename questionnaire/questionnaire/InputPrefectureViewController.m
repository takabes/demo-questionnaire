//
//  InputPrefectureViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "InputPrefectureViewController.h"
#import "InputZipCodeViewController.h"
#import "InputAddressViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "AppConsts.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"

@interface InputPrefectureViewController ()
@end

@implementation InputPrefectureViewController

@synthesize prefectureTextField = _prefectureTextField;
@synthesize prefecture01Button = _prefecture01Button;
@synthesize prefecture02Button = _prefecture02Button;
@synthesize prefecture03Button = _prefecture03Button;
@synthesize prefecture04Button = _prefecture04Button;
@synthesize prefecture05Button = _prefecture05Button;
@synthesize prefecture06Button = _prefecture06Button;
@synthesize prefecture07Button = _prefecture07Button;
@synthesize prefecture08Button = _prefecture08Button;
@synthesize prefecture09Button = _prefecture09Button;
@synthesize prefecture10Button = _prefecture10Button;
@synthesize guideImageView = _guideImageView;
@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;
@synthesize edit = _edit;

- (id)init {
    self = [super initWithNibName:@"InputPrefecture" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"都道府県入力";

    return self;
}

- (void)viewDidLoad {
	
	_guideMode = 1;
	_selectedPrefecture = 0;

	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];

	[_prefectureTextField setDelegate:self];

	[self initPrefecture];

	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initPrefecture {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	_selectedPrefecture = appDelegate.selectedPrefecture;

	switch (_selectedPrefecture) {
		case 1:
			[_prefecture01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			break;
			
		case 2:
			[_prefecture02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			break;
			
		case 3:
			[_prefecture03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			break;
			
		case 4:
			[_prefecture04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			break;
			
		case 5:
			[_prefecture05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			break;
			
		case 6:
			[_prefecture06Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			break;
			
		case 7:
			[_prefecture07Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			break;
			
		case 8:
			[_prefecture08Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			break;
			
		case 9:
			[_prefecture09Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			break;
			
		case 10:
			[_prefecture10Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
			_prefectureTextField.text = appDelegate.otherPrefecture;
			break;
			
		default:
			break;
	}
}

- (void)viewDidAppear:(BOOL)animated {
//	[_prefectureTextField becomeFirstResponder];
	[super viewDidAppear:animated];
}

- (IBAction)prefectureButtonTapped:(UIButton*)button {

	[_prefecture01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_prefecture02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_prefecture03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_prefecture04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_prefecture05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_prefecture06Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_prefecture07Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_prefecture08Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_prefecture09Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_prefecture10Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];

	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	if (button.tag == 10) {
		[_prefectureTextField becomeFirstResponder];
		_prefectureTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];
	} else {
		[_prefectureTextField resignFirstResponder];
		_prefectureTextField.backgroundColor = [UIColor whiteColor];
	}
	
	_selectedPrefecture = button.tag;
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	if (_selectedPrefecture == 0) {
		[self showAlert:@"必須項目" message:@"都道府県を選択してください"];
		return;
	}

	if (_selectedPrefecture == 10) {
		if ([_prefectureTextField.text length] == 0) {
			[self showAlert:@"必須項目" message:@"都道府県を入力してください"];
			return;
		}
		
		if ([UtilityFunction isZenkaku:_prefectureTextField.text] != YES) {
			[self showAlert:@"入力項目" message:@"都道府県は全角で入力してください"];
			return;
		}
		
		if ([_prefectureTextField.text length] > 4) {
			[self showAlert:@"入力項目" message:@"都道府県は4文字以内で入力してください"];
			return;
		}
	}
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.selectedPrefecture = _selectedPrefecture;
	
	NSString* prefecture;
	
	switch (_selectedPrefecture) {
		case 1:
			prefecture = @"茨城県";
			break;

		case 2:
			prefecture = @"栃木県";
			break;
			
		case 3:
			prefecture = @"群馬県";
			break;

		case 4:
			prefecture = @"千葉県";
			break;

		case 5:
			prefecture = @"埼玉県";
			break;

		case 6:
			prefecture = @"東京都";
			break;

		case 7:
			prefecture = @"神奈川県";
			break;

		case 8:
			prefecture = @"山梨県";
			break;

		case 9:
			prefecture = @"長野県";
			break;

		case 10:
			prefecture = _prefectureTextField.text;
			appDelegate.otherPrefecture = _prefectureTextField.text;
			break;

		default:
			break;
	}
	
	if (_edit) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		CATransition* transition = [CATransition animation];
		transition.duration = 0.2;
		transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		transition.type = kCATransitionPush;
		transition.subtype = kCATransitionFromRight;
		
		InputAddressViewController* viewController = [[InputAddressViewController alloc] init];
		viewController.prefecture = prefecture;
		viewController.cursor = 2;
		
		[self.navigationController.view.layer addAnimation:transition forKey:nil];
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

- (IBAction)backButtonTapped:(UIButton*)button {
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.prefecture = @"";
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)switchGuideButtonTapped:(UIButton*)button {
	if (_guideMode == 1) {
		_guideMode = 2;
		[button setTitle:@"未入力状態の説明に切り替える" forState:UIControlStateNormal];
		[_guideImageView setImage:[UIImage imageNamed:@"guide1.jpg"]];
	} else {
		_guideMode = 1;
		[button setTitle:@"入力中状態の説明に切り替える" forState:UIControlStateNormal];
		[_guideImageView setImage:[UIImage imageNamed:@"guide2.jpg"]];
	}
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中の内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		[UtilityFunction resetPersonalInfo];
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}
		
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
