//
//  Question04ViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Question04ViewController : UIViewController {
	UILabel*		_headerLabel;

	UIButton*		_q10Answer01Button;
	UIButton*		_q10Answer02Button;
	UIButton*		_q10Answer03Button;
	UIButton*		_q10Answer04Button;
	UIButton*		_q10Answer05Button;
	UIButton*		_q10Answer06Button;
	UIButton*		_q10Answer07Button;
	UIButton*		_q10Answer08Button;
	UIButton*		_q10Answer09Button;
	UIButton*		_q10Answer10Button;

	UIButton*		_q11Answer01Button;
	UIButton*		_q11Answer02Button;
	UIButton*		_q11Answer03Button;

	UIButton*		_q12Answer01Button;
	UIButton*		_q12Answer02Button;
	UIButton*		_q12Answer03Button;
	UIButton*		_q12Answer04Button;
	UIButton*		_q12Answer05Button;
	UIButton*		_q12Answer06Button;

	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UILabel* headerLabel;

@property (nonatomic) IBOutlet UIButton* q10Answer01Button;
@property (nonatomic) IBOutlet UIButton* q10Answer02Button;
@property (nonatomic) IBOutlet UIButton* q10Answer03Button;
@property (nonatomic) IBOutlet UIButton* q10Answer04Button;
@property (nonatomic) IBOutlet UIButton* q10Answer05Button;
@property (nonatomic) IBOutlet UIButton* q10Answer06Button;
@property (nonatomic) IBOutlet UIButton* q10Answer07Button;
@property (nonatomic) IBOutlet UIButton* q10Answer08Button;
@property (nonatomic) IBOutlet UIButton* q10Answer09Button;
@property (nonatomic) IBOutlet UIButton* q10Answer10Button;

@property (nonatomic) IBOutlet UIButton* q11Answer01Button;
@property (nonatomic) IBOutlet UIButton* q11Answer02Button;
@property (nonatomic) IBOutlet UIButton* q11Answer03Button;

@property (nonatomic) IBOutlet UIButton* q12Answer01Button;
@property (nonatomic) IBOutlet UIButton* q12Answer02Button;
@property (nonatomic) IBOutlet UIButton* q12Answer03Button;
@property (nonatomic) IBOutlet UIButton* q12Answer04Button;
@property (nonatomic) IBOutlet UIButton* q12Answer05Button;
@property (nonatomic) IBOutlet UIButton* q12Answer06Button;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
