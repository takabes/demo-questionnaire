//
//  PrivacyPolicyViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "PrivacyPolicyViewController.h"
#import "InputNameViewController.h"
#import "UtilityFunction.h"

@interface PrivacyPolicyViewController ()
@end

@implementation PrivacyPolicyViewController

@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"PrivacyPolicy" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"個人情報のお取り扱いについて";

    return self;
}

- (void)viewDidLoad {
	
	[self initScrollView];
	
	[UtilityFunction customizeButton:_backButton];

	[super viewDidLoad];
}

- (void)initScrollView {
	
	float height = 20.0f;
	
	_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(70.0f, 100.0f, 618.0f, 800.0f)];
	_scrollView.delegate = self;
	_scrollView.showsVerticalScrollIndicator = YES;
	_scrollView.showsHorizontalScrollIndicator = NO;
	//_scrollView.pagingEnabled = YES;
	_scrollView.clipsToBounds = YES; // 範囲外を非表示にする
	_scrollView.backgroundColor = [UIColor clearColor];
	_scrollView.indicatorStyle = UIScrollViewIndicatorStyleBlack;
	_scrollView.contentSize = CGSizeMake(618.0f, 800.0f + height);
	_scrollView.contentOffset = CGPointMake(0, 0); // 初期位置
	_scrollView.backgroundColor = [UIColor groupTableViewBackgroundColor];
	[self.view addSubview:_scrollView];
	
	UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 20.0f, 578.0f, 760.0f + height)];
	label.backgroundColor = [UIColor clearColor];
	label.numberOfLines = 0;
	
	// テキストから読み込む
	NSError *error = nil;
	NSString *filePath = [[NSBundle mainBundle] pathForResource:@"privacyPolicy" ofType:@"txt"];
	label.text = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
	
	label.font = [UIFont boldSystemFontOfSize:18];
	label.textColor = [UIColor blackColor];
	label.textAlignment = NSTextAlignmentLeft;
	[_scrollView addSubview:label];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

@end
