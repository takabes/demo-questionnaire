//
//  InputSerialNumberViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputSerialNumberViewController : UIViewController <UITextFieldDelegate>  {
	UITextField*	_serialNumberTextField;
	
	UIButton*		_oneButton;
	UIButton*		_twoButton;
	UIButton*		_threeButton;
	UIButton*		_fourButton;
	UIButton*		_fiveButton;
	UIButton*		_sixButton;
	UIButton*		_sevenButton;
	UIButton*		_eightButton;
	UIButton*		_nineButton;
	UIButton*		_zeroButton;
	UIButton*		_hyphenButton;
	UIButton*		_deleteButton;

	UIButton*		_checkBoxButton;

	UIButton*		_nextButton;
	UIButton*		_logoutButton;
	UIButton*		_showKeyboardButton;

	BOOL			_checkStatus;
}

@property (nonatomic) IBOutlet UITextField* serialNumberTextField;

@property (nonatomic) IBOutlet UIButton* oneButton;
@property (nonatomic) IBOutlet UIButton* twoButton;
@property (nonatomic) IBOutlet UIButton* threeButton;
@property (nonatomic) IBOutlet UIButton* fourButton;
@property (nonatomic) IBOutlet UIButton* fiveButton;
@property (nonatomic) IBOutlet UIButton* sixButton;
@property (nonatomic) IBOutlet UIButton* sevenButton;
@property (nonatomic) IBOutlet UIButton* eightButton;
@property (nonatomic) IBOutlet UIButton* nineButton;
@property (nonatomic) IBOutlet UIButton* zeroButton;
@property (nonatomic) IBOutlet UIButton* hyphenButton;
@property (nonatomic) IBOutlet UIButton* deleteButton;

@property (nonatomic) IBOutlet UIButton* checkBoxButton;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* logoutButton;
@property (nonatomic) IBOutlet UIButton* showKeyboardButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)checkButtonTapped:(UIButton*)button;
- (IBAction)logoutButtonTapped:(UIButton*)button;
- (IBAction)showKeyboardButtonTapped:(UIButton*)button;

@end
