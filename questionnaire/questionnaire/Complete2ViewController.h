//
//  Complete2ViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Complete2ViewController : UIViewController {
	UILabel*		_serialCodeLabel;
	UIButton*		_checkBoxButton;
	BOOL			_checkStatus;
	UIButton*		_registerButton;
	UIButton*		_closeButton;
	BOOL			_register;
}

@property (nonatomic) IBOutlet UILabel* serialCodeLabel;
@property (nonatomic) IBOutlet UIButton* checkBoxButton;
@property (nonatomic) IBOutlet UIButton* registerButton;
@property (nonatomic) IBOutlet UIButton* closeButton;

- (IBAction)registerButtonTapped:(UIButton*)button;
- (IBAction)checkButtonTapped:(UIButton*)button;
- (IBAction)closeButtonTapped:(UIButton*)button;

@end
