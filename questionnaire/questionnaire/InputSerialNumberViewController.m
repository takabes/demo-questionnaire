//
//  InputSerialNumberViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "InputSerialNumberViewController.h"
#import "CompleteViewController.h"
#import "SampleViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "AppConsts.h"
#import <QuartzCore/QuartzCore.h>

@interface InputSerialNumberViewController ()
@end

@implementation InputSerialNumberViewController

@synthesize serialNumberTextField = _serialNumberTextField;

@synthesize oneButton = _oneButton;
@synthesize twoButton = _twoButton;
@synthesize threeButton = _threeButton;
@synthesize fourButton = _fourButton;
@synthesize fiveButton = _fiveButton;
@synthesize sixButton = _sixButton;
@synthesize sevenButton = _sevenButton;
@synthesize eightButton = _eightButton;
@synthesize nineButton = _nineButton;
@synthesize zeroButton = _zeroButton;
@synthesize hyphenButton = _hyphenButton;

@synthesize deleteButton = _deleteButton;
@synthesize checkBoxButton = _checkBoxButton;
@synthesize nextButton = _nextButton;
@synthesize showKeyboardButton = _showKeyboardButton;

- (id)init {
    self = [super initWithNibName:@"InputSerialNumber" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"アンケート番号入力";

    return self;
}

- (void)viewDidLoad {
	
	[self initSwipe];

	// 戻るボタン非表示
	[self.navigationItem setHidesBackButton:YES];
	
	_checkStatus = NO;

	[_serialNumberTextField setDelegate:self];

	_serialNumberTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];

	[self initButton];
	
	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initButton {
	
	[_oneButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_twoButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_threeButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fourButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fiveButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sixButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sevenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_eightButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_nineButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_zeroButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_hyphenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[_deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_logoutButton];
	[UtilityFunction customizeButton:_showKeyboardButton];
	
	[UtilityFunction customizeKeyboardButton:_oneButton];
	[UtilityFunction customizeKeyboardButton:_twoButton];
	[UtilityFunction customizeKeyboardButton:_threeButton];
	[UtilityFunction customizeKeyboardButton:_fourButton];
	[UtilityFunction customizeKeyboardButton:_fiveButton];
	[UtilityFunction customizeKeyboardButton:_sixButton];
	[UtilityFunction customizeKeyboardButton:_sevenButton];
	[UtilityFunction customizeKeyboardButton:_eightButton];
	[UtilityFunction customizeKeyboardButton:_nineButton];
	[UtilityFunction customizeKeyboardButton:_zeroButton];
	[UtilityFunction customizeKeyboardButton:_hyphenButton];
	[UtilityFunction customizeKeyboardButton:_deleteButton];
}

- (void)viewWillAppear:(BOOL)animated {
	_serialNumberTextField.text = @"";
	_checkStatus = NO;
	[_checkBoxButton setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated {
	[_serialNumberTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];

	[super viewDidAppear:animated];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	
	_Log(@"textFieldShouldBeginEditing");
	
	return YES;
}

- (void)keyboardButtonTapped:(UIButton*)button {
	_serialNumberTextField.text = [NSString stringWithFormat:@"%@%@", _serialNumberTextField.text, [UtilityFunction getCharacter:button.tag]];
}

- (void)deleteButtonTapped:(UIButton*)button {
	_Log(@"deleteButtonTapped");
	
	if ([_serialNumberTextField.text length]) {
		NSString* text = [_serialNumberTextField.text substringToIndex:[_serialNumberTextField.text length] - 1];
		_serialNumberTextField.text = text;
	}
}

- (IBAction)checkButtonTapped:(UIButton*)button {
	if(_checkStatus){
		[_checkBoxButton setImage:[UIImage imageNamed:@"checkBoxOff.png"] forState: UIControlStateNormal];
	}
	else {
		[_checkBoxButton setImage:[UIImage imageNamed:@"checkBoxOn.png"] forState: UIControlStateNormal];
	}
	_checkStatus = !_checkStatus;
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	_Log(@"nextButtonTapped");
	
	if ([_serialNumberTextField.text length] == 0) {
		[self showAlert:@"エラー" message:@"アンケート番号を入力してください"];
		return;
	}
	
	if ([UtilityFunction isSerialNumber:_serialNumberTextField.text] != YES) {
		[self showAlert:@"エラー" message:@"アンケート番号の形式が正しくありません"];
		return;
	}
	
	[UtilityFunction resetPersonalInfo];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.serialNumber = _serialNumberTextField.text;
	
	if (_checkStatus) {
		appDelegate.regular = @"1";
		_Log(@"Regular");
	} else {
		appDelegate.regular = @"0";
		_Log(@"Not Regular");
	}
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	SampleViewController *viewController = [[SampleViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];

//	SampleViewController *viewController = [[SampleViewController alloc] init];
//	[self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)logoutButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.userId = @"";
	appDelegate.placeCode = @"";
	appDelegate.publicInfo = @"";
	
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)showKeyboardButtonTapped:(UIButton*)button {
	[_serialNumberTextField becomeFirstResponder];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	//[self.navigationController popViewControllerAnimated:YES];
}

@end
