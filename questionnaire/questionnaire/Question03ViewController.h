//
//  Question03ViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Question03ViewController : UIViewController <UITextFieldDelegate> {
	UILabel*		_headerLabel;

	UIButton*		_q6Answer01Button;
	UIButton*		_q6Answer02Button;
	UIButton*		_q6Answer03Button;
	UIButton*		_q6Answer04Button;

	UIButton*		_q7Answer01Button;
	UIButton*		_q7Answer02Button;
	UIButton*		_q7Answer03Button;
	UIButton*		_q7Answer04Button;

	UITextField*	_q7Answer01TextField;

	UIButton*		_q8Answer01Button;
	UIButton*		_q8Answer02Button;
	UIButton*		_q8Answer03Button;
	UIButton*		_q8Answer04Button;
	UIButton*		_q8Answer05Button;
	UIButton*		_q8Answer06Button;
	UIButton*		_q8Answer20Button;

	UIButton*		_q9Answer01Button;
	UIButton*		_q9Answer02Button;
	UIButton*		_q9Answer03Button;
	UIButton*		_q9Answer04Button;
	UIButton*		_q9Answer05Button;
	UIButton*		_q9Answer06Button;
	UIButton*		_q9Answer07Button;
	
	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UILabel* headerLabel;

@property (nonatomic) IBOutlet UIButton* q6Answer01Button;
@property (nonatomic) IBOutlet UIButton* q6Answer02Button;
@property (nonatomic) IBOutlet UIButton* q6Answer03Button;
@property (nonatomic) IBOutlet UIButton* q6Answer04Button;

@property (nonatomic) IBOutlet UIButton* q7Answer01Button;
@property (nonatomic) IBOutlet UIButton* q7Answer02Button;
@property (nonatomic) IBOutlet UIButton* q7Answer03Button;
@property (nonatomic) IBOutlet UIButton* q7Answer04Button;

@property (nonatomic) IBOutlet UITextField* q7Answer01TextField;

@property (nonatomic) IBOutlet UIButton* q8Answer01Button;
@property (nonatomic) IBOutlet UIButton* q8Answer02Button;
@property (nonatomic) IBOutlet UIButton* q8Answer03Button;
@property (nonatomic) IBOutlet UIButton* q8Answer04Button;
@property (nonatomic) IBOutlet UIButton* q8Answer05Button;
@property (nonatomic) IBOutlet UIButton* q8Answer06Button;
@property (nonatomic) IBOutlet UIButton* q8Answer20Button;

@property (nonatomic) IBOutlet UIButton* q9Answer01Button;
@property (nonatomic) IBOutlet UIButton* q9Answer02Button;
@property (nonatomic) IBOutlet UIButton* q9Answer03Button;
@property (nonatomic) IBOutlet UIButton* q9Answer04Button;
@property (nonatomic) IBOutlet UIButton* q9Answer05Button;
@property (nonatomic) IBOutlet UIButton* q9Answer06Button;
@property (nonatomic) IBOutlet UIButton* q9Answer07Button;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
