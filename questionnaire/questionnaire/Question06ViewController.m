//
//  Question06ViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "Question06ViewController.h"
#import "Question07ViewController.h"
#import "AppDelegate.h"
#import "UtilityFunction.h"
#import "QuestionListViewController.h"

@interface Question06ViewController ()
@end

@implementation Question06ViewController

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"Question06" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"アンケート入力";

    return self;
}

- (void)viewDidLoad {
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];
	
	[self initHeaderLabel];
	
	[self initButton];

	[self initQ14Answer];
	[self initQ15Answer];
	[self initQ16Answer];
	
	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initHeaderLabel {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	if (questions[@"q19"]) {
		if (questions[@"q20"]) {
			_headerLabel.text = @"6 / 9";
		} else {
			_headerLabel.text = @"6 / 8";
		}
	} else {
		_headerLabel.text = @"6 / 7";
	}
}

- (void)initButton {
	
	[_q14Answer01Button addTarget:self action:@selector(q14AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q14Answer02Button addTarget:self action:@selector(q14AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q14Answer03Button addTarget:self action:@selector(q14AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q14Answer04Button addTarget:self action:@selector(q14AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q14Answer05Button addTarget:self action:@selector(q14AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_q15Answer01Button addTarget:self action:@selector(q15AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q15Answer02Button addTarget:self action:@selector(q15AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q15Answer03Button addTarget:self action:@selector(q15AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q15Answer04Button addTarget:self action:@selector(q15AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q15Answer05Button addTarget:self action:@selector(q15AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_q16Answer01Button addTarget:self action:@selector(q16AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q16Answer02Button addTarget:self action:@selector(q16AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q16Answer03Button addTarget:self action:@selector(q16AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q16Answer04Button addTarget:self action:@selector(q16AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q16Answer05Button addTarget:self action:@selector(q16AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q16Answer20Button addTarget:self action:@selector(q16AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
}

- (void)initQ14Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q14Answer01 isEqualToString:@"1"]) {
		[_q14Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q14Answer02 isEqualToString:@"1"]) {
		[_q14Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q14Answer03 isEqualToString:@"1"]) {
		[_q14Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q14Answer04 isEqualToString:@"1"]) {
		[_q14Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q14Answer05 isEqualToString:@"1"]) {
		[_q14Answer05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ15Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q15Answer01 isEqualToString:@"1"]) {
		[_q15Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q15Answer02 isEqualToString:@"1"]) {
		[_q15Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q15Answer03 isEqualToString:@"1"]) {
		[_q15Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q15Answer04 isEqualToString:@"1"]) {
		[_q15Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q15Answer05 isEqualToString:@"1"]) {
		[_q15Answer05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ16Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q16Answer01 isEqualToString:@"1"]) {
		[_q16Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q16Answer02 isEqualToString:@"1"]) {
		[_q16Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q16Answer03 isEqualToString:@"1"]) {
		[_q16Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q16Answer04 isEqualToString:@"1"]) {
		[_q16Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q16Answer05 isEqualToString:@"1"]) {
		[_q16Answer05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q16Answer20 isEqualToString:@"1"]) {
		[_q16Answer20Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)q14AnswerButtonTapped:(UIButton*)button {
	
	[_q14Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q14Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q14Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q14Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q14Answer05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q14Answer01 = @"0";
	appDelegate.q14Answer02 = @"0";
	appDelegate.q14Answer03 = @"0";
	appDelegate.q14Answer04 = @"0";
	appDelegate.q14Answer05 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q14Answer01 = @"1";
			break;
		case 2:
			appDelegate.q14Answer02 = @"1";
			break;
		case 3:
			appDelegate.q14Answer03 = @"1";
			break;
		case 4:
			appDelegate.q14Answer04 = @"1";
			break;
		case 5:
			appDelegate.q14Answer05 = @"1";
			break;
	}
}

- (void)q15AnswerButtonTapped:(UIButton*)button {
	
	[_q15Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q15Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q15Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q15Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q15Answer05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q15Answer01 = @"0";
	appDelegate.q15Answer02 = @"0";
	appDelegate.q15Answer03 = @"0";
	appDelegate.q15Answer04 = @"0";
	appDelegate.q15Answer05 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q15Answer01 = @"1";
			break;
		case 2:
			appDelegate.q15Answer02 = @"1";
			break;
		case 3:
			appDelegate.q15Answer03 = @"1";
			break;
		case 4:
			appDelegate.q15Answer04 = @"1";
			break;
		case 5:
			appDelegate.q15Answer05 = @"1";
			break;
	}
}

- (void)q16AnswerButtonTapped:(UIButton*)button {
	
	[_q16Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q16Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q16Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q16Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q16Answer05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q16Answer20Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q16Answer01 = @"0";
	appDelegate.q16Answer02 = @"0";
	appDelegate.q16Answer03 = @"0";
	appDelegate.q16Answer04 = @"0";
	appDelegate.q16Answer05 = @"0";
	appDelegate.q16Answer20 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q16Answer01 = @"1";
			break;
		case 2:
			appDelegate.q16Answer02 = @"1";
			break;
		case 3:
			appDelegate.q16Answer03 = @"1";
			break;
		case 4:
			appDelegate.q16Answer04 = @"1";
			break;
		case 5:
			appDelegate.q16Answer05 = @"1";
			break;
		case 20:
			appDelegate.q16Answer20 = @"1";
			break;
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	NSString* message = @"";

	if ([appDelegate.q14Answer01 isEqualToString:@"0"] &&
		[appDelegate.q14Answer02 isEqualToString:@"0"] &&
		[appDelegate.q14Answer03 isEqualToString:@"0"] &&
		[appDelegate.q14Answer04 isEqualToString:@"0"] &&
		[appDelegate.q14Answer05 isEqualToString:@"0"]) {
		message = @"Q14の回答を選択してください";
	}

	if ([appDelegate.q15Answer01 isEqualToString:@"0"] &&
		[appDelegate.q15Answer02 isEqualToString:@"0"] &&
		[appDelegate.q15Answer03 isEqualToString:@"0"] &&
		[appDelegate.q15Answer04 isEqualToString:@"0"] &&
		[appDelegate.q15Answer05 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q15の回答を選択してください"];
		} else {
			message = @"Q15の回答を選択してください";
		}
	}

	if ([appDelegate.q16Answer01 isEqualToString:@"0"] &&
		[appDelegate.q16Answer02 isEqualToString:@"0"] &&
		[appDelegate.q16Answer03 isEqualToString:@"0"] &&
		[appDelegate.q16Answer04 isEqualToString:@"0"] &&
		[appDelegate.q16Answer05 isEqualToString:@"0"] &&
		[appDelegate.q16Answer20 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q16の回答を選択してください"];
		} else {
			message = @"Q16の回答を選択してください";
		}
	}

	if ([message length]) {
		[self showAlert:@"必須項目" message:message];
		return;
	}

	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	Question07ViewController *viewController = [[Question07ViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中のアンケート内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[UtilityFunction resetPersonalInfo];
		QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
