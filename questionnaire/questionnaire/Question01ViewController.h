//
//  Question01ViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Question01ViewController : UIViewController {
	UILabel*		_headerLabel;

	UIButton*		_q1Answer01Button;
	UIButton*		_q1Answer02Button;
	UIButton*		_q1Answer03Button;

	UIButton*		_q2Answer01Button;
	UIButton*		_q2Answer02Button;
	UIButton*		_q2Answer03Button;
	UIButton*		_q2Answer04Button;
	UIButton*		_q2Answer05Button;
	UIButton*		_q2Answer20Button;

	UIButton*		_q3Answer01Button;
	UIButton*		_q3Answer02Button;
	UIButton*		_q3Answer03Button;
	UIButton*		_q3Answer04Button;
	UIButton*		_q3Answer05Button;
	UIButton*		_q3Answer06Button;
	UIButton*		_q3Answer07Button;
	UIButton*		_q3Answer08Button;
	UIButton*		_q3Answer09Button;
	UIButton*		_q3Answer103utton;
	UIButton*		_q3Answer11Button;
	UIButton*		_q3Answer12Button;
	UIButton*		_q3Answer13Button;
	UIButton*		_q3Answer14Button;
	UIButton*		_q3Answer15Button;
	UIButton*		_q3Answer16Button;
	UIButton*		_q3Answer17Button;
	UIButton*		_q3Answer18Button;
	UIButton*		_q3Answer19Button;
	UIButton*		_q3Answer203utton;
	
	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UILabel* headerLabel;

@property (nonatomic) IBOutlet UIButton* q1Answer01Button;
@property (nonatomic) IBOutlet UIButton* q1Answer02Button;
@property (nonatomic) IBOutlet UIButton* q1Answer03Button;

@property (nonatomic) IBOutlet UIButton* q2Answer01Button;
@property (nonatomic) IBOutlet UIButton* q2Answer02Button;
@property (nonatomic) IBOutlet UIButton* q2Answer03Button;
@property (nonatomic) IBOutlet UIButton* q2Answer04Button;
@property (nonatomic) IBOutlet UIButton* q2Answer05Button;
@property (nonatomic) IBOutlet UIButton* q2Answer20Button;

@property (nonatomic) IBOutlet UIButton* q3Answer01Button;
@property (nonatomic) IBOutlet UIButton* q3Answer02Button;
@property (nonatomic) IBOutlet UIButton* q3Answer03Button;
@property (nonatomic) IBOutlet UIButton* q3Answer04Button;
@property (nonatomic) IBOutlet UIButton* q3Answer05Button;
@property (nonatomic) IBOutlet UIButton* q3Answer06Button;
@property (nonatomic) IBOutlet UIButton* q3Answer07Button;
@property (nonatomic) IBOutlet UIButton* q3Answer08Button;
@property (nonatomic) IBOutlet UIButton* q3Answer09Button;
@property (nonatomic) IBOutlet UIButton* q3Answer10Button;
@property (nonatomic) IBOutlet UIButton* q3Answer11Button;
@property (nonatomic) IBOutlet UIButton* q3Answer12Button;
@property (nonatomic) IBOutlet UIButton* q3Answer13Button;
@property (nonatomic) IBOutlet UIButton* q3Answer14Button;
@property (nonatomic) IBOutlet UIButton* q3Answer15Button;
@property (nonatomic) IBOutlet UIButton* q3Answer16Button;
@property (nonatomic) IBOutlet UIButton* q3Answer17Button;
@property (nonatomic) IBOutlet UIButton* q3Answer18Button;
@property (nonatomic) IBOutlet UIButton* q3Answer19Button;
@property (nonatomic) IBOutlet UIButton* q3Answer20Button;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
