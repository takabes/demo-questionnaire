//
//  CompleteViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompleteViewController : UIViewController {
	UIButton*		_nextButton;
}

@property (nonatomic) IBOutlet UIButton* nextButton;

- (IBAction)nextButtonTapped:(UIButton*)button;

@end
