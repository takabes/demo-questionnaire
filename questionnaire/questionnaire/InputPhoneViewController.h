//
//  InputPhoneViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputPhoneViewController : UIViewController {
	BOOL			_edit;

	UITextField*	_phoneTextField;
	
	UIButton*		_oneButton;
	UIButton*		_twoButton;
	UIButton*		_threeButton;
	UIButton*		_fourButton;
	UIButton*		_fiveButton;
	UIButton*		_sixButton;
	UIButton*		_sevenButton;
	UIButton*		_eightButton;
	UIButton*		_nineButton;
	UIButton*		_zeroButton;
	
	UIButton*		_deleteButton;
	
	UIButton*		_nextButton;
	UIButton*		_backButton;
	UIButton*		_showKeyboardButton;
}

@property (nonatomic) BOOL edit;

@property (nonatomic) IBOutlet UITextField* phoneTextField;

@property (nonatomic) IBOutlet UIButton* oneButton;
@property (nonatomic) IBOutlet UIButton* twoButton;
@property (nonatomic) IBOutlet UIButton* threeButton;
@property (nonatomic) IBOutlet UIButton* fourButton;
@property (nonatomic) IBOutlet UIButton* fiveButton;
@property (nonatomic) IBOutlet UIButton* sixButton;
@property (nonatomic) IBOutlet UIButton* sevenButton;
@property (nonatomic) IBOutlet UIButton* eightButton;
@property (nonatomic) IBOutlet UIButton* nineButton;
@property (nonatomic) IBOutlet UIButton* zeroButton;
@property (nonatomic) IBOutlet UIButton* deleteButton;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;
@property (nonatomic) IBOutlet UIButton* showKeyboardButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;
- (IBAction)showKeyboardButtonTapped:(UIButton*)button;

@end
