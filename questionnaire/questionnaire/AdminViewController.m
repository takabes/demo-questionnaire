//
//  AdminViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "AdminViewController.h"
#import "Complete2ViewController.h"
#import "AdminViewController.h"
#import "UtilityFunction.h"
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "FMDB.h"
#import "AppConsts.h"
#import "AppDelegate.h"

@interface AdminViewController ()
@end

@implementation AdminViewController

@synthesize dataCountLabel = _dataCountLabel;
@synthesize accountButton = _accountButton;
@synthesize zipCodeButton = _zipCodeButton;
@synthesize registerButton = _registerButton;
@synthesize questionButton = _questionButton;
@synthesize closeButton = _closeButton;
@synthesize versionLabel = _versionLabel;

- (id)init {
    self = [super initWithNibName:@"Admin" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"管理画面";

    return self;
}

- (void)viewDidLoad {
	
	NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
	_versionLabel.text = [NSString stringWithFormat:@"version %@", version];

	[self initButton];
	
	[self initDataCountLabel];
	
	[super viewDidLoad];
}

- (void)initButton {

	[UtilityFunction customizeButton:_registerButton];
	[UtilityFunction customizeButton:_accountButton];
	[UtilityFunction customizeButton:_zipCodeButton];
	[UtilityFunction customizeButton:_questionButton];
	[UtilityFunction customizeButton:_closeButton];

	if (MODE == MODE_ONLY_PERSONAL_INFO) {
		_questionButton.hidden = YES;
	}
}

- (void)initDataCountLabel {

	NSString* dataCount;

	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSString* saveAnswer = [defaults stringForKey:@"saveAnswer"];
	if (saveAnswer) {
		dataCount = [self getDataCount];
	} else {
		dataCount = @"0";
	}
	
	if ([dataCount isEqualToString:@"0"]) {
		_registerButton.enabled = NO;
	}
	
	_dataCountLabel.text = [NSString stringWithFormat:@"登録済未送信件数  %@", dataCount];
}

- (NSString*)getDataCount {
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];

	NSString* sql = @"SELECT COUNT(id) FROM answer WHERE delete_flag = '0';";
 
	[db open];
	
	FMResultSet* results = [db executeQuery:sql];
	NSString* count;
	while([results next]) {
		count = [results stringForColumnIndex:0];
		_Log(@"%@", count);
	}
	
	[db close];

	return count;
}

- (IBAction)accountButtonTapped:(UIButton*)button {
	
	// インディケータ
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];

	AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSDictionary* param = @{@"accessKey" : API_ACCESS_KEY};
	[manager POST:API_URL_GET_ACCOUNT parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {

		_Log(@"response: %@", responseObject);

		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];

		// アラート
		if ([responseObject[@"result"] isEqualToString:@"success"]) {
			[UtilityFunction saveAccounts:responseObject[@"response"][@"accounts"]];

			[self showAlert:@"確認" message:@"データを同期しました"];
		} else {
			NSString* message = [NSString stringWithFormat:@"%@:%@", responseObject[@"errorCode"], responseObject[@"errorMessage"]];
			[self showAlert:@"エラー" message:message];
		}
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		
		_Log(@"error = %@", error);
		
		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];

		// アラート
		[self showAlert:@"エラー" message:@"通信エラーが発生しました。ネットワーク状況を確認して下さい。"];
	}];
}

- (IBAction)zipCodeButtonTapped:(UIButton*)button {
	
	[UtilityFunction showNowDate];

	// インディケータ
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
		
	AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSDictionary* param = @{@"accessKey" : API_ACCESS_KEY};
	[manager POST:API_URL_GET_ZIP_CODE parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
		
		//_Log(@"response: %@", responseObject);
		
		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		if ([responseObject[@"result"] isEqualToString:@"success"]) {
			
			[self saveZipCodes:responseObject[@"response"][@"zipCodes"]];

			[self showAlert:@"確認" message:@"データを同期しました"];
		} else {
			NSString* message = [NSString stringWithFormat:@"%@:%@", responseObject[@"errorCode"], responseObject[@"errorMessage"]];
			[self showAlert:@"エラー" message:message];
		}
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		
		_Log(@"error = %@", error);

		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		[self showAlert:@"エラー" message:@"通信エラーが発生しました。ネットワーク状況を確認して下さい。"];
	}];
}

- (void)saveZipCodes:(NSMutableArray*)zipCodes {
	//_Log(@"%@", zipCodes);

	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
 
	// CREATE TABLE
	NSString* sql = @"CREATE TABLE IF NOT EXISTS zip_code (id INTEGER PRIMARY KEY AUTOINCREMENT, code TEXT, prefecture TEXT, address1 TEXT, address2 TEXT);";
	[db open];
	[db executeUpdate:sql];
	[db close];
	
	// DELETE TABLE
	sql = @"DELETE FROM zip_code";
	[db open];
	[db executeUpdate:sql];
	[db close];

	[UtilityFunction showNowDate];
	
	for (int i = 0; i < [zipCodes count]; i++) {
		//_Log(@"%@", [zipCodes objectAtIndex:i]);
		NSArray *items = [[zipCodes objectAtIndex:i] componentsSeparatedByString:@","];
		if (i <= 3) {
			_Log(@"%@", items);
		}
		
		// INSERT
		NSString* sql = @"INSERT INTO zip_code (code, prefecture, address1, address2) VALUES (?, ?, ?, ?)";
		[db open];
		[db executeUpdate:sql, [items objectAtIndex:0], [items objectAtIndex:1], [items objectAtIndex:2], [items objectAtIndex:3]];
		[db close];
	}
		
	[UtilityFunction showNowDate];
}

- (IBAction)questionButtonTapped:(UIButton*)button {
	_Log(@"questionButtonTapped");
	
	// インディケータ
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSDictionary* param = @{@"accessKey" : API_ACCESS_KEY, @"userId": appDelegate.userId, @"placeCode":appDelegate.placeCode};
	[manager POST:API_URL_GET_QUESTIONS parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {

		_Log(@"response: %@", responseObject);
		
		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		if ([responseObject[@"result"] isEqualToString:@"success"]) {
			// 保存
			//NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
			//[defaults setObject:responseObject[@"response"] forKey:@"questions"];

			[self saveQuestionTitles:responseObject[@"responseTitles"][@"QuestionTitles"]];

			[self saveQuestionItems:responseObject[@"responseItems"][@"QuestionItems"]];

			[self showAlert:@"確認" message:@"データを同期しました"];
		} else {
			NSString* message = [NSString stringWithFormat:@"%@:%@", responseObject[@"errorCode"], responseObject[@"errorMessage"]];
			[self showAlert:@"エラー" message:message];
		}
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		
		_Log(@"error = %@", error);
		
		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		[self showAlert:@"エラー" message:@"通信エラーが発生しました。ネットワーク状況を確認して下さい。"];
	}];
}

- (void)saveQuestionTitles:(NSMutableArray*)questionTitles {
	//_Log(@"%@", questionTitles);
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
 
	// CREATE TABLE
	NSString* sql = @"CREATE TABLE IF NOT EXISTS question_title (id INTEGER PRIMARY KEY AUTOINCREMENT, question_number INT, type INT, title TEXT, place_code TEXT, start_date DATE, end_date DATE);";
	[db open];
	[db executeUpdate:sql];
	[db close];
	
	// DELETE TABLE
	sql = @"DELETE FROM question_title";
	[db open];
	[db executeUpdate:sql];
	[db close];
	
	for (int i = 0; i < [questionTitles count]; i++) {
		//_Log(@"%@", [zipCodes objectAtIndex:i]);
		NSArray *items = [[questionTitles objectAtIndex:i] componentsSeparatedByString:@","];
		_Log(@"%@", items);
		
		// INSERT
		NSString* type = [items objectAtIndex:4];
		
		NSString* place_code = [items objectAtIndex:7];
		if ([place_code length] == 0) {
			place_code = @"0";
		}
		NSString* start_date = [items objectAtIndex:8];
		if ([start_date length] == 0) {
			start_date = @"2000-01-01";
		}
		NSString* end_date = [items objectAtIndex:9];
		if ([end_date length] == 0) {
			end_date = @"3000-12-31";
		}

		NSString* sql = @"INSERT INTO question_title (id, question_number, type, title, place_code, start_date, end_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
		[db open];
		[db executeUpdate:sql, [items objectAtIndex:0], [items objectAtIndex:1], type, [items objectAtIndex:2], place_code, start_date, end_date];
		[db close];
	}
	
	/*
	sql = @"SELECT id, question_number, type, title, place_code, start_date, end_date FROM question_title";
	
	[db open];
 
	FMResultSet* results = [db executeQuery:sql];
	//_Log(@"%@", results);
	
	//NSMutableArray* personalInfos = [[NSMutableArray alloc] init];
	
	while([results next]) {
		NSString* a1 = [results stringForColumnIndex:0];
		NSString* a2 = [results stringForColumnIndex:1];
		NSString* a3 = [results stringForColumnIndex:2];
		NSString* a4 = [results stringForColumnIndex:3];
		NSString* a5 = [results stringForColumnIndex:4];
		NSString* a6 = [results stringForColumnIndex:5];
		
		_Log(@"%@", a1);
		_Log(@"%@", a2);
		_Log(@"%@", a3);
		_Log(@"%@", a4);
		_Log(@"%@", a5);
		_Log(@"%@", a6);
		_Log(@"--------------");
		//[personalInfos addObject:[results stringForColumnIndex:1]];
	}
	//_Log(@"personalInfos = %@", personalInfos);
	[db close];
	 */
}

- (void)saveQuestionItems:(NSMutableArray*)questionItems {
	//_Log(@"%@", questionItems);
	
	// DBファイルのパス
	NSArray*    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString*   dir   = [paths objectAtIndex:0];
	FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"app.db"]];
 
	// CREATE TABLE
	NSString* sql = @"CREATE TABLE IF NOT EXISTS question_item (id INTEGER PRIMARY KEY AUTOINCREMENT, question_number INT, item_number INT, title TEXT, place_code TEXT, start_date DATE, end_date DATE);";
	[db open];
	[db executeUpdate:sql];
	[db close];
	
	// DELETE TABLE
	sql = @"DELETE FROM question_item";
	[db open];
	[db executeUpdate:sql];
	[db close];
	
	for (int i = 0; i < [questionItems count]; i++) {
		//_Log(@"%@", [zipCodes objectAtIndex:i]);
		NSArray *items = [[questionItems objectAtIndex:i] componentsSeparatedByString:@","];
		//_Log(@"%@", items);
		
		// INSERT
		NSString* place_code = [items objectAtIndex:5];
		if ([place_code length] == 0) {
			place_code = @"0";
		}
		NSString* start_date = [items objectAtIndex:6];
		if ([start_date length] == 0) {
			start_date = @"2000-01-01";
		}
		NSString* end_date = [items objectAtIndex:7];
		if ([end_date length] == 0) {
			end_date = @"3000-12-31";
		}
		//start_date = @"2016-09-01";
		//end_date = @"2016-09-30";
		
		NSString* sql = @"INSERT INTO question_item (id, question_number, item_number, title, place_code, start_date, end_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
		[db open];
		[db executeUpdate:sql, [items objectAtIndex:0], [items objectAtIndex:1], [items objectAtIndex:2], [items objectAtIndex:3], place_code, start_date, end_date];
		[db close];
	}
	
	/*
	 sql = @"SELECT id, question_number, title, place_code, start_date, end_date FROM question_item";
	 
	 [db open];
	 
	 FMResultSet* results = [db executeQuery:sql];
	 _Log(@"%@", results);
	
	 while([results next]) {
		NSString* a1 = [results stringForColumnIndex:0];
		NSString* a2 = [results stringForColumnIndex:1];
		NSString* a3 = [results stringForColumnIndex:2];
		NSString* a4 = [results stringForColumnIndex:3];
		NSString* a5 = [results stringForColumnIndex:4];
		NSString* a6 = [results stringForColumnIndex:5];
		
		_Log(@"%@", a1);
		_Log(@"%@", a2);
		_Log(@"%@", a3);
		_Log(@"%@", a4);
		_Log(@"%@", a5);
		_Log(@"%@", a6);
		_Log(@"--------------");
	 }
	 [db close];
	 */
}

- (IBAction)registerButtonTapped:(UIButton*)button {
	_Log(@"registerButtonTapped");
	
	NSString* url = @"";
	if (MODE == MODE_ONLY_PERSONAL_INFO) {
		url = API_URL_REGISTER_PERSONAL_INFO;
	} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
		url = API_URL_REGISTER_PERSONAL_INFO_ANSWER;
	} else {
		return;
	}
	_Log(@"url = %@", url);

	NSMutableArray* personalInfos = [UtilityFunction getPersonalInfos];
	
	// インディケータ
	[MBProgressHUD showHUDAddedTo:self.view animated:YES];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	_Log(@"appDelegate.userId = %@", appDelegate.userId);
	
	AFHTTPRequestOperationManager* manager = [AFHTTPRequestOperationManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	NSDictionary* param = @{@"accessKey" : API_ACCESS_KEY, @"userId": appDelegate.userId, @"personalInfos":personalInfos};
	[manager POST:url parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
	
		_Log(@"response: %@", responseObject);
		
		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		if ([responseObject[@"result"] isEqualToString:@"success"]) {
			[self showAlert:@"確認" message:@"データを登録しました"];
			[UtilityFunction deletePeronalInfos];
			_dataCountLabel.text = [NSString stringWithFormat:@"データ件数  0"];
			_registerButton.enabled = NO;
		} else {
			NSString* message = [NSString stringWithFormat:@"%@:%@", responseObject[@"errorCode"], responseObject[@"errorMessage"]];
			[self showAlert:@"エラー" message:message];
		}
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		
		_Log(@"error = %@", error);

		// インディケータ
		[MBProgressHUD hideHUDForView:self.view animated:YES];
		
		// アラート
		[self showAlert:@"エラー" message:@"通信エラーが発生しました。ネットワーク状況を確認して下さい。"];
	}];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)closeButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

@end
