//
//  InputZipCodeViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "InputZipCodeViewController.h"
#import "InputAddressViewController.h"
#import "InputPrefectureViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "FMDB.h"
#import "AppConsts.h"
#import "InputSerialNumberViewController.h"
#import "QuestionListViewController.h"

@interface InputZipCodeViewController ()
@end

@implementation InputZipCodeViewController

@synthesize edit = _edit;

@synthesize zipCodeTextField = _zipCodeTextField;
@synthesize oneButton = _oneButton;
@synthesize twoButton = _twoButton;
@synthesize threeButton = _threeButton;
@synthesize fourButton = _fourButton;
@synthesize fiveButton = _fiveButton;
@synthesize sixButton = _sixButton;
@synthesize sevenButton = _sevenButton;
@synthesize eightButton = _eightButton;
@synthesize nineButton = _nineButton;
@synthesize zeroButton = _zeroButton;

@synthesize deleteButton = _deleteButton;
@synthesize nextButton = _nextButton;
@synthesize showKeyboardButton = _showKeyboardButton;

- (id)init {
    self = [super initWithNibName:@"InputZipCode" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"郵便番号入力";
    
    return self;
}

- (void)viewDidLoad {

	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	[self initSwipe];

	[self initButton];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	_zipCodeTextField.text = appDelegate.zipCode;

	_zipCodeTextField.backgroundColor = [UtilityFunction colorWithRBGA:TEXTFIELD_BACKGROUND_COLOR];

	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initButton {
	
	[_oneButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_twoButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_threeButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fourButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_fiveButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sixButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_sevenButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_eightButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_nineButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_zeroButton addTarget:self action:@selector(keyboardButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_deleteButton addTarget:self action:@selector(deleteButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
	[UtilityFunction customizeButton:_showKeyboardButton];
	
	[UtilityFunction customizeKeyboardButton:_oneButton];
	[UtilityFunction customizeKeyboardButton:_twoButton];
	[UtilityFunction customizeKeyboardButton:_threeButton];
	[UtilityFunction customizeKeyboardButton:_fourButton];
	[UtilityFunction customizeKeyboardButton:_fiveButton];
	[UtilityFunction customizeKeyboardButton:_sixButton];
	[UtilityFunction customizeKeyboardButton:_sevenButton];
	[UtilityFunction customizeKeyboardButton:_eightButton];
	[UtilityFunction customizeKeyboardButton:_nineButton];
	[UtilityFunction customizeKeyboardButton:_zeroButton];
	[UtilityFunction customizeKeyboardButton:_deleteButton];
}

- (void)viewDidAppear:(BOOL)animated {
	[_zipCodeTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
	[super viewDidAppear:animated];
}

- (void)keyboardButtonTapped:(UIButton*)button {
	_zipCodeTextField.text = [NSString stringWithFormat:@"%@%@", _zipCodeTextField.text, [UtilityFunction getCharacter:button.tag]];
}

- (void)deleteButtonTapped:(UIButton*)button {
	_Log(@"deleteButtonTapped");
	
	if ([_zipCodeTextField.text length]) {
		NSString* text = [_zipCodeTextField.text substringToIndex:[_zipCodeTextField.text length] - 1];
		_zipCodeTextField.text = text;
	}
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	if ([_zipCodeTextField.text length]) {
		if ([UtilityFunction chkNumeric:_zipCodeTextField.text] != YES) {
			[self showAlert:@"入力項目" message:@"郵便番号は半角数字で入力してください"];
			return;
		} else if ([_zipCodeTextField.text length] != 7) {
			[self showAlert:@"入力項目" message:@"郵便番号は7桁で入力してください"];
			return;
		}
	}

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.zipCode = _zipCodeTextField.text;

	if (_edit) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		NSMutableArray* array = [UtilityFunction getAddress:_zipCodeTextField.text];
		
		if ([array count]) {
			CATransition* transition = [CATransition animation];
			transition.duration = 0.2;
			transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
			transition.type = kCATransitionPush;
			transition.subtype = kCATransitionFromRight;
			
			InputAddressViewController *viewController = [[InputAddressViewController alloc] init];
			viewController.prefecture = [array objectAtIndex:0];
			viewController.address1 = [array objectAtIndex:1];
			viewController.address2 = [array objectAtIndex:2];
			viewController.cursor = 4;

			[self.navigationController.view.layer addAnimation:transition forKey:nil];
			[self.navigationController pushViewController:viewController animated:NO];
		} else {
			CATransition* transition = [CATransition animation];
			transition.duration = 0.2;
			transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
			transition.type = kCATransitionPush;
			transition.subtype = kCATransitionFromRight;
			
			InputPrefectureViewController *viewController = [[InputPrefectureViewController alloc] init];
			
			[self.navigationController.view.layer addAnimation:transition forKey:nil];
			[self.navigationController pushViewController:viewController animated:NO];
		}
	}
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)showKeyboardButtonTapped:(UIButton*)button {
	[_zipCodeTextField becomeFirstResponder];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中の内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		[UtilityFunction resetPersonalInfo];
		
		if (MODE == MODE_ONLY_PERSONAL_INFO) {
			InputSerialNumberViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		} else if (MODE == MODE_PERSONAL_IFNO_ANSWER) {
			QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
			[self.navigationController popToViewController:viewController animated:YES];
		}
		
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
