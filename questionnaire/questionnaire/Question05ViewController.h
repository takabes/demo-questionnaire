//
//  Question05ViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Question05ViewController : UIViewController {
	UILabel*		_headerLabel;

	UIButton*		_q13Answer01Button;
	UIButton*		_q13Answer02Button;
	UIButton*		_q13Answer03Button;
	UIButton*		_q13Answer04Button;
	UIButton*		_q13Answer05Button;
	UIButton*		_q13Answer06Button;
	UIButton*		_q13Answer07Button;
	UIButton*		_q13Answer08Button;
	UIButton*		_q13Answer09Button;
	UIButton*		_q13Answer103utton;
	UIButton*		_q13Answer11Button;
	UIButton*		_q13Answer12Button;
	UIButton*		_q13Answer13Button;
	UIButton*		_q13Answer14Button;
	UIButton*		_q13Answer15Button;
	UIButton*		_q13Answer16Button;
	UIButton*		_q13Answer17Button;
	UIButton*		_q13Answer18Button;
	UIButton*		_q13Answer19Button;
	UIButton*		_q13Answer203utton;
	
	UIButton*		_nextButton;
	UIButton*		_backButton;
}

@property (nonatomic) IBOutlet UILabel* headerLabel;

@property (nonatomic) IBOutlet UIButton* q13Answer01Button;
@property (nonatomic) IBOutlet UIButton* q13Answer02Button;
@property (nonatomic) IBOutlet UIButton* q13Answer03Button;
@property (nonatomic) IBOutlet UIButton* q13Answer04Button;
@property (nonatomic) IBOutlet UIButton* q13Answer05Button;
@property (nonatomic) IBOutlet UIButton* q13Answer06Button;
@property (nonatomic) IBOutlet UIButton* q13Answer07Button;
@property (nonatomic) IBOutlet UIButton* q13Answer08Button;
@property (nonatomic) IBOutlet UIButton* q13Answer09Button;
@property (nonatomic) IBOutlet UIButton* q13Answer10Button;
@property (nonatomic) IBOutlet UIButton* q13Answer11Button;
@property (nonatomic) IBOutlet UIButton* q13Answer12Button;
@property (nonatomic) IBOutlet UIButton* q13Answer13Button;
@property (nonatomic) IBOutlet UIButton* q13Answer14Button;
@property (nonatomic) IBOutlet UIButton* q13Answer15Button;
@property (nonatomic) IBOutlet UIButton* q13Answer16Button;
@property (nonatomic) IBOutlet UIButton* q13Answer17Button;
@property (nonatomic) IBOutlet UIButton* q13Answer18Button;
@property (nonatomic) IBOutlet UIButton* q13Answer19Button;
@property (nonatomic) IBOutlet UIButton* q13Answer20Button;

@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;

- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;

@end
