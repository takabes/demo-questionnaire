//
//  CompleteViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "CompleteViewController.h"
#import "Complete2ViewController.h"
#import "UtilityFunction.h"
#import "AppDelegate.h"
#import "FMDB.h"
#import "FBEncryptorAES.h"
#import "AppConsts.h"

@interface CompleteViewController ()
@end

@implementation CompleteViewController

@synthesize nextButton = _nextButton;

- (id)init {
    self = [super initWithNibName:@"Complete" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"登録完了";

    return self;
}

- (void)viewDidLoad {
	
	// 戻るボタン非表示
	[self.navigationItem setHidesBackButton:YES];

	[UtilityFunction customizeButton2:_nextButton];
	_nextButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
	_nextButton.titleLabel.numberOfLines = 2;
	[_nextButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
	
	[super viewDidLoad];
}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	Complete2ViewController *viewController = [[Complete2ViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

@end
