//
//  Question01ViewController.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/08/10.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "Question01ViewController.h"
#import "Question02ViewController.h"
#import "AppDelegate.h"
#import "UtilityFunction.h"
#import "QuestionListViewController.h"

@interface Question01ViewController ()
@end

@implementation Question01ViewController

@synthesize q1Answer01Button = _q1Answer01Button;
@synthesize q1Answer02Button = _q1Answer02Button;
@synthesize q1Answer03Button = _q1Answer03Button;

@synthesize nextButton = _nextButton;
@synthesize backButton = _backButton;

- (id)init {
    self = [super initWithNibName:@"Question01" bundle:nil];
    if (!self) {
        return nil;
    }
	
	self.title = @"アンケート入力";

    return self;
}

- (void)viewDidLoad {

	// 戻るボタン非表示
	[self.navigationItem setHidesBackButton:YES];

	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"＜ 戻る"
																			 style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(backButtonTapped:)];
	
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"●"
																	style:UIBarButtonItemStylePlain
																			target:self
																			action:@selector(cancelButtonTapped)];
	[cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} forState:UIControlStateNormal];
	self.navigationItem.rightBarButtonItem = cancelButton;

	
	[self initSwipe];
	
	[self initHeaderLabel];
	
	[self initButton];

	[self initQ3Choice];
	
	[self initQ1Answer];
	[self initQ2Answer];
	[self initQ3Answer];
	
	[super viewDidLoad];
}

- (void)initSwipe {
	
	UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
	
	swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
	
	[self.view addGestureRecognizer:swipeLeftGesture];
	
	UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
	
	swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
	
	[self.view addGestureRecognizer:swipeRightGesture];
}

- (void)initHeaderLabel {
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	if (questions[@"q19"]) {
		if (questions[@"q20"]) {
			_headerLabel.text = @"1 / 9";
		} else {
			_headerLabel.text = @"1 / 8";
		}
	} else {
		_headerLabel.text = @"1 / 7";
	}
}

- (void)initQ3Choice {
	
	_q3Answer01Button.hidden = YES;
	_q3Answer02Button.hidden = YES;
	_q3Answer03Button.hidden = YES;
	_q3Answer04Button.hidden = YES;
	_q3Answer05Button.hidden = YES;
	_q3Answer06Button.hidden = YES;
	_q3Answer07Button.hidden = YES;
	_q3Answer08Button.hidden = YES;
	_q3Answer09Button.hidden = YES;
	_q3Answer10Button.hidden = YES;
	_q3Answer11Button.hidden = YES;
	_q3Answer12Button.hidden = YES;
	_q3Answer13Button.hidden = YES;
	_q3Answer14Button.hidden = YES;
	_q3Answer15Button.hidden = YES;
	_q3Answer16Button.hidden = YES;
	_q3Answer17Button.hidden = YES;
	_q3Answer18Button.hidden = YES;
	_q3Answer19Button.hidden = YES;
	_q3Answer20Button.hidden = YES;
		
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id questions = [defaults objectForKey:@"questions"];
	NSArray* q3Choices = questions[@"q3"][@"choices"];

	for (int i=0; i < [q3Choices count]; i++) {
		
		switch (i) {
			case 0:
				_q3Answer01Button.hidden = NO;
				[_q3Answer01Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 1:
				_q3Answer02Button.hidden = NO;
				[_q3Answer02Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 2:
				_q3Answer03Button.hidden = NO;
				[_q3Answer03Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 3:
				_q3Answer04Button.hidden = NO;
				[_q3Answer04Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 4:
				_q3Answer05Button.hidden = NO;
				[_q3Answer05Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 5:
				_q3Answer06Button.hidden = NO;
				[_q3Answer06Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 6:
				_q3Answer07Button.hidden = NO;
				[_q3Answer07Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 7:
				_q3Answer08Button.hidden = NO;
				[_q3Answer08Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 8:
				_q3Answer09Button.hidden = NO;
				[_q3Answer09Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 9:
				_q3Answer10Button.hidden = NO;
				[_q3Answer10Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 10:
				_q3Answer11Button.hidden = NO;
				[_q3Answer11Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 11:
				_q3Answer12Button.hidden = NO;
				[_q3Answer12Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 12:
				_q3Answer12Button.hidden = NO;
				[_q3Answer12Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 13:
				_q3Answer14Button.hidden = NO;
				[_q3Answer14Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 14:
				_q3Answer15Button.hidden = NO;
				[_q3Answer15Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 15:
				_q3Answer15Button.hidden = NO;
				[_q3Answer15Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 16:
				_q3Answer17Button.hidden = NO;
				[_q3Answer17Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 17:
				_q3Answer18Button.hidden = NO;
				[_q3Answer18Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 18:
				_q3Answer19Button.hidden = NO;
				[_q3Answer19Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
			case 19:
				_q3Answer20Button.hidden = NO;
				[_q3Answer20Button setTitle:[q3Choices objectAtIndex:i] forState:UIControlStateNormal];
				break;
		}
	}

	NSString* q3OtherChoice = questions[@"q3"][@"otherChoice"];

	if (q3OtherChoice && [q3OtherChoice length]) {
		switch ([q3Choices count]) {
			case 1:
				_q3Answer02Button.hidden = NO;
				_q3Answer02Button.tag = 20;
				[_q3Answer02Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 2:
				_q3Answer03Button.hidden = NO;
				_q3Answer03Button.tag = 20;
				[_q3Answer03Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 3:
				_q3Answer04Button.hidden = NO;
				_q3Answer04Button.tag = 20;
				[_q3Answer04Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 4:
				_q3Answer05Button.hidden = NO;
				_q3Answer05Button.tag = 20;
				[_q3Answer05Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 5:
				_q3Answer06Button.hidden = NO;
				_q3Answer06Button.tag = 20;
				[_q3Answer06Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 6:
				_q3Answer07Button.hidden = NO;
				_q3Answer07Button.tag = 20;
				[_q3Answer07Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 7:
				_q3Answer07Button.hidden = NO;
				_q3Answer07Button.tag = 20;
				[_q3Answer07Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 8:
				_q3Answer09Button.hidden = NO;
				_q3Answer09Button.tag = 20;
				[_q3Answer09Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 9:
				_q3Answer10Button.hidden = NO;
				_q3Answer10Button.tag = 20;
				[_q3Answer10Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 10:
				_q3Answer11Button.hidden = NO;
				_q3Answer11Button.tag = 20;
				[_q3Answer11Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 11:
				_q3Answer12Button.hidden = NO;
				_q3Answer12Button.tag = 20;
				[_q3Answer12Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 12:
				_q3Answer13Button.hidden = NO;
				_q3Answer13Button.tag = 20;
				[_q3Answer13Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 13:
				_q3Answer14Button.hidden = NO;
				_q3Answer14Button.tag = 20;
				[_q3Answer14Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 14:
				_q3Answer15Button.hidden = NO;
				_q3Answer15Button.tag = 20;
				[_q3Answer15Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 15:
				_q3Answer16Button.hidden = NO;
				_q3Answer16Button.tag = 20;
				[_q3Answer16Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 16:
				_q3Answer17Button.hidden = NO;
				_q3Answer17Button.tag = 20;
				[_q3Answer17Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 17:
				_q3Answer18Button.hidden = NO;
				_q3Answer18Button.tag = 20;
				[_q3Answer18Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 18:
				_q3Answer19Button.hidden = NO;
				_q3Answer19Button.tag = 20;
				[_q3Answer19Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
			case 19:
				_q3Answer20Button.hidden = NO;
				_q3Answer20Button.tag = 20;
				[_q3Answer20Button setTitle:q3OtherChoice forState:UIControlStateNormal];
				break;
		}
	}
}

- (void)initButton {
	
	[_q1Answer01Button addTarget:self action:@selector(q1AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q1Answer02Button addTarget:self action:@selector(q1AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q1Answer03Button addTarget:self action:@selector(q1AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[_q2Answer01Button addTarget:self action:@selector(q2AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q2Answer02Button addTarget:self action:@selector(q2AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q2Answer03Button addTarget:self action:@selector(q2AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q2Answer04Button addTarget:self action:@selector(q2AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q2Answer05Button addTarget:self action:@selector(q2AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q2Answer20Button addTarget:self action:@selector(q2AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[_q3Answer01Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer02Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer03Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer04Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer05Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer06Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer07Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer08Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer09Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer10Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer11Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer12Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer13Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer14Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer15Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer16Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer17Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer18Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer19Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[_q3Answer20Button addTarget:self action:@selector(q3AnswerButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[UtilityFunction customizeButton:_nextButton];
	[UtilityFunction customizeButton:_backButton];
	
	_backButton.hidden = YES;
}

- (void)initQ1Answer {

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	if ([appDelegate.q1Answer01 isEqualToString:@"1"]) {
		[_q1Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q1Answer02 isEqualToString:@"1"]) {
		[_q1Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q1Answer03 isEqualToString:@"1"]) {
		[_q1Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ2Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q2Answer01 isEqualToString:@"1"]) {
		[_q2Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q2Answer02 isEqualToString:@"1"]) {
		[_q2Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q2Answer03 isEqualToString:@"1"]) {
		[_q2Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q2Answer04 isEqualToString:@"1"]) {
		[_q2Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q2Answer05 isEqualToString:@"1"]) {
		[_q2Answer05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q2Answer20 isEqualToString:@"1"]) {
		[_q2Answer20Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
}

- (void)initQ3Answer {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	
	if ([appDelegate.q3Answer01 isEqualToString:@"1"]) {
		[_q3Answer01Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer02 isEqualToString:@"1"]) {
		[_q3Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer03 isEqualToString:@"1"]) {
		[_q3Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer04 isEqualToString:@"1"]) {
		[_q3Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer05 isEqualToString:@"1"]) {
		[_q3Answer05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer06 isEqualToString:@"1"]) {
		[_q3Answer06Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer07 isEqualToString:@"1"]) {
		[_q3Answer07Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer08 isEqualToString:@"1"]) {
		[_q3Answer08Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer09 isEqualToString:@"1"]) {
		[_q3Answer09Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer10 isEqualToString:@"1"]) {
		[_q3Answer10Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer11 isEqualToString:@"1"]) {
		[_q3Answer11Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer12 isEqualToString:@"1"]) {
		[_q3Answer12Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer13 isEqualToString:@"1"]) {
		[_q3Answer13Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer14 isEqualToString:@"1"]) {
		[_q3Answer14Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer15 isEqualToString:@"1"]) {
		[_q3Answer15Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer16 isEqualToString:@"1"]) {
		[_q3Answer16Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer17 isEqualToString:@"1"]) {
		[_q3Answer17Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer18 isEqualToString:@"1"]) {
		[_q3Answer18Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer19 isEqualToString:@"1"]) {
		[_q3Answer19Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	}
	if ([appDelegate.q3Answer20 isEqualToString:@"1"]) {
		
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		id questions = [defaults objectForKey:@"questions"];
		NSArray* q3Choices = questions[@"q3"][@"choices"];

		switch ([q3Choices count]) {
			case 1:
				[_q3Answer02Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 2:
				[_q3Answer03Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 3:
				[_q3Answer04Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 4:
				[_q3Answer05Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 5:
				[_q3Answer06Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 6:
				[_q3Answer07Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 7:
				[_q3Answer08Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 8:
				[_q3Answer09Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 9:
				[_q3Answer10Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 10:
				[_q3Answer11Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 11:
				[_q3Answer12Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 12:
				[_q3Answer13Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 13:
				[_q3Answer14Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 14:
				[_q3Answer15Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 15:
				[_q3Answer16Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 16:
				[_q3Answer17Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 17:
				[_q3Answer18Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 18:
				[_q3Answer19Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
			case 19:
				[_q3Answer20Button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
				break;
		}
	}
}

- (void)q1AnswerButtonTapped:(UIButton*)button {
	
	[_q1Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q1Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q1Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q1Answer01 = @"0";
	appDelegate.q1Answer02 = @"0";
	appDelegate.q1Answer03 = @"0";

	switch (button.tag) {
		case 1:
			appDelegate.q1Answer01 = @"1";
			break;
		case 2:
			appDelegate.q1Answer02 = @"1";
			break;
		case 3:
			appDelegate.q1Answer03 = @"1";
			break;
	}
}

- (void)q2AnswerButtonTapped:(UIButton*)button {
	
	[_q2Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q2Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q2Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q2Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q2Answer05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q2Answer20Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q2Answer01 = @"0";
	appDelegate.q2Answer02 = @"0";
	appDelegate.q2Answer03 = @"0";
	appDelegate.q2Answer04 = @"0";
	appDelegate.q2Answer05 = @"0";
	appDelegate.q2Answer20 = @"0";
	
	switch (button.tag) {
		case 1:
			appDelegate.q2Answer01 = @"1";
			break;
		case 2:
			appDelegate.q2Answer02 = @"1";
			break;
		case 3:
			appDelegate.q2Answer03 = @"1";
			break;
		case 4:
			appDelegate.q2Answer04 = @"1";
			break;
		case 5:
			appDelegate.q2Answer05 = @"1";
			break;
		case 20:
			appDelegate.q2Answer20 = @"1";
			break;
	}
}

- (void)q3AnswerButtonTapped:(UIButton*)button {
	
	_Log(@"q3AnswerButtonTapped");
	
	[_q3Answer01Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer02Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer03Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer04Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer05Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer06Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer07Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer08Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer09Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer10Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer11Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer12Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer13Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer14Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer15Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer16Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer17Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer18Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer19Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	[_q3Answer20Button setImage:[UIImage imageNamed:@"radioButtonOff.png"] forState: UIControlStateNormal];
	
	[button setImage:[UIImage imageNamed:@"radioButtonOn.png"] forState: UIControlStateNormal];

	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.q3Answer01 = @"0";
	appDelegate.q3Answer02 = @"0";
	appDelegate.q3Answer03 = @"0";
	appDelegate.q3Answer04 = @"0";
	appDelegate.q3Answer05 = @"0";
	appDelegate.q3Answer06 = @"0";
	appDelegate.q3Answer07 = @"0";
	appDelegate.q3Answer08 = @"0";
	appDelegate.q3Answer09 = @"0";
	appDelegate.q3Answer10 = @"0";
	appDelegate.q3Answer11 = @"0";
	appDelegate.q3Answer12 = @"0";
	appDelegate.q3Answer13 = @"0";
	appDelegate.q3Answer14 = @"0";
	appDelegate.q3Answer15 = @"0";
	appDelegate.q3Answer16 = @"0";
	appDelegate.q3Answer17 = @"0";
	appDelegate.q3Answer18 = @"0";
	appDelegate.q3Answer19 = @"0";
	appDelegate.q3Answer20 = @"0";

	switch (button.tag) {
		case 1:
			appDelegate.q3Answer01 = @"1";
			break;
		case 2:
			appDelegate.q3Answer02 = @"1";
			break;
		case 3:
			appDelegate.q3Answer03 = @"1";
			break;
		case 4:
			appDelegate.q3Answer04 = @"1";
			break;
		case 5:
			appDelegate.q3Answer05 = @"1";
			break;
		case 6:
			appDelegate.q3Answer06 = @"1";
			break;
		case 7:
			appDelegate.q3Answer07 = @"1";
			break;
		case 8:
			appDelegate.q3Answer08 = @"1";
			break;
		case 9:
			appDelegate.q3Answer09 = @"1";
			break;
		case 10:
			appDelegate.q3Answer10 = @"1";
			break;
		case 11:
			appDelegate.q3Answer11 = @"1";
			break;
		case 12:
			appDelegate.q3Answer12 = @"1";
			break;
		case 13:
			appDelegate.q3Answer13 = @"1";
			break;
		case 14:
			appDelegate.q3Answer14 = @"1";
			break;
		case 15:
			appDelegate.q3Answer15 = @"1";
			break;
		case 16:
			appDelegate.q3Answer16 = @"1";
			break;
		case 17:
			appDelegate.q3Answer17 = @"1";
			break;
		case 18:
			appDelegate.q3Answer18 = @"1";
			break;
		case 19:
			appDelegate.q3Answer19 = @"1";
			break;
		case 20:
			appDelegate.q3Answer20 = @"1";
			break;
	}

}

- (IBAction)nextButtonTapped:(UIButton*)button {
	
	AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];

	NSString* message = @"";
	
	if ([appDelegate.q1Answer01 isEqualToString:@"0"] &&
		[appDelegate.q1Answer02 isEqualToString:@"0"] &&
		[appDelegate.q1Answer03 isEqualToString:@"0"]) {
		message = @"Q1の回答を選択してください";
	}

	if ([appDelegate.q2Answer01 isEqualToString:@"0"] &&
		[appDelegate.q2Answer02 isEqualToString:@"0"] &&
		[appDelegate.q2Answer03 isEqualToString:@"0"] &&
		[appDelegate.q2Answer04 isEqualToString:@"0"] &&
		[appDelegate.q2Answer05 isEqualToString:@"0"] &&
		[appDelegate.q2Answer20 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q2の回答を選択してください"];
		} else {
			message = @"Q2の回答を選択してください";
		}
	}

	if ([appDelegate.q3Answer01 isEqualToString:@"0"] &&
		[appDelegate.q3Answer02 isEqualToString:@"0"] &&
		[appDelegate.q3Answer03 isEqualToString:@"0"] &&
		[appDelegate.q3Answer04 isEqualToString:@"0"] &&
		[appDelegate.q3Answer05 isEqualToString:@"0"] &&
		[appDelegate.q3Answer06 isEqualToString:@"0"] &&
		[appDelegate.q3Answer07 isEqualToString:@"0"] &&
		[appDelegate.q3Answer08 isEqualToString:@"0"] &&
		[appDelegate.q3Answer09 isEqualToString:@"0"] &&
		[appDelegate.q3Answer10 isEqualToString:@"0"] &&
		[appDelegate.q3Answer11 isEqualToString:@"0"] &&
		[appDelegate.q3Answer12 isEqualToString:@"0"] &&
		[appDelegate.q3Answer13 isEqualToString:@"0"] &&
		[appDelegate.q3Answer14 isEqualToString:@"0"] &&
		[appDelegate.q3Answer15 isEqualToString:@"0"] &&
		[appDelegate.q3Answer16 isEqualToString:@"0"] &&
		[appDelegate.q3Answer17 isEqualToString:@"0"] &&
		[appDelegate.q3Answer18 isEqualToString:@"0"] &&
		[appDelegate.q3Answer19 isEqualToString:@"0"] &&
		[appDelegate.q3Answer20 isEqualToString:@"0"]) {
		if ([message length]) {
			message = [NSString stringWithFormat:@"%@\n%@", message, @"Q3の回答を選択してください"];
		} else {
			message = @"Q3の回答を選択してください";
		}
	}
	
	if ([message length]) {
		[self showAlert:@"必須項目" message:message];
		return;
	}

	CATransition* transition = [CATransition animation];
	transition.duration = 0.2;
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
	transition.type = kCATransitionPush;
	transition.subtype = kCATransitionFromRight;
	
	Question02ViewController *viewController = [[Question02ViewController alloc] init];
	
	[self.navigationController.view.layer addAnimation:transition forKey:nil];
	[self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)backButtonTapped:(UIButton*)button {
	[self showBackAlert:@"確認" message:@"戻ると入力中のアンケート内容がすべて消去されます。よろしいですか？"];
}

- (void)showAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (void)showBackAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[UtilityFunction resetPersonalInfo];
		[self.navigationController popViewControllerAnimated:YES];
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

// 左から右へスワイプされたら実行
- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
	_Log(@"右から左にスワイプされました");
	[self nextButtonTapped:_nextButton];
}

// 右から左へスワイプされたら実行
- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
	_Log(@"左から右にスワイプされました");
}

- (void)cancelButtonTapped {
	[self showCancelAlert:@"確認" message:@"入力中のアンケート内容がすべて消去されます。よろしいですか？"];
}

- (void)showCancelAlert:(NSString*)title message:(NSString*)message {
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	[alertController addAction:[UIAlertAction actionWithTitle:@"はい" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		[UtilityFunction resetPersonalInfo];
		QuestionListViewController *viewController = [self.navigationController.viewControllers objectAtIndex:1];
		[self.navigationController popToViewController:viewController animated:YES];
	}]];
	[alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
	}]];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
