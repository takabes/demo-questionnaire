//
//  AppDelegate.m
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize userId = _userId;
@synthesize placeCode = _placeCode;
@synthesize showPublicInfo = _showPublicInfo;
@synthesize regular = _regular;
@synthesize serialNumber = _serialNumber;
@synthesize lastName = _lastName;
@synthesize firstName = _firstName;
@synthesize lastNameKana = _lastNameKana;
@synthesize firstNameKana = _firstNameKana;
@synthesize zipCode = _zipCode;
@synthesize prefecture = _prefecture;
@synthesize address1 = _address1;
@synthesize address2 = _address2;
@synthesize address3 = _address3;
@synthesize address4 = _address4;
@synthesize phone = _phone;
@synthesize emailAccount = _emailAccount;
@synthesize emailDomain = _emailDomain;
@synthesize directMail = _directMail;
@synthesize mailMagazie = _mailMagazie;
@synthesize publicInfo = _publicInfo;
@synthesize selectedPrefecture = _selectedPrefecture;
@synthesize otherPrefecture = _otherPrefecture;

@synthesize q1Answer01 = _q1Answer01;
@synthesize q1Answer02 = _q1Answer02;
@synthesize q1Answer03 = _q1Answer03;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	// Override point for customization after application launch.
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	self.window.backgroundColor = [UIColor whiteColor];
	MainViewController *viewController = [[MainViewController alloc] init];
	self.window.rootViewController = viewController;
	[self.window makeKeyAndVisible];

	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
