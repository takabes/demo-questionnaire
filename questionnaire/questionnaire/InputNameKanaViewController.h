//
//  InputNameKanaViewController.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputNameKanaViewController : UIViewController <UITextFieldDelegate> {
	NSUInteger		_guideMode;
	BOOL			_edit;
	NSUInteger		_currentTextFieldTag;

	UITextField*	_lastNameTextField;
	UITextField*	_firstNameTextField;
	UIButton*		_lastNameSubmitButton;
	UIButton*		_firstNameSubmitButton;
	UIImageView*	_guideImageView;
	UIButton*		_switchGuiteButton;
	UIButton*		_showGuiteButton;
	UIButton*		_hideGuiteButton;
	UIButton*		_nextButton;
	UIButton*		_backButton;
	UIButton*		_showKeyboardButton;
}

@property (nonatomic) BOOL edit;
@property (nonatomic) IBOutlet UITextField* lastNameTextField;
@property (nonatomic) IBOutlet UITextField* firstNameTextField;
@property (nonatomic) IBOutlet UIButton* lastNameSubmitButton;
@property (nonatomic) IBOutlet UIButton* firstNameSubmitButton;
@property (nonatomic) IBOutlet UIImageView* guideImageView;
@property (nonatomic) IBOutlet UIButton* switchGuiteButton;
@property (nonatomic) IBOutlet UIButton* showGuiteButton;
@property (nonatomic) IBOutlet UIButton* hideGuiteButton;
@property (nonatomic) IBOutlet UIButton* nextButton;
@property (nonatomic) IBOutlet UIButton* backButton;
@property (nonatomic) IBOutlet UIButton* showKeyboardButton;

- (IBAction)lastNameSubmitButtonTapped:(UIButton*)button;
- (IBAction)nextButtonTapped:(UIButton*)button;
- (IBAction)backButtonTapped:(UIButton*)button;
- (IBAction)switchGuideButtonTapped:(UIButton*)button;
- (IBAction)showGuideButtonTapped:(UIButton*)button;
- (IBAction)hideGuideButtonTapped:(UIButton*)button;
- (IBAction)showKeyboardButtonTapped:(UIButton*)button;

@end
