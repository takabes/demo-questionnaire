//
//  AppConsts.h
//  questionnaire
//
//  Created by Shuichi Takabe on 2016/01/12.
//  Copyright © 2016年 Di-Next. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppConsts : NSObject

// モード
#define MODE_ONLY_PERSONAL_INFO		1
#define MODE_PERSONAL_IFNO_ANSWER	2

// AES暗号キー
#define AES_KEY			@"4dd0b0caa573836d962b9286d68c318f"

// APIアクセスキー
#define API_ACCESS_KEY	@"36c52b57d636382625a80ad11f8f0570"

// モード
extern NSUInteger const MODE;

// スタッフアカウント取得API URL
extern NSString* const API_URL_GET_ACCOUNT;

// 郵便番号取得API URL
extern NSString* const API_URL_GET_ZIP_CODE;

// 可変アンケート取得API URL
extern NSString* const API_URL_GET_QUESTIONS;

// 個人情報登録API URL
extern NSString* const API_URL_REGISTER_PERSONAL_INFO;

// 個人情報 + アンケート回答登録API URL
extern NSString* const API_URL_REGISTER_PERSONAL_INFO_ANSWER;

// 入力中テキストフィールド背景色
#define TEXTFIELD_BACKGROUND_COLOR  0xFFBEDA

@end